package com.guaoran.distributed.message.rabbitmq.spring.producer;

import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2019/1/16 19:54
 */
public class MessageProducerTest {
    public void sendMessage() throws Exception {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        MessageProducer producer = (MessageProducer) applicationContext.getBean("messageProducer");
        int i = 0;
        while (i<100) {
            producer.sendMessage("第"+i+"次发送消息");
            i++;
            Thread.sleep(100);
        }
    }

}