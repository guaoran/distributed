package com.guaoran.distributed.message.rabbitmq.java.api.demo;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author : guaoran
 * @Description : <br/>
 *  消息提供者
 * @date :2019/1/16 17:38
 */
public class ProducerDemo {
    private final static String EXCHANGE_NAME = "SIMPLE_EXCHANGE";
    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        // 连接IP
        factory.setHost("192.168.45.136");
        // 连接端口
        factory.setPort(5672);
        // 虚拟机
        factory.setVirtualHost("/");
        // 用户
        factory.setUsername("guaoran");
        factory.setPassword("guaoran");

        // 建立连接
        Connection conn = factory.newConnection();
        // 创建消息通道
        Channel channel = conn.createChannel();

        // 发送消息
        String msg = "Hello world, Rabbit MQ";

        // String exchange, String routingKey, BasicProperties props, byte[] body
        channel.basicPublish(EXCHANGE_NAME, "first.test", null, msg.getBytes());

        channel.close();
        conn.close();
    }
}
