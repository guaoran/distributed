package com.guaoran.distributed.message.rabbitmq.java.api.util;

import java.util.ResourceBundle;

/**
 * @author : guaoran
 * @Description : <br/>
 * 配置文件读取工具类
 * @date :2019/1/16 18:47
 */
public class ResourceUtil {
    private static final ResourceBundle resourceBundle;

    static{
        resourceBundle = ResourceBundle.getBundle("config");
    }

    public static String getKey(String key){
        return resourceBundle.getString(key);
    }

}
