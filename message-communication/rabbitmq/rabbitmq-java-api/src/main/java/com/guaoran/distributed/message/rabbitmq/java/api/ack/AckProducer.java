package com.guaoran.distributed.message.rabbitmq.java.api.ack;

import com.guaoran.distributed.message.rabbitmq.java.api.util.ResourceUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author : guaoran
 * @Description : <br/>
 *  生产者，测试消费者手工应答和重回队列
 * @date :2019/1/16 18:44
 */
public class AckProducer {
    private final static String QUEUE_NAME = "TEST_ACK_QUEUE";

    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(ResourceUtil.getKey("rabbitmq.uri"));

        // 建立连接
        Connection conn = factory.newConnection();
        // 创建消息通道
        Channel channel = conn.createChannel();

        String msg = "test ack message ";
        // 声明队列（默认交换机AMQP default，Direct）
        // String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 发送消息
        // String exchange, String routingKey, BasicProperties props, byte[] body
        for (int i =0; i<15; i++){
            if(i<5) {
                channel.basicPublish("", QUEUE_NAME, null, (msg + i).getBytes());
            }else if (i<10){
                channel.basicPublish("", QUEUE_NAME, null, "拒收".getBytes());
            }else{
                channel.basicPublish("", QUEUE_NAME, null, "异常".getBytes());
            }
        }


        channel.close();
        conn.close();
    }
}

