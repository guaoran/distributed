package com.guaoran.distributed.message.rabbitmq.springboot.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;


/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2019/1/17 11:21
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class RabbitSenderTest {
//    @Autowired
    RabbitSender rabbitSender;

//    @Test
    public void contextLoads() throws JsonProcessingException {
        // 先启动消费者 springboot-rabbit-consumer工程，否则交换机、队列、绑定都不存在
        rabbitSender.send();
    }
}