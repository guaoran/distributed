package com.guaoran.distributed.message.rabbitmq.springboot.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.guaoran.distributed.message.rabbitmq.springboot.entity.Merchant;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @author : guaoran
 * @Description : <br/>
 *  消息发送类
 * @date :2019/1/17 10:52
 */
@Component
@PropertySource("classpath:rabbitmq.properties")
public class RabbitSender {

    @Value("${com.guaoran.directexchange}")
    private String directExchange;

    @Value("${com.guaoran.topicexchange}")
    private String topicExchange;

    @Value("${com.guaoran.fanoutexchange}")
    private String fanoutExchange;

    @Value("${com.guaoran.directroutingkey}")
    private String directRoutingKey;

    @Value("${com.guaoran.topicroutingkey1}")
    private String topicRoutingKey1;

    @Value("${com.guaoran.topicroutingkey2}")
    private String topicRoutingKey2;


    // 自定义的模板，所有的消息都会转换成JSON发送
    @Autowired
    AmqpTemplate rabbitmqTemplate;

    public void send() throws JsonProcessingException {
        Merchant merchant =  new Merchant(1001,"a direct msg : direct","北京市");
        // 直连交换机
        rabbitmqTemplate.convertAndSend(directExchange,directRoutingKey, merchant);

        rabbitmqTemplate.convertAndSend(topicExchange,topicRoutingKey1,"a topic msg : shangqiu.guaoran.student");
        rabbitmqTemplate.convertAndSend(topicExchange,topicRoutingKey2,"a topic msg : beijing.guaoran.java");

        // 发送JSON字符串
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(merchant);
        System.out.println(json);
        rabbitmqTemplate.convertAndSend(fanoutExchange,"", json);
    }


}
