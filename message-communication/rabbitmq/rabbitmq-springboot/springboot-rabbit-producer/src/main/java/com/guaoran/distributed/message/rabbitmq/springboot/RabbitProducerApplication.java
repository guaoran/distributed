package com.guaoran.distributed.message.rabbitmq.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : guaoran
 * @Description : <br/>
 *  启动类
 * @date :2019/1/17 10:52
 */
@SpringBootApplication
public class RabbitProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RabbitProducerApplication.class, args);
	}
}
