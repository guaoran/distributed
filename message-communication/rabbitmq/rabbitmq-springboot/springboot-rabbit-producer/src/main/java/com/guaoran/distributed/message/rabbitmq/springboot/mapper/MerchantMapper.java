package com.guaoran.distributed.message.rabbitmq.springboot.mapper;
import com.guaoran.distributed.message.rabbitmq.springboot.entity.Merchant;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MerchantMapper {

   Merchant getMerchantById(Integer sid);

    public List<Merchant> getMerchantList(String name, int page, int limit);

    public int add(Merchant merchant);

    public int update(Merchant merchant);

    public int updateState(Merchant merchant);

    public int delete(Integer sid);

    int getMerchantCount();
}