package com.guaoran.distributed.message.rabbitmq.springboot.consumer;

import com.guaoran.distributed.message.rabbitmq.springboot.entity.Merchant;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.PropertySource;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @author : guaoran
 * @Description : <br/>
 *  消费队列
 * @date :2019/1/17 10:52
 */
@Component
@PropertySource("classpath:rabbitmq.properties")
@RabbitListener(queues = "${com.guaoran.firstqueue}", containerFactory="rabbitListenerContainerFactory")
public class FirstConsumer {

    @RabbitHandler
    public void process(@Payload Merchant merchant){
        System.out.println("First Queue received msg : " + merchant.getName());
    }

}
