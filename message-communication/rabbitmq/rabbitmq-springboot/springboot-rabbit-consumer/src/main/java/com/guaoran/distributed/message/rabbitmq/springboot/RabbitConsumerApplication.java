package com.guaoran.distributed.message.rabbitmq.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : guaoran
 * @Description : <br/>
 *  启动程序，会创建对象，开始监听
 * @date :2019/1/17 10:52
 */
@SpringBootApplication
public class RabbitConsumerApplication {
	public static void main(String[] args) {
		SpringApplication.run(RabbitConsumerApplication.class, args);
	}
}
