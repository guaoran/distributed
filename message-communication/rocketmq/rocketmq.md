# 安装

```shell
wget https://mirrors.tuna.tsinghua.edu.cn/apache/rocketmq/4.4.0/rocketmq-all-4.4.0-bin-release.zip


# 修改启动内存大小
vim runserver.sh 
vim runbroker.sh

# 启动
nohup sh mqnamesrv &
nohup sh mqbroker -n 192.168.45.128:9876 &

# 关闭
sh mqshutdown broker
sh mqshutdown namesrv



```





