package com.guaoran.distributed.message.rocketmq;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;


/**
 * @author : 孤傲然
 * @description : 生产者
 * @date :2019/11/3 18:47
 */
public class RocketMqProducer {

    public static void main(String[] args) throws MQClientException {

        DefaultMQProducer producer =
                new DefaultMQProducer("producer-group");
        producer.setNamesrvAddr("192.168.45.128:9876");;
        producer.start();


        int num = 0;
        while(num < 20){
            num ++;
            Message message =
                    new Message(
                            "test-topic",
                            "TAG",
                            ("hello,rocketmq:"+num).getBytes());
            try {
                SendResult send = producer.send(message);
                System.out.println(send);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
