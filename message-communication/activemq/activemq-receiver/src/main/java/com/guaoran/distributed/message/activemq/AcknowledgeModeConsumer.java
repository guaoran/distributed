package com.guaoran.distributed.message.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @author : guaoran
 * @Description : <br/>
 *  会话存在的机制
 *      事务性和非事务性
 *  acknowledgeMode
 *       static final int AUTO_ACKNOWLEDGE = 1;//自动确认
 *       static final int CLIENT_ACKNOWLEDGE = 2;//客户端手动确认
 *       static final int DUPS_OK_ACKNOWLEDGE = 3;//延迟确认，可能会出现消息重复发送
 *       static final int SESSION_TRANSACTED = 0;
 * @date :2018/11/8 14:53
 */
public class AcknowledgeModeConsumer {
    public static void main(String[] args) throws JMSException {
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(CommonUtil.CONNECT_URL);
        Connection connection = null;
        try {
            //创建连接
            connection = connectionFactory.createConnection();
            connection.start();
            //创建session会话
            Session session = connection.createSession(Boolean.TRUE,Session.CLIENT_ACKNOWLEDGE);
            /**
             * Boolean.TRUE ：表示是事务性会话，只有当执行commit()时才会将告知mq已经消费消息，则删除该消息，否则，该消息会一直存在，
             * Boolean.FALSE: 表示是非事务性会话，不存在commit和rollback，都会将消息删除
             */

            //创建目的地
            Destination destination = session.createQueue("myQueue");
            MessageConsumer consumer = session.createConsumer(destination);
            //用来接收消息
            TextMessage message = (TextMessage) consumer.receive();//如果当前没有消息，则会进行阻塞
            System.out.println(message.getText());
            //当Session session = connection.createSession(Boolean.FALSE,Session.CLIENT_ACKNOWLEDGE);
            //需要客户端手动确认签收，
            /**
             *   在多条消息中，如果该消息之前的消息都没有进行手动签收，而该消息进行手动签收，
             *   则会导致，该消息及该消息之前的消息全部会被签收，该消息之后的消息不受影响
             */
            message.acknowledge();
            session.close();

        } catch (JMSException e) {
            e.printStackTrace();
        }finally {
            if(connection != null){
                connection.close();
            }
        }
    }
}
