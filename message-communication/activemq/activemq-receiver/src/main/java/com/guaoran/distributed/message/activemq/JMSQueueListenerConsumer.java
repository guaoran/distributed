package com.guaoran.distributed.message.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @author : guaoran
 * @Description : <br/>
 *  消费端 p2p 的消息 进行 循环监听
 *  每个消息只能有一个消费者
 *  消息的生产者和消费者之间没有时间上的相关性。
 *  无论消费者在消息发送的时候是否处于运行状态，都能收到消息
 * @date :2018/11/8 14:53
 */
public class JMSQueueListenerConsumer {
    public static void main(String[] args) throws JMSException {
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(CommonUtil.CONNECT_URL);
        Connection connection = null;
        try {
            //创建连接
            connection = connectionFactory.createConnection();
            connection.start();
            //创建session会话
            Session session = connection.createSession(Boolean.TRUE,Session.AUTO_ACKNOWLEDGE);
            //创建目的地
            Destination destination = session.createQueue("myQueue");
            MessageConsumer consumer = session.createConsumer(destination);
            //用来接收消息,监听器的方式
            MessageListener listener = new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        System.out.println(((TextMessage)message).getText());
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            };
            while(true){
                //如果有消息则会循环进行消费，如果没有则循环进行监听
                consumer.setMessageListener(listener);
                session.commit();
//                session.close();
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }finally {
            if(connection != null){
                connection.close();
            }
        }
    }
}
