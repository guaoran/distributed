package com.guaoran.distributed.message.activemq;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @author : guaoran
 * @Description : <br/>
 *  会话存在的机制
 *      事务性和非事务性
 * @date :2018/11/8 16:03
 */

/*
 消息同步发送和异步发送
 默认情况下，非持久化消息是异步发送的，持久化消息且在非事务模式下是同步发送的。
 但是在开启事务的情况下，消息都是异步发送的。由于异步发送的效率会比同步发送性能更高。
    所以在发送持久化消息的时候，尽量去开启事务会话。
 除了持久化消息和非持久化消息的同步和异步特性以外，我们可以通过以下几种方式来设置异步发送
 */
public class Producer {
    public static void main(String[] args) throws JMSException {
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(CommonUtil.CONNECT_URL);
        //设置异步发送
//                new ActiveMQConnectionFactory(CommonUtil.CONNECT_URL+"?jms.useAsyncSend=true");
        //也可以这样设置异步发送
        ((ActiveMQConnectionFactory)connectionFactory).setUseAsyncSend(true);
        Connection connection = null;
        try {
            //创建连接
            connection = connectionFactory.createConnection(); //也可以这样设置异步发送
            ((ActiveMQConnection)connection).setUseAsyncSend(true);
            connection.start();
            //创建session会话
            Session session = connection.createSession(Boolean.FALSE,Session.AUTO_ACKNOWLEDGE);
            /**
             * Boolean.TRUE ：表示是事务性会话，只有当执行commit()时才会将消息提交到mq上，否则，消费端不会消费到此消息
             * Boolean.FALSE: 表示是非事务性会话，不存在commit和rollback，都会将消息提交到mq上
             */
            //创建目的地
            Destination destination = session.createQueue("myQueue");
            //创建消息发送者
            MessageProducer producer = session.createProducer(destination);
            //创建消息，并发送
            TextMessage message = session.createTextMessage("Hello world!");
            producer.send(message);
            //如果设置了 createSession(Boolean.TRUE,Session.AUTO_ACKNOWLEDGE);
            // 必须commit 否则将不会将消息提交到 Broker，消费者更不会受到
//            session.commit();

            session.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }finally {
            if(connection != null){
                connection.close();
            }
        }
    }
}
