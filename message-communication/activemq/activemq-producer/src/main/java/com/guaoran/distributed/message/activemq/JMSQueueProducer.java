package com.guaoran.distributed.message.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @author : guaoran
 * @Description : <br/>
 *  服务端 创建一个queue p2p 的消息
 *  每个消息只能有一个消费者
 *  消息的生产者和消费者之间没有时间上的相关性。
 *  无论消费者在消息发送的时候是否处于运行状态，都能收到消息
 * @date :2018/11/8 14:53
 */
public class JMSQueueProducer {
    public static void main(String[] args) throws JMSException {
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(CommonUtil.CONNECT_URL);
        Connection connection = null;
        try {
            //创建连接
            connection = connectionFactory.createConnection();
            connection.start();
            //创建session会话
            Session session = connection.createSession(Boolean.TRUE,Session.AUTO_ACKNOWLEDGE);
            //创建目的地
            Destination destination = session.createQueue("myQueue");
            //创建消息发送者
            MessageProducer producer = session.createProducer(destination);
            //创建消息，并发送
            TextMessage message = session.createTextMessage("Hello world!");
            producer.send(message);
            session.commit();
            session.close();
        } catch (JMSException e) {
            e.printStackTrace();
        }finally {
            if(connection != null){
                connection.close();
            }
        }
    }
}
