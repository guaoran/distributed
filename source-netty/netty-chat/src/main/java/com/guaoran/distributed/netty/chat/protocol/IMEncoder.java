package com.guaoran.distributed.netty.chat.protocol;

import com.guaoran.distributed.netty.chat.util.CommonUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.msgpack.MessagePack;

/**
 * @Author gucheng
 * @Description 自定义 IM 协议的编码器
 * 2019-04-19 17:23
 */
public class IMEncoder extends MessageToByteEncoder<IMMessage> {
    /**
     * 要重写的解码功能
     * @param channelHandlerContext
     * @param imMessage
     * @param byteBuf
     * @throws Exception
     */
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, IMMessage imMessage, ByteBuf byteBuf) throws Exception {
        byteBuf.writeBytes(new MessagePack().write(imMessage));
    }

    /**
     * 将自定义的内容解析为字符串
     * @param msg
     * @return
     */
    public String encode(IMMessage msg) {
        if(null == msg){ return ""; }
        String prefix = "[" + msg.getCmd() + "]" + "[" + msg.getTime() + "]";
        // 如果是登陆、聊天、鲜花 命令的话
        if(IMProtocol.LOGIN.getName().equals(msg.getCmd())
                || IMProtocol.CHAT.getName().equals(msg.getCmd())
                || IMProtocol.FLOWER.getName().equals(msg.getCmd())){
            prefix += ("[" + msg.getSender() + "]");
        }else if(IMProtocol.SYSTEM.getName().equals(msg.getCmd())){
            // 系统消息
            prefix += ("[" + msg.getOnline() + "]");
        }
        // 如果内容不为空，则拼接内容
        // [system][1234679][user] - content
        if(CommonUtil.notEmpty(msg.getContent())){
            prefix += (" - " + msg.getContent());
        }
        return prefix;
    }
}
