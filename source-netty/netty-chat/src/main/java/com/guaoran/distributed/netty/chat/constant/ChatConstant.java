package com.guaoran.distributed.netty.chat.constant;

/**
 * @author : guaoran
 * @Description 常量类
 * @date :2019/4/20 9:09
 */
public class ChatConstant {
    // 消息分隔符前缀、后缀
    public static final String MSG_SEPARATOR_PREFIX = "[";
    public static final String MSG_SEPARATOR_SUFFIX = "]";
    // 给登录者自己发送消息、非登录者
    public static final String LOGIN_SELF_SEND_MSG = "已与服务器建立连接！";
    public static final String NON_LOGIN_SELF_SEND_MSG = "加入！";
    // 自己发送消息后显示发送者
    public static final String SELF_SENDER_MSG = "you";
    // 刷花间隔时间，单位毫秒
    public static final Long BRUSH_FLOWERS_INTERVAL_MILLIS = 60 * 1000L;
    public static final String REPLACE = "${REPLACE}";
    // 刷花频繁提示
    public static final String BRUSH_FLOWERS_FREQUENTLY = "您送鲜花太频繁,"+REPLACE+"秒后再试";


}
