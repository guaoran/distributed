package com.guaoran.distributed.netty.chat.client.handler;

import com.guaoran.distributed.netty.chat.protocol.IMMessage;
import com.guaoran.distributed.netty.chat.protocol.IMProtocol;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Scanner;


/**
 * @Author gucheng
 * @Description 聊天客户端逻辑实现
 * 2019-04-19 17:46
 */
public class ChatClientHandler extends ChannelInboundHandlerAdapter {
    private Logger logger = Logger.getLogger(getClass());
    private String nickName;
    public ChatClientHandler(String nickName) {
        this.nickName = nickName;
    }
    private ChannelHandlerContext ctx;


    /**启动客户端控制台*/
    private void session() throws IOException {
        new Thread(){
            public void run(){
                logger.info(nickName + ",你好，请在控制台输入消息内容");
                IMMessage message = null;
                Scanner scanner = new Scanner(System.in);
                do{
                    if(scanner.hasNext()){
                        String input = scanner.nextLine();
                        if("exit".equals(input)){
                            message = new IMMessage(IMProtocol.LOGOUT.getName(),System.currentTimeMillis(),nickName);
                        }else{
                            message = new IMMessage(IMProtocol.CHAT.getName(),System.currentTimeMillis(),nickName,input);
                        }
                    }
                }
                while (sendMsg(message));
                scanner.close();
            }
        }.start();
    }

    /**
     * tcp链路建立成功后调用
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        this.ctx = ctx;
        IMMessage message = new IMMessage(IMProtocol.LOGIN.getName(),System.currentTimeMillis(),this.nickName);
        sendMsg(message);
        logger.info("成功连接服务器,已执行登录动作");
        session();
    }
    /**
     * 发送消息
     * @param msg
     * @return
     * @throws IOException
     */
    private boolean sendMsg(IMMessage msg){
        ctx.channel().writeAndFlush(msg);
        logger.info("已发送至聊天面板,请继续输入");
        return msg.getCmd().equals(IMProtocol.LOGOUT) ? false : true;
    }
    /**
     * 收到消息后调用
     * @throws IOException
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws IOException {
        IMMessage m = (IMMessage)msg;
        logger.info(m);
    }
    /**
     * 发生异常时调用
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.info("与服务器断开连接:"+cause.getMessage());
        ctx.close();
    }
}
