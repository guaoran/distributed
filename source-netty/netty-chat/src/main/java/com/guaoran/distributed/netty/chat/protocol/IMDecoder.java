package com.guaoran.distributed.netty.chat.protocol;

import com.guaoran.distributed.netty.chat.constant.ChatConstant;
import com.guaoran.distributed.netty.chat.util.CommonUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.msgpack.MessagePack;
import org.msgpack.MessageTypeException;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author gucheng
 * @Description 自定义 IM 协议的编码器
 * 2019-04-19 17:22
 */
public class IMDecoder extends ByteToMessageDecoder {
    //解析IM写一下请求内容的正则
    private Pattern pattern = Pattern.compile("^\\[(.*)\\](\\s\\-\\s(.*))?");

    /**
     * 要重写的编码功能
     * @param context
     * @param byteBuf
     * @param list
     * @throws Exception
     */
    @Override
    protected void decode(ChannelHandlerContext context, ByteBuf byteBuf, List<Object> list) throws Exception {
        try {
            // 获取可读字节数
            final int length = byteBuf.readableBytes();
            final byte[] bytes = new byte[length];
            String content = new String(bytes,byteBuf.readerIndex(),length);
            // 如果不为空
            if(CommonUtil.notEmpty(content)){
                if(!IMProtocol.isIMP(content)){
                    // TODO ？？？
                    context.channel().pipeline().remove(this);
                    return;
                }
            }
            byteBuf.getBytes(byteBuf.readerIndex(),bytes,0,length);
            // 添加处理类是 IMMessage 类
            list.add(new MessagePack().read(bytes,IMMessage.class));
            byteBuf.clear();
        }catch (MessageTypeException e){
            context.channel().pipeline().remove(this);
        }
    }

    /**
     * 根据字符串内容封装为消息对象
     * @param message
     * @return
     */
    public IMMessage decode(String message) {
        if(CommonUtil.isEmpty(message)) return null;
        try {
            Matcher m = pattern.matcher(message);
            String header = "";
            String content = "";
            if(m.matches()){
                header = m.group(1);
                content = m.group(3);
            }
            String [] headers = header.split("\\]\\[");
            long time = 0;
            try {
                time = Long.parseLong(headers[1]);
            }catch (Exception e){
            }
            String nickName = headers[2];
            // 昵称最多保留10个字
            nickName = CommonUtil.substring(nickName,10);

            // 根据消息的指令类型进行分别处理
            // 如果是消息类型，会告诉其他客户端 who 发送了 what 内容
            if (message.startsWith(ChatConstant.MSG_SEPARATOR_PREFIX+IMProtocol.CHAT+ChatConstant.MSG_SEPARATOR_SUFFIX)){
                return new IMMessage(headers[0],time,nickName,content);
            }

            // 登录功能，会告诉其他客户端 nickName 登录了
            else if (message.startsWith(ChatConstant.MSG_SEPARATOR_PREFIX+IMProtocol.LOGIN+ChatConstant.MSG_SEPARATOR_SUFFIX)){
                return new IMMessage(headers[0], time, nickName);
            }

            // 发送鲜花功能
            else if (message.startsWith(ChatConstant.MSG_SEPARATOR_PREFIX+IMProtocol.FLOWER+ChatConstant.MSG_SEPARATOR_SUFFIX)) {
                return new IMMessage(headers[0], time, nickName);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
