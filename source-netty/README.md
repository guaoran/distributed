# 目录

-   [Netty](#netty)
    -   [Java NIO（Non-Block IO）](#java-nionon-block-io)
        -   [缓冲区 Buffer](#缓冲区-buffer)
        -   [通道 Channel](#通道-channel)
    -   [Netty 简介](#netty-简介)
    -   [BIO 、NIO、 AIO](#bio-nio-aio)
        -   [BIO 同步阻塞模型](#bio-同步阻塞模型)
        -   [NIO 同步非阻塞模型](#nio-同步非阻塞模型)
        -   [Unix 网络编程的5种I/O模型](#unix-网络编程的5种io模型)
            -   [阻塞I/O](#阻塞io)
            -   [非阻塞I/O](#非阻塞io)
            -   [I/O 复用](#io-复用)
            -   [信号驱动的I/O](#信号驱动的io)
            -   [异步I/O](#异步io)
    -   [NIO](#nio)
        -   [NIO 核心组件](#nio-核心组件)


# Netty

## Java NIO（Non-Block IO）

在 NIO 中有几个核心对象需要掌握：缓冲区（Buffer）、通道（Channel）、选择器（Selector）

### 缓冲区 Buffer

缓冲区实际上是一个容器对象，更直接的说，其实就是一个数组，在 NIO 库中，所有数据都是用缓冲区处理的。在读取数据时，它是直接读到缓冲区中的； 在写入数据时，它也是写入到缓冲区中的；任何时候访问 NIO 中的数据，都是将它放到缓冲区中。而在面向流 I/O系统中，所有数据都是直接写入或者直接将数据读取到 Stream 对象中。在 NIO 中，所有的缓冲区类型都继承于抽象类 Buffer，最常用的就是 ByteBuffer，对于 Java
中的基本类型，基本都有一个具体 Buffer 类型与之相对应，它们之间的继承关系如下图所
示：

![1555653041585](assets/1555653041585.png)

缓冲区对象本质上是一个数组，但它其实是一个特殊的数组，缓冲区对象内置了一些机制，能够跟踪和记录缓冲区的状态变化情况，如果我们使用 get()方法从缓冲区获取数据或者使用 put()方法把数据写入缓冲区，都会引起缓冲区状态的变化。

在缓冲区中，最重要的属性有下面三个，它们一起合作完成对缓冲区内部状态的变化跟踪：

* position：指定了下一个将要被写入或者读取的元素索引，它的值由 get()/put()方法自动更新，在
  新创建一个 Buffer 对象时，position 被初始化为 0。

* limit：指定还有多少数据需要取出(在从缓冲区写入通道时)，或者还有多少空间可以放入数据(在从
  通道读入缓冲区时)。

* capacity：指定了可以存储在缓冲区中的最大数据容量，实际上，它指定了底层数组的大小，或者至
  少是指定了准许我们使用的底层数组的容量。

以上三个属性值之间有一些相对大小的关系：0 <= position <= limit <= capacity。如果我们创建一个新的容量大小为 10 的 ByteBuffer 对象，在初始化的时候，position 设置为 0，limit 和 capacity被设置为 10，在以后使用 ByteBuffer 对象过程中，capacity 的值不会再发生变化，而其它两个个将会随着使用而变化。四个属性值分别如图所示

**`init`** 

```java
// 分配了10个长度的 int 数组
IntBuffer buffer = IntBuffer.allocate(10);
```

![1555653476544](assets/1555653476544.png)

**`put`**

```java
for (int i = 0; i < 4; i++) {
    buffer.put(i);
}
```

如果我们写入只写入4个数据时，则此时 position 的值为 4，即下一个将要被写入的字节索引为 4，而 limit仍然是 10，如下图所示：

![1555654264685](assets/1555654264685.png)

**`flip`**

```java
// 重设缓冲区
buffer.flip();
```

当我们需要读取数据时，需要调用  `flip()` 方法，该方法做了两件事

1. 把 limit 设置为当前的 position 值，
2. 把 position 设置为0

由于 position 被设置为 0，所以可以保证在下一步输出时读取到的是缓冲区中的第一个字节，而 limit 被设置为当前的 position，可以保证读取的数据正好是之前写入到缓冲区中的数据，如下图所示：

![1555654711190](assets/1555654711190.png)

**`get`**

```java
while (buffer.hasRemaining()){
    System.out.print(buffer.get()+"\t");
}
```

当循环调用 get() 方法从缓冲区中读取数据写入到输出通道，这会导致 position 的增加而 limit 保持不变，但 position 不会超过 limit 的值，所以在读取我们之前写入到缓冲区中的 4 个自己之后，position 和 limit 的值都为 4，如下图所示：

![1555654907941](assets/1555654907941.png)

**`clear`**

```java
buffer.clear();
```

在从缓冲区中读取数据完毕后，limit 的值仍然保持在我们调用 flip()方法时的值，调用 clear()方法能够把所有的状态变化设置为初始化时的值，（但是并没有清理数据），如下图所示：

![1555655442610](assets/1555655442610.png)

完整代码：

```java
public static void main(String[] args) {
    // 分配10 个 int
    IntBuffer buffer = IntBuffer.allocate(10);
    System.out.println(buffer.mark());//pos=0 lim=10 cap=10
    for (int i = 0; i < 4; i++) {
        buffer.put(i);
    }
    System.out.println(buffer.mark());//pos=4 lim=10 cap=10
    buffer.flip();
    System.out.println(buffer.mark());//pos=0 lim=4 cap=10
    while (buffer.hasRemaining()){
        System.out.print(buffer.get()+"\t");//0	1	2	3
    }
    System.out.println("\n"+buffer.mark());//pos=4 lim=4 cap=10
    buffer.clear();
    System.out.println(buffer.mark());//pos=0 lim=10 cap=10
    while (buffer.hasRemaining()){
        // 0	1	2	3	0	0	0	0	0	0
        System.out.print(buffer.get()+"\t");
    }
    System.out.println("\n"+buffer.mark());//pos=10 lim=10 cap=10
}
```

### 通道 Channel

通道是一个对象，通过它可以读取和写入数据，当然了所有数据都通过 Buffer 对象来处理。我们永远不会将字节直接写入通道中，相反是将数据写入包含一个或者多个字节的缓冲区。同样不会直接从通道中读取字节，而是将数据从通道读入缓冲区，再从缓冲区获取这个字节。在 NIO 中，提供了多种通道对象，而所有的通道对象都实现了 Channel 接口。





## Netty 简介

1. 异步事件驱动框架，用于快速开发高性能服务端和客户端
2. 封装了JDK底层BIO和NIO模型，提高高度可用的API
3. 自带编码解码器解决拆包粘包问题，用户只用关系业务逻辑
4. 精心设计的Reactor线程模型支持高并发海量连接
5. 自带协议栈，无需用户关心

## BIO 、NIO、 AIO

阻塞和非阻塞：是对于应用程序而言的

同步和异步：是对于服务端处理而言的

同步和异步的区别在于：数据访问的时候进程是否阻塞

阻塞和非阻塞的区别在于：应用程序的调用是否立即返回

### BIO 同步阻塞模型

* 1:1 ：一个请求对应一个线程
  * 缺点：
    * 阻塞，会创建多个线程，
    * 随着并发访问量增大，系统性能会急剧下降
    * 线程越多，对于CPU 会频繁切换上下文
    * ![1548315421546](assets/1548315421546.png)
* M:N：请求来了之后通过线程池来创建线程
  * 缺点：
    * 阻塞，线程池满了会等待
    * ![1548315444538](assets/1548315444538.png)

### NIO 同步非阻塞模型

* NIO + 单线程 Reactor 模式
  * ![1548315621701](assets/1548315621701.png)
* NIO + 多线程 Reactor 模式
  * ![1548315648034](assets/1548315648034.png)
* NIO + 主从多线程 Reactor 模式
  - ![1548315698265](assets/1548315698265.png)

### Unix 网络编程的5种I/O模型

#### 阻塞I/O

![1548315966315](assets/1548315966315.png)

#### 非阻塞I/O

![1548316008846](assets/1548316008846.png)

#### I/O 复用

![1548316037131](assets/1548316037131.png)

#### 信号驱动的I/O 

![1548316058423](assets/1548316058423.png)

#### 异步I/O

![1548316090426](assets/1548316090426.png)

## NIO

### NIO 核心组件

* channel : 面向buffer 的通道

```sequence
channelA -> buffer :
channelB -> buffer  :
```

* buffer
  * capacity
  * position
  * limit
  * mark

* selector
* unsafe

