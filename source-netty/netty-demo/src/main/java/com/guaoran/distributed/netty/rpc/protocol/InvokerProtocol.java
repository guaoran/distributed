package com.guaoran.distributed.netty.rpc.protocol;

import lombok.Data;

import java.io.Serializable;

/**
 * @author : 孤傲然
 * @description : invoker
 * @date :2020/8/19 11:41
 */
@Data
public class InvokerProtocol implements Serializable {
    private String className; // 服务名
    private String methodName; // 方法名
    private Class<?> [] params; // 形参列表
    private Object[] values; // 实参列表

}
