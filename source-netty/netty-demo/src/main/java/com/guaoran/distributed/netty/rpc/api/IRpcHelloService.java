package com.guaoran.distributed.netty.rpc.api;

/**
 * @author : 孤傲然
 * @description : rpc
 * @date :2020/8/19 11:38
 */
public interface IRpcHelloService {
    String sayHello(String message);
}
