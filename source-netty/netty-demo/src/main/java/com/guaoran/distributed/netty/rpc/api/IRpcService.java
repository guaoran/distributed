package com.guaoran.distributed.netty.rpc.api;

/**
 * @author : 孤傲然
 * @description : rpc
 * @date :2020/8/19 11:39
 */
public interface IRpcService {
    int add (int a,int b);
    int subtract (int a,int b);
    int multiply (int a,int b);
    int divide (int a,int b);
}
