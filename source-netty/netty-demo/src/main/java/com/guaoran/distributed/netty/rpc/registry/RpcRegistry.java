package com.guaoran.distributed.netty.rpc.registry;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * @author : 孤傲然
 * @description : TODO 加上描述吧
 * @date :2020/8/19 15:49
 */
public class RpcRegistry {
    int port;
    public RpcRegistry(int port) {
        this.port = port;
    }

    public void start(){
        ServerBootstrap server = new ServerBootstrap();
        EventLoopGroup parent = new NioEventLoopGroup();
        EventLoopGroup child = new NioEventLoopGroup();
        server.group(parent,child)
        .channel(NioServerSocketChannel.class)
        .childHandler(new ChannelInitializer<SocketChannel>() {

            @Override
            protected void initChannel(SocketChannel ch) throws Exception {
                System.out.println(ch);

            }
        });

    }

    public static void main(String[] args) {
        new RpcRegistry(8080).start();
    }
}
