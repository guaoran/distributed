package com.guaoran.distributed.io.nio.one.buffer;

import java.nio.IntBuffer;

/**
 * @Author gucheng
 * @Description Buffer
 * 2019-04-19 14:00
 */
public class BufferDemo {
    public static void main(String[] args) {
        // 分配10 个 int
        IntBuffer buffer = IntBuffer.allocate(10);
        System.out.println("init\t\t"+buffer.mark());//pos=0 lim=10 cap=10
        for (int i = 0; i < 4; i++) {
            buffer.put(i);
        }
        System.out.println("put after\t"+buffer);//pos=4 lim=10 cap=10
        System.out.println("put after\t"+buffer.mark());//pos=4 lim=10 cap=10
        // 翻转
        buffer.flip();
        System.out.println("read before\t"+buffer.mark());//pos=0 lim=4 cap=10
        while (buffer.hasRemaining()){
            System.out.print(buffer.get()+"\t");//0	1	2	3
        }
        System.out.println();
        System.out.println("read after\t"+buffer.mark());//pos=4 lim=4 cap=10
        // 清除状态
        buffer.clear();
        System.out.println("clear after\t"+buffer.mark());//pos=0 lim=10 cap=10
        while (buffer.hasRemaining()){
            // 0	1	2	3	0	0	0	0	0	0
            System.out.print(buffer.get()+"\t");
        }
        System.out.println();
        System.out.println(buffer.mark());//pos=10 lim=10 cap=10
    }
}
