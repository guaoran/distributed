package com.guaoran.distributed.io.nio.one.buffer;

import java.nio.IntBuffer;

/**
 * @Author gucheng
 * @Description IntBuffer
 * 2019-04-19 10:12
 */
public class IntBufferDemo {
    public static void main(String[] args) {
        // 分配10个长度的int buffer 数组
        IntBuffer buffer = IntBuffer.allocate(10);
        System.out.println("buffer.put() before  -> position:"+buffer.position()+",limit:"+buffer.limit()+",capacity:"+buffer.capacity());
        for (int i = 0; i < 4; i++) {
            // 将int 值存入到 buffer 中
            buffer.put(i);
        }
        // position <= limit <= capacity
        System.out.println("buffer.flip() before -> position:"+buffer.position()+",limit:"+buffer.limit()+",capacity:"+buffer.capacity());
        // 重设此缓冲区，将限制设置为当前位置，然后将当前位置设置为0
        // 固定缓冲区中的某些值，告诉缓冲区，
        // 我要开始操作了，如果你再往缓冲区写数据的话
        //todo  【不要再覆盖我固定状态以前的数据了】 这句话好像不是很对，
        // TODO 在尝试修改值得时候还是可以修改的
        buffer.flip();

        System.out.println("buffer.flip() after  -> position:"+buffer.position()+",limit:"+buffer.limit()+",capacity:"+buffer.capacity());
        // 可以知道在 buffer 中存在四个变量值， mark,position,limit,capacity
        // 当buffer 初始化时，position 值为0，limit 和 capacity 是分配的容量值
        // 当buffer 填充值时，position 值为已填充的数量（或是下一个要填充的位置），limit 和 capacity 位置还是分配的容量值
        // 当buffer 进行flip操作时，position 值为0，limit 的值代表是已经填充的数量值，capacity 是最大容量值（分配的容量值）

        while(buffer.hasRemaining()){
            // 尝试修改下标为1 的元素值
            // buffer.put(1,99);
            System.out.print(buffer.get()+"\t");
        }
        System.out.println();
        System.out.println("buffer.get() after   -> position:"+buffer.position()+",limit:"+buffer.limit()+",capacity:"+buffer.capacity());
        buffer.clear();
        System.out.println("buffer.clear() after -> position:"+buffer.position()+",limit:"+buffer.limit()+",capacity:"+buffer.capacity());

//        // 尝试修改下标为1 的元素值
//        buffer.put(1,99);
//        System.out.println("\n===============================");
//
//        for (int i = 0; i < buffer.limit(); i++) {
//            System.out.print(buffer.get(i)+"\t");
//        }
//        System.out.println("\n===============================");
//        // 尝试修改下标为1 的元素值
//        buffer.put(1,99);
//        for (int i = 0; i < buffer.limit(); i++) {
//            System.out.print(buffer.get(i)+"\t");
//        }
    }
}
