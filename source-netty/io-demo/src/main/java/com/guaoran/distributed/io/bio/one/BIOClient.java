package com.guaoran.distributed.io.bio.one;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

/**
 * @author : guaoran
 * @Description BIO client
 * @date :2019/4/18 20:46
 */
public class BIOClient {
    public void send(){
        try {
            Socket socket = new Socket("127.0.0.1",8080);
            OutputStream os = socket.getOutputStream();
            os.write(UUID.randomUUID().toString().getBytes());
            os.flush();
            InputStream is = socket.getInputStream();
            byte[] bytes = new byte[1024];
            int len = is.read(bytes);
            if(len>0){
                System.out.println("server 回复信息："+new String(bytes,0,len));
            }
            os.close();
            is.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        BIOClient client = new BIOClient();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                try {
                    countDownLatch.await();
                    client.send();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        countDownLatch.countDown();

    }
}
