package com.guaoran.distributed.io.nio.two.channel;


import java.io.*;
import java.nio.channels.FileChannel;

/**
 * @Author gucheng
 * @Description 测试文件复制
 * @DESC:
 * @@1. 当 文件大小 比较小的时候，适合使用IO 方式，
 * @@2. 当文件大小比较大的时候，channel 方式的性能比较高
 * @@@ fileSize = 10M 时， IO：78s; channel: 8.5s
 * 2019-04-30 14:10
 */
public class FileCopyTest {


    public static void fileCopyWithIO(File fromFile, File toFile){
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(fromFile);
            fos = new FileOutputStream(toFile);

            byte[] bytes = new byte[1024];
            int i;
            // 读取到输入流数据，然后写入到输出流中去，实现复制
            while ((i = fis.read(bytes)) != -1) {
                fos.write(bytes, 0, i);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(fos != null){
                    fos.close();
                }
                if(fis != null){
                    fis.close();
                }
            }catch (IOException e){}
        }
    }
    public static void fileCopyWithChannel(File fromFile ,File toFile){
        FileOutputStream fos = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fromFile);
            fos = new FileOutputStream(toFile);
            // 得到 fileInputStream 的文件通道
            FileChannel fisChannel = fis.getChannel();
            // 得到 fileOutputStream 的文件通道
            FileChannel fosChannel = fos.getChannel();

            // 将 fisChannel 通道的数据，写入到 fosChannel 通道
            fisChannel.transferTo(0,fisChannel.size(),fosChannel);

            fosChannel.close();
            fisChannel.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                if(fos != null){
                    fos.close();
                }
                if(fis != null){
                    fis.close();
                }
            }catch (IOException e){}
        }
    }

    public static void main(String[] args) {
        // 当 文件大小 比较小的时候，适合使用IO 方式，
        // 当文件大小比较大的时候，channel 方式的性能比较高
        // fileSize = 10M 时， IO：78s; channel: 8.5s
        File fromFile = new File("F:\\fromFile.pdf");
        File toFile = new File("F:\\toFile.pdf");

        fileCopyWithIO(fromFile,toFile);
        fileCopyWithChannel(fromFile,toFile);

        // 计时
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            fileCopyWithIO(fromFile, toFile);
        }
        System.out.println("fileCopyWithIO time: " + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            fileCopyWithChannel(fromFile, toFile);
        }
        System.out.println("fileCopyWithChannel time: " + (System.currentTimeMillis() - start));







    }
}
