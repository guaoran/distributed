package com.guaoran.source.zookeeper.demo;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.*;
import org.apache.curator.retry.ExponentialBackoffRetry;

import java.io.IOException;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/12 19:13
 */
public class CuratorWatchDemo {
    public static void main(String[] args) throws Exception {
        CuratorFramework curatorFramework =
                CuratorFrameworkFactory.
                        builder().
                        connectString("192.168.45.131:2181,192.168.45.133:2181,192.168.45.134:2181").
                        sessionTimeoutMs(4000).
                        retryPolicy(new ExponentialBackoffRetry(1000,3)).
                        namespace("curator").build();

        //启动
        curatorFramework.start();
        System.out.println("开始监听。。。。。。。。。。");
        //监听当前节点的更新和创建事件
//        addListenWithNodeCache(curatorFramework,"/guaoran/demo");
        //监听某个节点下的子节点的添加、更新和删除操作
//        addListenWithPathChildrenChche(curatorFramework,"/guaoran/demo");
        addListenWithTreeChche(curatorFramework,"/guaoran/demo");
        System.in.read();
    }

    /**
     * 监听当前节点、所有子节点以及子节点的子节点..的创建、更新和删除操作
     * @param curatorFramework
     * @param path
     * @throws Exception
     */
    private static void addListenWithTreeChche(CuratorFramework curatorFramework, String path) throws Exception {
        TreeCache treeCache = new TreeCache(curatorFramework,path);
        TreeCacheListener treeCacheListener = new TreeCacheListener() {
            @Override
            public void childEvent(CuratorFramework curatorFramework, TreeCacheEvent treeCacheEvent) throws Exception {
                System.out.println("TreeChche..."+treeCacheEvent.getType()+"-->"+treeCacheEvent.getData().getPath());
            }
        };
        treeCache.getListenable().addListener(treeCacheListener);
        treeCache.start();
    }
    /**
     * 监听某个节点下的子节点的添加、更新和删除操作
     * @param curatorFramework
     * @param path
     * @throws Exception
     */
    private static void addListenWithPathChildrenChche(CuratorFramework curatorFramework, String path) throws Exception {
        PathChildrenCache pathChildrenCache = new PathChildrenCache(curatorFramework,path,true);
        PathChildrenCacheListener pathChildrenCacheListener = new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
                System.out.println("PathChildrenCache..."+pathChildrenCacheEvent.getType()+":"+pathChildrenCacheEvent.getData().getPath());
            }
        };
        pathChildrenCache.getListenable().addListener(pathChildrenCacheListener);
        pathChildrenCache.start();
//        pathChildrenCache.start(PathChildrenCache.StartMode.NORMAL);
    }

    /**
     * 监听当前节点的更新和创建事件
     * @param curatorFramework
     * @param path
     */
    private static void addListenWithNodeCache(CuratorFramework curatorFramework,String path) throws Exception {
        NodeCache nodeCache = new NodeCache(curatorFramework,path,false);
        NodeCacheListener nodeCacheListener = new NodeCacheListener() {
            @Override
            public void nodeChanged() throws Exception {
                System.out.println("NodeCache..."+nodeCache.getCurrentData().getPath());
            }
        };
        nodeCache.getListenable().addListener(nodeCacheListener);
        nodeCache.start();

    }
}
