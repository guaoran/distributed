package com.guaoran.source.zookeeper.lock;

import org.apache.curator.RetryPolicy;
import org.apache.curator.RetrySleeper;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.locks.InterProcessMutex;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * @author : 孤傲然
 * @Description :
 *  curator 实现分布式锁
 * @date :2018/6/13 10:37
 */
public class CuratorLockDemo {
    public static void main(String[] args) throws Exception {
        CuratorFramework curatorFramework =
                CuratorFrameworkFactory.
                        builder().
                        connectString("192.168.45.131:2181,192.168.45.133:2181,192.168.45.134:2181").
                        sessionTimeoutMs(4000).
                        retryPolicy(new ExponentialBackoffRetry(1000,3)).
                        namespace("curator").build();
        InterProcessMutex interProcessMutex = new InterProcessMutex(curatorFramework,"/lock");
        interProcessMutex.acquire();
    }
}
