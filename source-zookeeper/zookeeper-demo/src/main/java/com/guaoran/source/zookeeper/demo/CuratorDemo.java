package com.guaoran.source.zookeeper.demo;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/12 18:48
 */
public class CuratorDemo {
    public static void main(String[] args) {
        CuratorFramework curatorFramework =
                CuratorFrameworkFactory.
                        builder().
                        connectString("192.168.45.131:2181,192.168.45.133:2181,192.168.45.134:2181").
                        sessionTimeoutMs(4000).
                        retryPolicy(new ExponentialBackoffRetry(1000,3)).
                        namespace("curator").build();

        //启动
        curatorFramework.start();
        try {
//            curatorFramework.
//                    create().
//                    creatingParentsIfNeeded().
//                    withMode(CreateMode.PERSISTENT).
//                    forPath("/guaoran/node","demo".getBytes());
//            Stat stat = new Stat();
//            byte[] bytes = curatorFramework.getData().storingStatIn(stat).forPath("/guaoran/node");
//            System.out.println(new String(bytes));
//            stat = curatorFramework.setData().withVersion(stat.getVersion()).forPath("/guaoran/node", "update".getBytes());
            //删除子节点
            curatorFramework.delete().deletingChildrenIfNeeded().forPath("/guaoran");
//            curatorFramework.delete().deletingChildrenIfNeeded().withVersion(stat.getVersion()).forPath("/guaoran/node");


            curatorFramework.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
