package com.guaoran.source.rpc;

import com.guaoran.source.curatorzk.IRegisterCenter;
import com.guaoran.source.curatorzk.RegisterCenterImpl;

import java.io.IOException;


/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 16:50
 */
public class RpcServerDemo {
    public static void main(String[] args) throws IOException {
        ISendMessageService sendMessageService = new SendMessageServiceImpl();
        ISendMessageService sendMessageService2 = new SendMessageServiceImpl2();
        IRegisterCenter registerCenter = new RegisterCenterImpl();
        RpcRegisterServer registerServer = new RpcRegisterServer("127.0.0.1:8888",registerCenter);
        registerServer.bind(sendMessageService,sendMessageService2);
        registerServer.publisher();
        System.in.read();
    }
}
