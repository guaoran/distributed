package com.guaoran.source.rmi.rcp;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 13:55
 */
public class HelloServiceImpl implements IHelloService {
    @Override
    public String sayHello(String msg) {
        System.out.println("客户端正在请求");
        return "Hello:"+msg;
    }
}
