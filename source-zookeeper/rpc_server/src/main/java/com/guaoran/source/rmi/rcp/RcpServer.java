package com.guaoran.source.rmi.rcp;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 13:57
 */
public class RcpServer {
    //创建一个不限制最大线程数
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    /**
     * 发布服务
     *  将HelloServiceImpl发布出来
     * @param service
     * @param port
     */
    public void publish(final Object service,int port){
        ServerSocket serverSocket = null;

        try{
            //启动服务监听
            serverSocket = new ServerSocket(port);
            while (true){
                //将服务放到线程中去处理
                Socket socket = serverSocket.accept();
                executorService.execute(new ProcessThread(socket,service));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(serverSocket != null){
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
