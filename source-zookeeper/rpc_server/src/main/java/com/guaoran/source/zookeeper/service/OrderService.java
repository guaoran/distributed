package com.guaoran.source.zookeeper.service;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 12:19
 */
public interface OrderService {
    /**
     * 订单支付
     * @param state
     * @return
     */
    public String payOrder(String state);
}
