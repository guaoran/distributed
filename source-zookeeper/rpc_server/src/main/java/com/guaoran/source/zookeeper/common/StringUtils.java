package com.guaoran.source.zookeeper.common;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 12:31
 */
public class StringUtils {
    /**
     * 是否为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){
        if(str==null) {
            return true;
        }
        if("".equals(str.trim())) {
            return true;
        }
        return false;
    }
    /**
     * 是否非空
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str){
        return !isEmpty(str);
    }
}
