package com.guaoran.source.rmi.demo;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 13:08
 */
public class SayHelloImpl extends UnicastRemoteObject implements ISayHello {
    protected SayHelloImpl() throws RemoteException {
        super();
    }

    @Override
    public String sayHello(String msg) throws RemoteException {
        System.out.println("客户端正在调用");
        return "hello："+msg;
    }
}
