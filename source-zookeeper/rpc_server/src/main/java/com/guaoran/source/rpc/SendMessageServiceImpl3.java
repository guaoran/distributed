package com.guaoran.source.rpc;

import com.guaoran.source.anno.RpcAnnotation;

/**
 * @author : 孤傲然
 * @Description :
 *  接口实现类
 * @date :2018/6/13 15:05
 */
@RpcAnnotation(value = ISendMessageService.class)
public class SendMessageServiceImpl3 implements ISendMessageService {
    @Override
    public String sendMessage(String message) {
        System.out.println(System.currentTimeMillis()+"-->客户端正在调用服务发送消息");
        return "8889:"+message;
    }
}
