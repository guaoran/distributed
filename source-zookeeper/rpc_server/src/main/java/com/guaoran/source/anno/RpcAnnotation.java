package com.guaoran.source.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 18:31
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RpcAnnotation {
    /**
     * 对外发布的服务的接口地址
     * @return
     */
    Class<?> value();
    String version() default "";

}
