package com.guaoran.source.rmi.demo;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 13:12
 */
public class ServerDemo {
    public static void main(String[] args) throws RemoteException, MalformedURLException {
        ISayHello sayHello = new SayHelloImpl();
        LocateRegistry.createRegistry(1099);
        Naming.rebind("rmi://127.0.0.1/hello",sayHello);
        System.out.println("服务启动成功");
    }
}
