package com.guaoran.source.zookeeper.service.impl;

import com.guaoran.source.zookeeper.anno.RpcAnnotation;
import com.guaoran.source.zookeeper.service.OrderService;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 12:20
 */
@RpcAnnotation(OrderService.class)
public class OrderService2Impl implements OrderService {
    @Override
    public String payOrder(String state) {
        System.out.println("订单支付中...state="+state);
        return "8082--》订单支付成功-->"+state;
    }
}
