package com.guaoran.source.rpc;

import java.io.Serializable;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 15:30
 */
public class RpcRequest implements Serializable{
    private static final long serialVersionUID = -2868533126197871078L;
    private String className;
    private Object[] params;
    private String methodName;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    private String version;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
