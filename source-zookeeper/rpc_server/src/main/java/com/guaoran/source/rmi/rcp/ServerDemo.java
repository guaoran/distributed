package com.guaoran.source.rmi.rcp;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 14:58
 */
public class ServerDemo {
    public static void main(String[] args) {
        IHelloService helloService = new HelloServiceImpl();
        RcpServer server = new RcpServer();
        server.publish(helloService,8888);
        System.out.println("服务启动成功");
    }
}
