package com.guaoran.source.rpc;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;
import java.util.Map;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 15:20
 */
public class ProcessThread implements Runnable {
    private Map<String,Object> registerCenterMap;
    private Socket socket;

    public ProcessThread(Map<String,Object> registerCenterMap, Socket socket) {
        this.registerCenterMap = registerCenterMap;
        this.socket = socket;
    }

    @Override
    public void run() {
        //处理客户端的请求
        ObjectOutputStream outputStream = null;
        ObjectInputStream inputStream = null;
        try {
            //获得客户端的请求
            inputStream = new ObjectInputStream(socket.getInputStream());
            //将请求转换成RpcRequest请求信息
            RpcRequest request = (RpcRequest) inputStream.readObject();
            //拿到客户端的请求信息，反射去调用方法
            Object result = invoke(request);
            //将执行结果通过传输返回给客户端
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeObject(result);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            if(outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * 通过反射去调用方法
     * @param request
     * @return
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public Object invoke(RpcRequest request) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Object[] args = request.getParams();
        Class<?>[] types = new Class[args.length];
        for (int i = 0;i<types.length;i++){
            types[i] = args[i].getClass();
        }
        String serviceName = request.getClassName();
        String version = request.getVersion();
        if(version != null && !"".equals(version)){
            serviceName +="-"+version;
        }

        System.out.println("正在执行调用方法"+request.getMethodName());
        Object service = registerCenterMap.get(serviceName);
        Method method = service.getClass().getMethod(request.getMethodName(),types);
        return method.invoke(service,args);
    }
}
