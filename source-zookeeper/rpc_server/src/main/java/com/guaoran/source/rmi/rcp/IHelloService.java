package com.guaoran.source.rmi.rcp;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 13:55
 */
public interface IHelloService {
    public String sayHello(String msg);
}
