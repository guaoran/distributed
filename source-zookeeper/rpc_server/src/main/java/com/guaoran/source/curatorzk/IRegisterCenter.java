package com.guaoran.source.curatorzk;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 18:08
 */
public interface IRegisterCenter {
    /**
     * 注册中心
     * @param serviceName :服务名称
     * @param serviceAddress :服务地址
     */
    public void register(String serviceName, String serviceAddress);
}
