package com.guaoran.source.zookeeper.common;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 12:49
 */
public interface ServiceRegisterCenter {
    /**
     * 注册服务
     * @param serviceName：服务名称
     * @param serviceAddress：服务地址
     */
    public void registerCenter(String serviceName,String serviceAddress);
}
