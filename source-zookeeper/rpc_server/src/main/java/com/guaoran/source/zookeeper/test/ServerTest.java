package com.guaoran.source.zookeeper.test;

import com.guaoran.source.zookeeper.common.ServiceRegisterCenter;
import com.guaoran.source.zookeeper.common.ServiceRegisterCenterImpl;
import com.guaoran.source.zookeeper.config.OrderServerConfig;
import com.guaoran.source.zookeeper.service.OrderService;
import com.guaoran.source.zookeeper.service.impl.OrderServiceImpl;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 12:12
 */
public class ServerTest {
    public static void main(String[] args) {
        //服务注册中心
        ServiceRegisterCenter serviceRegisterCenter = new ServiceRegisterCenterImpl();
        //实例化订单服务配置中心
        OrderServerConfig orderServerConfig = new OrderServerConfig("127.0.0.1:8088",serviceRegisterCenter);
        OrderService orderService = new OrderServiceImpl();
        //注册并发布服务
        orderServerConfig.registerCenter(orderService);
        orderServerConfig.publisherServer();
    }
}
