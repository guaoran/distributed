package com.guaoran.source.zookeeper.config;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 12:13
 */
public class ZookeeperConfig {
    public final static String CONNECTION_STR = "192.168.45.131:2181,192.168.45.133:2181,192.168.45.134:2181";
    public final static String ZK_REGISTER_PATH = "product/order";
    public final static int SESSION_TIMEOUT_MS = 4000;
    public final static int RETRY_POLICY_BASE_SLEEP_TIME_MS = 1000;
    public final static int RETRY_POLICY_MAX_RETRIES = 3;

    /**
     * 加载zookeeper
     * @return
     */
    public static CuratorFramework initCuratorFramework(){
        CuratorFramework curatorFramework =
                CuratorFrameworkFactory.
                builder().
                retryPolicy(new ExponentialBackoffRetry(RETRY_POLICY_BASE_SLEEP_TIME_MS,RETRY_POLICY_MAX_RETRIES)).
                sessionTimeoutMs(SESSION_TIMEOUT_MS).
                connectString(CONNECTION_STR).
                namespace(ZK_REGISTER_PATH).
                build();
        curatorFramework.start();
        return curatorFramework;
    }
}
