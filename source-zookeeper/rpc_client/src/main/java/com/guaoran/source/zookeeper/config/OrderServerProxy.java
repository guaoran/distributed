package com.guaoran.source.zookeeper.config;

import com.guaoran.source.zookeeper.common.OrderInvocationHandler;

import java.lang.reflect.Proxy;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 14:07
 */
public class OrderServerProxy {
    public <T> T getServerProxy(Class<T> clazz,String version){
        return (T) Proxy.newProxyInstance(clazz.getClassLoader(),new Class[]{clazz},new OrderInvocationHandler(version));
    }
}
