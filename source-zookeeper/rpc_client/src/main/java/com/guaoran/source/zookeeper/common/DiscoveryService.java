package com.guaoran.source.zookeeper.common;

import java.util.List;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 13:46
 */
public interface DiscoveryService {
    /**
     * 根据服务名称查询服务地址
     * @param serviceName
     * @return
     */
    public String discoveryService(String serviceName);
}
