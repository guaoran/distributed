package com.guaoran.source.zookeeper.common;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 14:09
 */
public class OrderInvocationHandler implements InvocationHandler {
    private String version;
    public OrderInvocationHandler(String version) {
        this.version = version;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //进行远程调用服务，封装参数，传输给服务端（序列化传输）
        RpcRequest request = new RpcRequest();
        request.setMethodName(method.getName());
        request.setVersion(version);
        request.setParams(args);
        request.setClassName(method.getDeclaringClass().getName());
        DiscoveryService discoveryService = new DiscoveryServiceImpl();
        String serviceName = request.getClassName();
        if(StringUtils.isNotEmpty(version)){
            serviceName += "-"+version;
        }
        String serviceAddress = discoveryService.discoveryService(serviceName);
        RpcTCPTransport tcpTransport = new RpcTCPTransport(serviceAddress);
        //将客户端的请求信息发送给服务端，请求后获得结果返回
        return tcpTransport.sendRequest(request);
    }
}
