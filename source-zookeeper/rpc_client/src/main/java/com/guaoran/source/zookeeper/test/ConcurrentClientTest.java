package com.guaoran.source.zookeeper.test;

import com.guaoran.source.zookeeper.config.OrderServerProxy;
import com.guaoran.source.zookeeper.service.OrderService;

import java.util.concurrent.CountDownLatch;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 14:39
 */
public class ConcurrentClientTest {
    public static void main(String[] args) throws InterruptedException {
        OrderServerProxy serverProxy = new OrderServerProxy();
        CountDownLatch countDownLatch = new CountDownLatch(20);
        for (int i = 0; i < 20; i++) {
            OrderService orderService = serverProxy.getServerProxy(OrderService.class,null);
            System.out.println(orderService.payOrder("OK"));
        }
    }
}
