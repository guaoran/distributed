package com.guaoran.source.zookeeper.test;

import com.guaoran.source.zookeeper.config.OrderServerProxy;
import com.guaoran.source.zookeeper.service.OrderService;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 13:44
 */
public class ClientVersionTest {
    public static void main(String[] args) {
        OrderServerProxy serverProxy = new OrderServerProxy();
        OrderService orderService = serverProxy.getServerProxy(OrderService.class,"2.0");
        System.out.println(orderService.payOrder("OK"));
    }
}
