package com.guaoran.source.rmi.rcp;

import java.io.Serializable;

/**
 * @author : 孤傲然
 * @Description :
 * 传输请求对象
 * @date :2018/6/13 14:17
 */
public class RpcRequest implements Serializable {
    private static final long serialVersionUID = -6668773910633777311L;
    private String className;
    private String methodName;
    private Object[] parameters;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getParameters() {
        return parameters;
    }

    public void setParameters(Object[] parameters) {
        this.parameters = parameters;
    }
}
