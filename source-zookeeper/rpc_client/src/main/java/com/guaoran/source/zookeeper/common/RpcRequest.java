package com.guaoran.source.zookeeper.common;

import java.io.Serializable;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 13:06
 */
public class RpcRequest implements Serializable{
    private static final long serialVersionUID = -5465286757315410307L;
    private String className;
    private String methodName;
    private Object[] params;
    private String version;

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
