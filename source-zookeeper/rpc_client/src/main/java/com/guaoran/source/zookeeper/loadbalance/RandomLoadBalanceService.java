package com.guaoran.source.zookeeper.loadbalance;

import java.util.List;
import java.util.Random;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 14:03
 */
public class RandomLoadBalanceService extends AbstractLoadBalanceServer {
    @Override
    protected String doSelect(List<String> serviceList) {
        Random random = new Random();
        int num = random.nextInt(serviceList.size());
        return serviceList.get(num);
    }
}
