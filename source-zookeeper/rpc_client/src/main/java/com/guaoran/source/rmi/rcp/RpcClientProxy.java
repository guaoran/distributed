package com.guaoran.source.rmi.rcp;

import java.lang.reflect.Proxy;

/**
 * @author : 孤傲然
 * @Description :
 *  代理接口
 * @date :2018/6/13 14:17
 */
public class RpcClientProxy {
    public <T> T clientProxy(final Class<T> interfaceCls,final String host,final int port){
        //通过接口获得代理实现类
        return (T) Proxy.newProxyInstance(interfaceCls.getClassLoader(),
                new Class[]{interfaceCls},new RpcInvocationHandler(host,port));
    }
}
