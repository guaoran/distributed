package com.guaoran.source.curatorzk;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 18:01
 */
public class ZookeeperConfig {
    public final static String CONNECTION_STR = "192.168.45.131:2181,192.168.45.133:2181,192.168.45.134:2181";
    public final static String ZK_REGISTER_PATH = "/register";
    public final static int SESSION_TIMEOUT_MS = 4000;
    public final static int RETRY_POLICY_BASE_SLEEP_TIME_MS = 1000;
    public final static int RETRY_POLICY_MAX_RETRIES = 3;

    /**
     * 实例CuratorFramework
     * @return
     */
    public static CuratorFramework initCuratorFramework(){
        CuratorFramework curatorFramework =
                CuratorFrameworkFactory.builder().
                        connectString(ZookeeperConfig.CONNECTION_STR).
                        sessionTimeoutMs(ZookeeperConfig.SESSION_TIMEOUT_MS).
                        retryPolicy(
                                new ExponentialBackoffRetry(ZookeeperConfig.RETRY_POLICY_BASE_SLEEP_TIME_MS,
                                        ZookeeperConfig.RETRY_POLICY_MAX_RETRIES)
                        ).
                        build();
        curatorFramework.start();
        return curatorFramework;
    }
}
