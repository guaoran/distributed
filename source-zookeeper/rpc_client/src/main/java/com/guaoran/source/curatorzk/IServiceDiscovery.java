package com.guaoran.source.curatorzk;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 10:10
 */
public interface IServiceDiscovery {
    /**
     * 根据服务名称获得对应的调用地址
     * @param serviceName
     * @return
     */
    public String discovery(String serviceName);
}
