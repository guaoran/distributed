package com.guaoran.source.rmi.demo;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 13:18
 */
public class ClientDemo {
    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {
        ISayHello sayHello =
                (ISayHello) Naming.lookup("rmi://127.0.0.1/hello");
        System.out.println(sayHello.sayHello("孤傲然"));
    }
}
