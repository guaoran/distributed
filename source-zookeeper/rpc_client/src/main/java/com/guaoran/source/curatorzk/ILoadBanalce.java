package com.guaoran.source.curatorzk;

import java.util.List;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 10:29
 */
public interface ILoadBanalce {
    /**
     * 根据子节点随机获取一个节点进行调用
     * @param childrenList
     * @return
     */
    String selectHost(List<String> childrenList);
}
