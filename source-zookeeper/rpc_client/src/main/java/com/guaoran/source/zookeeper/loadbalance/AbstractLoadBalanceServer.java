package com.guaoran.source.zookeeper.loadbalance;

import java.util.List;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 13:59
 */
public abstract class AbstractLoadBalanceServer implements LoadBalanceServer{
    protected abstract String doSelect(List<String> serviceList);
    @Override
    public String selectServer(List<String> serviceList) {
        if(serviceList == null || serviceList.size()==0){
            return null;
        }
        if(serviceList.size()==1){
            return serviceList.get(0);
        }
        return doSelect(serviceList);
    }
}
