package com.guaoran.source.curatorzk;

import java.util.List;
import java.util.Random;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 10:33
 */
public class RandomLoadBanalce extends AbstractLoadBanalce {
    @Override
    protected String doSelect(List<String> childrenList) {
        int len = childrenList.size();
        Random random = new Random();
        return childrenList.get(random.nextInt(len));
    }
}
