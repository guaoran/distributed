package com.guaoran.source.rpc;

/**
 * @author : 孤傲然
 * @Description :
 * 消息接口类
 * @date :2018/6/13 15:04
 */
public interface ISendMessageService {
    /**
     * 发送消息
     * @param message
     * @return
     */
    public String sendMessage(String message);
}
