package com.guaoran.source.rpc;

import com.guaoran.source.curatorzk.IServiceDiscovery;

import java.lang.reflect.Proxy;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 15:32
 */
public class RpcClientProxy {
    private IServiceDiscovery serviceDiscovery;
    private String version;

    public RpcClientProxy(IServiceDiscovery serviceDiscovery,String version) {
        this.serviceDiscovery = serviceDiscovery;
        this.version = version;
    }

    public <T> T clientProxy(final Class<T> clientInterfaceClass){
        return (T) Proxy.newProxyInstance
                (clientInterfaceClass.getClassLoader(),
                        new Class[]{clientInterfaceClass},
                        new RpcInvocationHandler(serviceDiscovery,version));
    }
}
