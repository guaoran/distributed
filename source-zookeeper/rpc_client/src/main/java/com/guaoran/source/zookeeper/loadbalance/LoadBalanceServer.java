package com.guaoran.source.zookeeper.loadbalance;

import java.util.List;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 13:58
 */
public interface LoadBalanceServer {
    /**
     * 负载均衡去查询服务
     * @return
     */
    public String selectServer(List<String> serviceList);
}
