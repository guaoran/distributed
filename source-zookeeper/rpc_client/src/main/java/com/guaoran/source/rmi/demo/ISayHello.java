package com.guaoran.source.rmi.demo;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 13:07
 */
public interface ISayHello extends Remote{
    public String sayHello(String msg) throws RemoteException;
}
