package com.guaoran.source.rmi.rcp;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 13:56
 */
public class ClientDemo {
    public static void main(String[] args) {
        RpcClientProxy clientProxy = new RpcClientProxy();
        IHelloService helloService =
                clientProxy.clientProxy(IHelloService.class,"localhost",8888);
        System.out.println(helloService.sayHello("guaoran"));
    }
}
