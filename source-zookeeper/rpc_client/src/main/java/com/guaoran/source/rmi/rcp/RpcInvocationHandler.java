package com.guaoran.source.rmi.rcp;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author : 孤傲然
 * @Description :
 * 发起客户端与服务端的远程调用
 * @date :2018/6/13 14:20
 */
public class RpcInvocationHandler implements InvocationHandler {
    private String host;
    private int port;

    public RpcInvocationHandler(String host, int port) {
        this.host = host;
        this.port = port;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //进行远程服务调用
        RpcRequest rpcRequest = new RpcRequest();
        rpcRequest.setClassName(method.getDeclaringClass().getName());
        rpcRequest.setMethodName(method.getName());
        rpcRequest.setParameters(args);
        TCPTransport tcpTransport = new TCPTransport(host,port);
        Object result = tcpTransport.send(rpcRequest);
        return result;
    }
}
