package com.guaoran.source.rpc;

import com.guaoran.source.curatorzk.IServiceDiscovery;
import com.guaoran.source.curatorzk.ServiceDiscoveryImpl;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 16:51
 */
public class RpcClientDemo {
    public static void main(String[] args) {

        //查询服务
        IServiceDiscovery serviceDiscovery = new ServiceDiscoveryImpl();
        RpcClientProxy clientProxy = new RpcClientProxy(serviceDiscovery,"v2.0");
        ISendMessageService service = clientProxy.clientProxy(ISendMessageService.class);
        System.out.println(service.sendMessage("hello"));

    }
}
