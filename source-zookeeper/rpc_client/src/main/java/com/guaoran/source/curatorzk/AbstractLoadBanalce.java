package com.guaoran.source.curatorzk;

import java.util.List;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 10:31
 */
public abstract class AbstractLoadBanalce implements ILoadBanalce{
    protected abstract String doSelect(List<String> childrenList);
    @Override
    public String selectHost(List<String> childrenList) {
        if(childrenList == null || childrenList.size() == 0){
            return null;
        }
        if(childrenList.size() == 1){
            return childrenList.get(0);
        }
        return doSelect(childrenList);
    }
}
