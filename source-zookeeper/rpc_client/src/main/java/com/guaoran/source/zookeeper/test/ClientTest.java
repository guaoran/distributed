package com.guaoran.source.zookeeper.test;

import com.guaoran.source.zookeeper.common.DiscoveryService;
import com.guaoran.source.zookeeper.common.DiscoveryServiceImpl;
import com.guaoran.source.zookeeper.config.OrderServerProxy;
import com.guaoran.source.zookeeper.service.OrderService;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 13:44
 */
public class ClientTest {
    public static void main(String[] args) {
        OrderServerProxy serverProxy = new OrderServerProxy();
        OrderService orderService = serverProxy.getServerProxy(OrderService.class,null);
        System.out.println(orderService.payOrder("OK"));
    }
}
