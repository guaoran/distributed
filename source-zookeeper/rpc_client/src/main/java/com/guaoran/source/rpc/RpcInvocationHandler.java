package com.guaoran.source.rpc;

import com.guaoran.source.curatorzk.IServiceDiscovery;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/13 16:23
 */
public class RpcInvocationHandler implements InvocationHandler {
    private IServiceDiscovery serviceDiscovery;
    private String version;
    public RpcInvocationHandler(IServiceDiscovery serviceDiscovery,String version) {
        this.serviceDiscovery = serviceDiscovery;
        this.version = version;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //进行远程调用服务，封装参数，传输给服务端（序列化传输）
        RpcRequest request = new RpcRequest();
        request.setClassName(method.getDeclaringClass().getName());
        request.setMethodName(method.getName());
        request.setParams(args);
        request.setVersion(version);
        String serviceAddress = serviceDiscovery.discovery(method.getDeclaringClass().getName());
        RpcTCPTransport tcpTransport = new RpcTCPTransport(serviceAddress);
        //将客户端的请求信息发送给服务端，请求后获得结果返回
        Object result = tcpTransport.sendToServer(request);
        return result;
    }
}
