package com.guaoran.distributed.protocol;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/5/27 22:58
 */
public class DoubleServerDemo {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = null;
        BufferedReader br = null;
        try {
            serverSocket = new ServerSocket(8080);
            Socket socket = serverSocket.accept();
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            BufferedReader sin = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("DoubleServerDemo....client:"+br.readLine());
        }finally {
            if(serverSocket != null){
                serverSocket.close();
            }
        }
    }
}
