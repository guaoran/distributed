package com.guaoran.distributed.protocol.tcp;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/5/27 19:31
 */
public class ClientSocketDemo {
    public static void main(String[] args) throws IOException {
        Socket socket = null;
        PrintWriter out = null;
        try {
            socket = new Socket("127.0.0.1",8080);
            out = new PrintWriter(socket.getOutputStream());
            out.println("ClientSocketDemo...输出：hello...");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(out != null){
                out.close();
            }
            if(socket != null){
                socket.close();
            }
        }
    }
}
