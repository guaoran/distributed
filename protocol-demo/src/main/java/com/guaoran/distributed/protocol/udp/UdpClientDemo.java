package com.guaoran.distributed.protocol.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/5/27 22:51
 */
public class UdpClientDemo {
    public static void main(String[] args) throws IOException {
        InetAddress address = InetAddress.getByName("localhost");
        byte[] sendData = "Hello world".getBytes();
        DatagramPacket sendPacket = new
                DatagramPacket(sendData,sendData.length,address,8080);
        DatagramSocket datagramSocket = new DatagramSocket();
        datagramSocket.send(sendPacket);
    }
}
