package com.guaoran.distributed.protocol.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/5/27 22:49
 */
public class UdpServerDemo {
    public static void main(String[] args) throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket(8080);
        byte [] receiveData = new byte[1024];
        DatagramPacket datagramPacket = new DatagramPacket(receiveData,receiveData.length);
        datagramSocket.receive(datagramPacket);
        System.out.println(new String(receiveData,0,receiveData.length));
    }
}
