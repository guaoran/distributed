# 目录

-   [ELK](#elk)
    -   [ElasticSearch](#elasticsearch)
        -   [基本概念](#基本概念)
        -   [安装启动](#安装启动)
        -   [集群配置](#集群配置)
            -   [快速启动一个集群节点实例](#快速启动一个集群节点实例)
            -   [集群的状态](#集群的状态)
    -   [head 、 cerebro 插件安装](#head-cerebro-插件安装)
        -   [安装 head](#安装-head)
            -   [安装 nodejs](#安装-nodejs)
            -   [安装 grunt](#安装-grunt)
            -   [启动 head](#启动-head)
        -   [安装 cerebro](#安装-cerebro)
    -   [安装 kibana 、 logstash](#安装-kibana-logstash)
        -   [kibana](#kibana)
        -   [logstash](#logstash)
    -   [ELK 网站流量可视化监控](#elk-网站流量可视化监控)
        -   [工具](#工具)
        -   [网站流量可视化监控配置](#网站流量可视化监控配置)
            -   [部署 Tomcat 应用](#部署-tomcat-应用)
            -   [配置 `logstash` 输入、解析、输出日志到
                `elasticSearch`](#配置-logstash-输入解析输出日志到-elasticsearch)
                -   [tomcat access](#tomcat-access)
                -   [nginx access](#nginx-access)
                    -   [格式一](#格式一)
                    -   [格式二](#格式二)
                -   [启动 logstash](#启动-logstash)
                -   [Logstash 与 ElasticSearch
                    集成](#logstash-与-elasticsearch-集成)
    -   [基本操作](#基本操作)
        -   [ElasticSearch操作](#elasticsearch操作)
            -   [创建索引](#创建索引)
            -   [插入数据](#插入数据)
            -   [修改文档](#修改文档)
            -   [删除文档](#删除文档)
            -   [删除索引](#删除索引)
            -   [查询语句](#查询语句)
            -   [query 条件](#query-条件)
        -   [kibana 进行视图展示](#kibana-进行视图展示)
            -   [匹配索引](#匹配索引)
                -   [查询日志插入到 ES
                    上创建的索引](#查询日志插入到-es-上创建的索引)
                -   [导入索引](#导入索引)
            -   [探索数据](#探索数据)
            -   [数据可视化](#数据可视化)
        -   [Logstash 、Kibana 时区问题](#logstash-kibana-时区问题)
            -   [logstsh 的时区问题](#logstsh-的时区问题)
                -   [复现问题](#复现问题)
                -   [解决问题](#解决问题)
            -   [kibana 的时区问题](#kibana-的时区问题)
                -   [复现问题](#复现问题-1)
                -   [解决问题](#解决问题-1)
            -   [kibana 通过 nginx
                权限认证](#kibana-通过-nginx-权限认证)


# ELK

## ElasticSearch

### 基本概念

- **索引**： 含有相同属性的文档集合。
- **类型**：索引可以定义一个或多个类型，文档必须属于一个类型。
- **文档**：文档是可以被索引的基本数据单元。
- **分片**：每个索引都有多个分片，每个分片是一个Lucene 索引。
- **备份**：拷贝一份分片就完成了分片的备份。

### 安装启动

```shell
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.1.tar.gz
tar -zxvf elasticsearch-6.5.1.tar.gz
mkdir /guaoran/elk
mv elasticsearch-6.5.1 /guaoran/elk/
```

启动

```shell
/guaoran/elk/elasticsearch-6.5.1/bin/elasticsearch
```

报错：

```shell
[root@localhost bin]# ./elasticsearch 
[2019-05-05T14:23:10,893][WARN ][o.e.b.ElasticsearchUncaughtExceptionHandler] [unknown] uncaught exception in thread [main]
org.elasticsearch.bootstrap.StartupException: java.lang.RuntimeException: can not run elasticsearch as root
```

原因 及 解决 ：

ElasticSearch 不允许 root 用户启动，所以需要单独创建一个用户来运行。

1. 创建用户 : `useradd guaoran(用户名) -g elk(所属组名) -p 123456(密码)`

   ```shell
   groupadd elk
   useradd guaoran -g elk -p 123456
   ```

2. 授权访问组权限 `chown -R guaoran(用户名):elk(所属组名) /path`

   ```shell
   chown -R guaoran:elk /guaoran/elk
   chmod -R 777 /guaoran/elk
   su guaoran
   ```

3. 修改启动参数

   ```powershell
   vi /guaoran/elk/elasticsearch-6.5.1/config/elasticsearch.yml
   ```

   绑定 ip 默认是 `127.0.0.1` ，修改为

   ```yml
   network.host: 0.0.0.0
   ```

4. 切换用户（`su guaoran`）后重新启动 ,依然报错，错误如下：

   ```shell
   ERROR: [2] bootstrap checks failed
   [1]: max file descriptors [4096] for elasticsearch process is too low, increase to at least [65536]
   [2]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
   [2019-05-05T15:30:08,742][INFO ][o.e.n.Node               ] [QKqC8YA] stopping ...
   [2019-05-05T15:30:08,763][INFO ][o.e.n.Node               ] [QKqC8YA] stopped
   [2019-05-05T15:30:08,763][INFO ][o.e.n.Node               ] [QKqC8YA] closing ...
   [2019-05-05T15:30:08,800][INFO ][o.e.n.Node               ] [QKqC8YA] closed
   [2019-05-05T15:30:08,803][INFO ][o.e.x.m.j.p.NativeController] [QKqC8YA] Native controller process has stopped - no new native processes can be started
   ```

   修改配置

   ```shell
   su root 
   echo "* soft nofile 65536" >> /etc/security/limits.conf
   echo "* hard nofile 65536" >> /etc/security/limits.conf
   echo "* soft memlock unlimited" >> /etc/security/limits.conf
   echo "* hard memlock unlimited" >> /etc/security/limits.conf
   echo "vm.max_map_count = 262144" >> /etc/sysctl.conf
   sysctl -p
   ulimit -l unlimited
   ```

5. 开放端口

   ```shell
   #添加
   firewall-cmd --zone=public --add-port=9200/tcp --permanent
   #重新载入
   firewall-cmd --reload
   #查看所有打开的端口
   firewall-cmd --zone=public --list-ports
   ```

6. 检测是否启动

   ```tex
   http://192.168.45.135:9200/
   http://192.168.45.134:9200/
   http://192.168.45.131:9200/
   ```

7. 启动后台运行

   ```shell
   /guaoran/elk/elasticsearch-6.5.1/bin/elasticsearch -d
   ```

   

### 集群配置

在 `elasticsearch.yml` 中根据角色进行添加以下内容

master：

```yaml
#集群名称
cluster.name: elasticsearch 
#节点 ID，保证唯一
node.name: master 
#标记是否为主节点
node.master: true 
network.host: 0.0.0.0
#允许跨域
http.cors.enabled: true 
http.cors.allow-origin: "*"
```

slave

```yaml
#集群名称三个节点保持一致
cluster.name: elasticsearch
#从节点 ID，保证唯一
node.name: slave-1 
#集群的 IP 组，配置主节点 IP 即可
discovery.zen.ping.unicast.hosts: ["192.168.45.135","192.168.45.134","192.168.45.131"] 
network.host: 0.0.0.0
```



#### 快速启动一个集群节点实例

```shell
/guaoran/elk/elasticsearch-6.5.1/bin/elasticsearch -Ecluster.name=my_cluster -Epath.data=my_cluster_node1 -Enode.name=node1 -Ehttp.port=5100 -d
```

#### 集群的状态 

- **green** 指所有主副分片都正常分配
- **yellow** 指所有主分片都正常分配
- **red** 有主分片未分配

三种状态只是代表分片的工作状态，并不是代表整个es集群是否能够对外提供服务



##  head 、 cerebro 插件安装

### 安装 head

#### 安装 nodejs

```shell
tar -xvf node-v10.15.3-linux-x64.tar.xz 
mv node-v10.15.3-linux-x64 /guaoran/nodejs
cd /guaoran/nodejs
ln -s /guaoran/nodejs/bin/npm /usr/local/bin/
ln -s /guaoran/nodejs/bin/node /usr/local/bin/
vim /etc/profile
export NODE_HOME=/guaoran/nodejs export PATH=$PATH:$NODE_HOME/bin export NODE_PATH=$NODE_HOME/lib/node_modules
source /etc/profile
node -v
npm -v
```

#### 安装 grunt

```shell
npm install -g cnpm --registry=https://registry.npm.taobao.org
ln -s /guaoran/nodejs/bin/cnpm /usr/local/bin/

cnpm install -g grunt-cli
ln -s /guaoran/nodejs/bin/grunt /usr/local/bin/
grunt -version
```

执行 `grunt -version` 会发现只有 `grunt-cli v1.3.2` 的版本信息，此时还需要到 `elasticsearch-head` 中 安装其他依赖。

#### 启动 head

```shell
wget https://codeload.github.com/mobz/elasticsearch-head/zip/master
unzip elasticsearch-head-master.zip 
mv elasticsearch-head-master /guaoran/elk/head
cd /guaoran/elk/head/

cnpm install

grunt server
```

访问链接

```tex
http://192.168.45.135:9100/
```



### 安装 cerebro

```shell
wget https://github.com/lmenezes/cerebro/releases/download/v0.8.3/cerebro-0.8.3.tgz
tar -zxvf cerebro-0.8.3.tgz
mv cerebro-0.8.3 /guaoran/elk/
/guaoran/elk/cerebro-0.8.3/bin/cerebro
```

访问链接

```tex
http://192.168.45.135:9000
```



## 安装 kibana  、 logstash

### kibana

```shell
wget https://artifacts.elastic.co/downloads/kibana/kibana-6.5.1-linux-x86_64.tar.gz
tar -xzf  kibana-6.5.1-linux-x86_64.tar.gz
mv kibana-6.5.1-linux-x86_64 /guaoran/elk/kibana

vi /guaoran/elk/kibana/config/kibana.yml
```

`kibana.yml` 

```yaml
elasticsearch.url: "http://192.168.45.135:9200"

server.host: "192.168.45.135"
```

启动

```shell
/guaoran/elk/kibana/bin/kibana
# 后台启动
nohup /guaoran/elk/kibana/bin/kibana &
```

访问链接

```shell
http://192.168.45.135:5601
```



### logstash

```shell
wget https://artifacts.elastic.co/downloads/logstash/logstash-6.5.1.zip
unzip logstash-6.5.1.zip 
mv logstash-6.5.1 /guaoran/elk/
```

**启动**

修改 配置文件 `logstash.yml`

```yaml
http.host: "192.168.45.135"

http.port: "9600"

xpack.monitoring.elasticsearch.url: "http://192.168.45.135:9200"
```



方式一

```shell
/guaoran/elk/logstash-6.5.1/bin/logstash -e 'input { stdin {} } output { stdout {} }'
```

方式二

```shell
vi /guaoran/elk/logstash-6.5.1/config/logstash.conf
```

输入以下内容

```con
    input {
        stdin {}
    }
    output {
        stdout {}
    }
```

启动

```shell
bin/logstash -f config/logstash.conf 
# 后台启动
nohup  bin/logstash -f config/logstash.conf >/dev/null & 
```

**检测**

```shell
http://192.168.45.135:5601/app/monitoring
```

是否出现 `logstash`



## ELK 网站流量可视化监控

![1557211141770](assets/1557211141770.png)

![1557211424202](assets/1557211424202.png)

通过上图我们可以看到，ELK 是由三个 Elastic 的产品组合而成，分别是 ElasticSearch、Logstash 和 Kibana。三者之间的部署关系如下图所示：

![1557211529269](assets/1557211529269.png)

Logstash 就好比是挖矿工，将原料采集回来存放到 ElasticSearch 这个仓库中，Kibana 再将存放在 ElasticSearch 中的原料进行加工包装成产品，输出到 web 界面。基本工作原理如下图所示

![1557211573772](assets/1557211573772.png)

### 工具

[Grok Debugger 国内镜像](https://elasticsearch.cn/article/179) 是一个常用的对日志进行结构化的一个工具，可以通过在线工具进行调试：
[http://grokdebug.herokuapp.com](http://grokdebug.herokuapp.com/)
[grok 正则匹配工具（科学上网）](http://grokconstructor.appspot.com/)

[grok国内镜像](http://grok.elasticsearch.cn)

### 网站流量可视化监控配置

#### 部署 Tomcat 应用

部署  tomcat 服务应用，将 access 日志输出到  `/guaoran/elk/webapps/logs/bigdata/`目录下

修改 tomcat 日志输出路径  `conf/server.xml`

```xml
<Host>
    <Valve className="org.apache.catalina.valves.AccessLogValve" directory="/guaoran/elk/webapps/logs/bigdata"
           prefix="localhost_access_log" suffix=".log"
           pattern="%h %l %u %t &quot;%r&quot; %s %b" />
    <Context path="/" docBase="/guaoran/elk/webapps/bigdata" reloadable="false"></Context>
</Host>
```



#### 配置 `logstash` 输入、解析、输出日志到 `elasticSearch`

##### tomcat access

创建正则配置目录 和 添加正则文件

```shell
/guaoran/elk/logstash-6.5.1/config
mkdir patterns
vi /guaoran/elk/logstash-6.5.1/config/patterns/tomcat
```

日志内容 `/guaoran/elk/webapps/logs/bigdata/localhost_access_log.2019-05-09.log`

```shell
10.30.185.250 - - [11/Apr/2019:17:59:47 +0800] "GET / HTTP/1.0" 200 11064
10.30.185.250 - - [11/Apr/2019:17:59:47 +0800] "GET /css/base.css HTTP/1.0" 200 996
10.30.185.250 - - [11/Apr/2019:17:59:47 +0800] "GET /css/common.css HTTP/1.0" 200 10564
10.30.185.250 - - [11/Apr/2019:17:59:47 +0800] "GET /css/index.css HTTP/1.0" 200 23328
```

正则内容 `/guaoran/elk/logstash-6.5.1/config/patterns/tomcat`

```tex
TOMCATACCESS %{IPORHOST:clientip} (?:-|%{USER:ident}) (?:-|%{USER:auth}) \[%{HTTPDATE:timestamp}\] \"(?:%{WORD:verb} %{NOTSPACE:request}(?: HTTP/%{NUMBER:httpversion})?|-)\" %{NUMBER:response} (?:-|%{NUMBER:bytes})
```

启动配置 `/guaoran/elk/logstash-6.5.1/config/tomcat.conf`

```tex
input {
    file {
        path => ["/guaoran/elk/webapps/logs/bigdata/localhost_access_log.*.log"]
		type => "bigdata_tomcat"
		#sincedb_path => "/dev/null"
		start_position => "beginning"
    }
}

filter {
	if [type] == "bigdata_tomcat" {
		grok {
			patterns_dir => "/guaoran/elk/logstash-6.5.1/config/patterns/"
			match => {
				"message" => "%{TOMCATACCESS}"
			}
		}
		date {
        	match => ["timestamp","dd/MMM/YYYY:HH:mm:ss Z"]
        }
    }
}

output {
	stdout {
		codec => rubydebug
	}
	elasticsearch {
		hosts => ["http://192.168.192.129:9200"]
		index => "logstash-%{type}-%{+YYYY.MM.dd}"
		document_type => "%{type}"
		sniffing => true
	}
}
```

##### nginx access

###### 格式一

日志格式 

```tex
log_format main '$host $status [$time_local] $remote_addr [$time_local] $request_uri '
                    '"$http_referer" "$http_user_agent" "$http_x_forwarded_for" '
                    '$bytes_sent $request_time $sent_http_x_cache_hit';
```

日志内容 `/guaoran/elk/webapps/logs/agdata/`

```tex
www.guaoran.cn 200 [08/May/2019:08:00:01 +0800] 127.0.0.1 [08/May/2019:08:00:01 +0800] /datarecommend/getrecommand-465212.do "-" "Apache-HttpClient/4.5.3 (Java/1.8.0_144)" "-" 811 0.011 -
www.guaoran.cn 206 [09/May/2019:03:34:18 +0800] 127.0.0.1 [09/May/2019:03:34:18 +0800] /js/jquery-1.9.1.js "http://www.guaoran.cn/client/helpInfo.html" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36" "-" 23869 0.003 -
```

启动配置 `/guaoran/elk/logstash-6.5.1/config/agdata.conf`

```tex
input {
    file {
        path => ["/guaoran/elk/webapps/logs/agdata/*"]
		type => "agdata_nginx"
		#sincedb_path => "/dev/null"
		start_position => "beginning"
    }
}

filter {
	grok {
		match => {
			"message" => "(?:-|%{URIHOST:HOSTNAME}) %{NOTSPACE:response} \[%{HTTPDATE:timestamp2}\] %{IPORHOST:clientip} \[%{HTTPDATE:timestamp}\] %{NOTSPACE:request} (?:-|%{QS:referrer}) %{QS:agent} %{QS:xforwardedfor} (?:-|%{NUMBER:bytes}) %{BASE10NUM:response_time} "
		}
	}
	date {
		match => ["timestamp","dd/MMM/YYYY:HH:mm:ss Z"]
	}
	mutate{
		gsub => ["HOSTNAME",'"','']
	}
	mutate{
		convert => {"bytes"=>"float"}
	}
	geoip{
		source => "clientip"
	}
	useragent{
		source => "agent"
		target => "useragent"
	}

	if [referrer] =~ /^"http/ {
		grok{
			match => {
				"referrer" => '%{URIPROTO}://%{URIHOST:referrer_host}'
			}
		}
	}
	mutate{
		gsub => ["referrer",'"','']
	}
	mutate{remove_field=>["message"]}
}

output {
	#stdout{
	#	codec => rubydebug{
			metadata => true
	#	}
	#}
	elasticsearch {
		hosts => ["http://192.168.192.129:9200"]
		index => "logstash-%{type}-%{+YYYY.MM.dd}"
		document_type => "%{type}"
		sniffing => true
	}
}
```

###### 格式二

日志内容 `/guaoran/elk/webapps/logs/demo/`

```tex
127.0.0.1 - - [11/Nov/2018:00:01:02 +0800] "POST /api3/getrelevantcourse HTTP/1.1" 200 774 "www.guaoran.cn" "-" cid=608&secrect=xx&timestamp=1478707262003&token=xx&uid=4203162 "guaoran/5.0.2 (iPhone; iOS 10.0.1; Scale/3.00)" "-" 127.0.0.1:80 200 0.048 0.048
127.0.0.1 - - [11/Nov/2018:00:01:18 +0800] "POST /course/ajaxmediauser HTTP/1.1" 200 54 "www.guaoran.cn" "http://www.guaoran.cn/code/1883" mid=1883&time=60 "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko" "-" 127.0.0.1:80 200 0.016 0.016
127.0.0.1 - - [11/Nov/2018:00:01:18 +0800] "HEAD / HTTP/1.1" 301 0 "127.0.0.1" "-" - "curl/7.19.7 (x86_64-redhat-linux-gnu) libcurl/7.19.7 NSS/3.16.2.3 Basic ECC zlib/1.2.3 libidn/1.18 libssh2/1.4.2" "-" - - - 0.000
```

启动配置 `/guaoran/elk/logstash-6.5.1/config/demo.conf`

```tex
input{
    file{
        path => "/guaoran/elk/webapps/logs/demo/access.*.log"
        #sincedb_path => "/dev/null"
		start_position => "beginning"
    }
}

filter{
    if [@metadata][debug] {
        mutate{ remove_field => ["headers"] }
    }

    grok{
        match => {
           "message" => '%{IPORHOST:clientip} %{USER:ident} %{USER:auth} \[%{HTTPDATE:[@metadata][timestamp]}\] "%{WORD:verb} %{DATA:request} HTTP/%{NUMBER:httpversion}" %{NOTSPACE:response_status_code} (?:%{NUMBER:bytes}|-) %{QS:hostname} %{QS:referrer} (?:-|%{DATA:params}) %{QS:agent} %{QS:xforwardedfor} (?:-|%{MY_URI:upstream_host}) (?:-|%{MY_RESP:upstream_response_status_code}) (?:-|%{MY_RESP_TIME:upstream_response_time}) %{BASE10NUM:response_time:float}'
        }
        pattern_definitions=>{
            "MY_URI" => '%{URIHOST}(, %{URIHOST})*'
            "MY_RESP" => '%{NUMBER}(, %{NUMBER})*'
            "MY_RESP_TIME" => '%{BASE10NUM}(, %{BASE10NUM})*'
        }
    }
    date{
        match => ["[@metadata][timestamp]","dd/MMM/yyyy:HH:mm:ss Z"]
    }
    mutate{
        split => {"upstream_host" => ", "}
        split => {"upstream_response_status_code" => ", "}
        split => {"upstream_response_time" => ", "}
        gsub => ["hostname",'"','']
    }
    mutate{
        convert => {"upstream_response_time"=>"float"}
    }
    geoip{
        source => "clientip"
    }
    useragent{
        source => "agent"
        target => "useragent"
    }
    mutate{
        add_field => {
            "[@metadata][index]" => "nginx_logs_%{+YYYY.MM.dd}"
        }
    }

    if [referrer] =~ /^"http/ {
        grok{
            match => {
                "referrer" => '%{URIPROTO}://%{URIHOST:referrer_host}'
            }
        }
        if "guaoran.cn" in [referrer_host] {
            grok{
                match => {
                    "referrer" => ['%{URIPROTO}://%{URIHOST}/(%{NOTSPACE:demo_type}/%{NOTSPACE:demo_res_id})?"','%{URIPROTO}://%{URIHOST}/(%{NOTSPACE:demo_type})?"']
                }
            }
        }
    }
    mutate{
        gsub => ["referrer",'"','']
    }
    if "_grokparsefailure" in [tags] {
        mutate{
            replace => {
                "[@metadata][index]" => "nginx_logs_parsefailure_%{+YYYY.MM.dd}"
            }
        }
    }else{
        mutate{remove_field=>["message"]}
    }
}

output{
	if [@metadata][debug]{
		stdout{
			codec => rubydebug{
				metadata => true
			}
		}
	}else{
		#stdout{
		#	codec=>rubydebug
		#}
		elasticsearch{
			index => "%{[@metadata][index]}"
		}
	}
}
```

##### 启动 logstash

```shell
cd /guaoran/elk/logstash-6.5.1/
bin/logstash -f config/demo.conf -r
```

##### Logstash 与 ElasticSearch 集成

当需要将 tomcat 的 access 日志文件保存到 ElasticSearch 中时，在 `logstash.conf`  中  `output` 里面追加以下内容

```conf
elasticsearch {
	hosts => ["http://192.168.192.129:9200"]
	index => "logstash-%{type}-%{+YYYY.MM.dd}"
    document_type => "%{type}"
    sniffing => true
}
```



## 基本操作

#### ElasticSearch操作

##### 创建索引

- 一般索引

![1557198079081](assets/1557198079081.png)

- 结构化索引

![1557198006219](assets/1557198006219.png)

```json
{
	"novel": {
		"properties": {
			"title": {
				"type": "text"
			}
		}
	}
}
```

- 使用 postman  **PUT** 方式：`http://192.168.45.135:9200/demo_test`

![1557198794049](assets/1557198794049.png)

```json
{
    "settings":{
    "number_of_shards":3,
    "number_of_replicas":1
    },
    "mappings":{
        "man":{
            "properties":{
                "name":{
                    "type":"text"
                },
                "country":{
                    "type":"keyword"
                },
                "age":{
                    "type":"integer"
                },
                "data":{
                    "type":"date",
                    "format":"yyyy-MM-dd HH:mm:ss || yyyy-MM-dd||epoch_millis"
                }
            }
        }
    
    },
    "woman":{}
}
```

##### 插入数据

- 方式一：
  - 指定文档ID 插入，在Postman 中使用PUT 方法`http://192.168.45.135:9200/demo_test/man/1`

![1557199583184](assets/1557199583184.png)

- 方式二：
  - 自动生成文档ID 插入，在Postman 中使用POST 方法，`http://192.168.45.135:9200/demo_test/man`

![1557199817597](assets/1557199817597.png)



##### 修改文档

- 方式一：
  - 直接修改文档，打开Postmain，选择POST 方法，`http://192.168.45.135:9200/demo_test/man/1/_update`

![1557200317049](assets/1557200317049.png)

- 方式二：

  - 通过脚本修改文档，在raw 区输入以下内容：

  ![1557201141421](assets/1557201141421.png)

  - 所有年龄增加一岁。

    ```json
    {
    	"script": {
    		"lang": "painless",
    		"inline": "ctx._source.age += 1"
    	}
    }
    ```

  - 修改年龄为30 岁

    ```json
    {
    	"script": {
    		"lang": "painless",
    		"inline": "ctx._source.age = params.age",
    		"params": {
    			"age": 30
    		}
    	}
    }
    ```



##### 删除文档

- 打开Postman，选择DELETE 方法，输入 `http://192.168.45.135:9200/demo_test/man/1`

![1557201725090](assets/1557201725090.png)

##### 删除索引

- 打开Postman ，DELETE方法 输入 `http://192.168.45.135:9200/demo_test`，执行结果如下：

![1557201790110](assets/1557201790110.png)

##### 查询语句

- 全表查询：GET 方法，打开Postman 输入 `http://192.168.45.135:9200/demo_test/_search`

  ![1557205576701](assets/1557205576701.png)

- 条件查询:

  - POST方法，在Postman 中输入 `http://192.168.45.135:9200/demo_test/_search`，然后在raw 区域中编辑如下内容：查询`name`中包含`zhang`关键字，且按日期`date`降序排序。

  ```json
  {
  	"query": {
  		"match": {
  			"name": "zhang"
  		}
  	},
  	"sort": [{
  		"date": {
  			"order": "desc"
  		}
  	}]
  }
  ```

  ![1557206089604](assets/1557206089604.png)

- 聚合查询:

  - 在Postman 中选择GET 方法，输入`http://192.168.45.135:9200/demo_test/_search`，然后在raw 区域中编辑如下内容:根据年龄和日期进行分组

  ```json
  {
  	"aggs": {
  		"group_by_age": {
  			"terms": {
  				"field": "age"
  			}
  		},
  		"group_by_date": {
  			"terms": {
  				"field": "date"
  			}
  		}
  	}
  }
  ```

  ![1557206359462](assets/1557206359462.png)

- 聚合统计:

  - POST 方法，在Postman 中输入`http://192.168.45.135:9200/demo_test/_search`，然后在raw 区域中编辑如下内容：根据`age`进行聚合统计

  ```json
  {
  	"aggs": {
  		"grades_age": {
  			"stats": {
  				"field": "age"
  			}
  		}
  	}
  }
  ```

  ![1557206528007](assets/1557206528007.png)

##### query 条件

- 模糊匹配，在Postman 中选择Post方法，输入`http://192.168.45.135:9200/demo_test/_search`，然后在raw 区域中编辑如下内容：查询名称包含“java" 和 ”spark“ 的关键字

  ```json
  {
  	"query": {
  		"match": {
  			"name": "java spark"
  		}
  	}
  }
  ```

  ![1557208198329](assets/1557208198329.png)

- 近似匹配

  - phrase match，就是要去将多个term作为一个短语，一起去搜索，只有包含这个短语的doc才会作为结果返回。match是只在包含其中任何一个分词就返回。

  ```json
  {
  	"query": {
  		"match_phrase": {
  			"name": "java spark"
  		}
  	}
  }
  ```

  ![1557208139018](assets/1557208139018.png)

- 多字段匹配:查询 `name` 和 `countr`  包含 `java` 关键字

  ```json
  {
  	"query": {
  		"multi_match": {
  			"query": "java",
  			"fields":["name","country"]
  		}
  	}
  }
  ```

  ![1557208461052](assets/1557208461052.png)

 [ElasticSearch 索引查询使用指南——详细版](https://www.cnblogs.com/pilihaotian/p/5830754.html)

### kibana 进行视图展示

[官网用户手册](https://www.elastic.co/guide/cn/kibana/current/index.html)

当我们访问 tomcat 应用时 ，logstash 启动窗口 会输出日志，日志如下

```tex
{
      "timestamp" => "08/May/2019:10:28:36 +0800",
          "bytes" => "10809",
       "response" => "200",
    "httpversion" => "1.1",
     "@timestamp" => 2019-05-08T02:28:36.000Z,
           "path" => "/guaoran/elk/webapps/logs/localhost_access_log.2019-05-08.log",
           "type" => "tomcat_access",
           "host" => "localhost.localdomain",
           "verb" => "POST",
       "clientip" => "192.168.192.1",
        "message" => "192.168.192.1 - - [08/May/2019:10:28:36 +0800] \"POST /index.html HTTP/1.1\" 200 10809",
       "@version" => "1",
        "request" => "/index.html"
}
```

##### 匹配索引

我们在正式使用Kibana之前，需要先匹配我们Elasticsearch中的索引库，因为我们的Elasticsearch有可能会有很多索引库，Kibana为了性能因素，是不会事先把所有的索引库都导进来的，我们需要用那个索引就导哪个索引。

###### 查询日志插入到 ES 上创建的索引

点击 **kibana** 菜单 `monitoring`,点击 **Elasticsearch** 的 `Overview` ，点击 `Indices`，或者通过下面的步骤也可以看到存在哪些索引

###### 导入索引

点击 **kibana** 菜单 `Management `,点击 **Kibana** 的 `Index Patterns` ，点击 `Create index pattern`

![1557300836050](assets/1557300836050.png)

上图中方框便是所有的索引，在 `Index pattern` 中选择需要的索引，可以正则匹配。输入好之后点击 `Next step` ,选择时间过滤字段 `@timestamp` ,点击 `Create index pattern` ，完成匹配。

下图可以看到对应的字段![1557301389218](assets/1557301389218.png)

##### 探索数据

点击 **kibana** 菜单 `Discover `,点击 **Kibana** 的 `Index Patterns` ，点击 `Create index pattern`

可选择对应的索引，数据过滤或者展示某些感兴趣的字段

![1557302431846](assets/1557302431846.png)

##### 数据可视化

点击 **kibana** 菜单 `Visualize `,点击  **`+`**  按钮，进行选择对应的视图



### Logstash 、Kibana 时区问题

#### logstsh 的时区问题

##### 复现问题

以 [nginx 日志格式和配置](./#格式一) 为例

日志内容：

```tex
192.168.20.2 200 [14/May/2019:00:00:02 +0800] 192.168.20.44 [14/May/2019:00:00:02 +0800] /dataManual/dataIndustryTree.html "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36" "-" 11329 0.008 -
```

当开启 `stdout`

```tex
stdout{
	codec=>rubydebug
}
```

`logstash`  启动时读取日志时的输出为：

```tex
{
	"request" => "/dataManual/dataIndustryTree.html",
	...
  	"@timestamp" => 2019-05-13T16:00:02.000Z,
  	...
    "timestamp" => "14/May/2019:00:00:02 +0800"
}
```

此时会有两个问题：

1. 我们读取的时间为 `14/May/2019:00:00:02 +0800` 但是 `@timestamp` 时间却少了八个小时
2. 打开 `kibana` 查看生成的索引文件，索引文件名称却不是 `xxxx-2019.05.14` ,而是 `xxxx-2019.05.13` 可知索引文件的名称是通过 `@timestamp` 的时间而定的

##### 解决问题

在配置文件中的 `filter`  中加入以下配置

```tex
date {
	match => ["timestamp","dd/MMM/YYYY:HH:mm:ss Z"]
	target => "@timestamp"
}
ruby {
	code => "event.set('timestamp', event.get('@timestamp').time.localtime + 8*60*60)"
}
ruby {
	code => "event.set('@timestamp',event.get('timestamp'))"
}
mutate {remove_field => ["timestamp"]}
```

再次执行导入，输出结果

```tex
{
	"request" => "/dataManual/dataIndustryTree.html",
    "@timestamp" => 2019-05-14T00:00:02.000Z,
    "timestamp" => 2019-05-14T00:00:02.000Z
}
```

此时查看上面的两个问题已经解决了



#### kibana 的时区问题

##### 复现问题

当我们在 **kibana** 的 `Discover`  中对刚刚生成的索引文件进行展示时，如图所示

![1557899629573](assets/1557899629573.png)

上图中的时间展示却又多个8个小时，通过 `postman` 查看该索引的内容，如下

![1557899860596](assets/1557899860596.png)

可以得到 存储的内容是正确的，但是 `kibana` 中展示不正确，可得又是 `kibana` 的时区问题

##### 解决问题

修改 `kibana` 的时区

点击 `kibana` 的菜单 `Management` ,点击  `Advanced Settings` 

![1557900154983](assets/1557900154983.png)

将 `dateFormat:tz` 由 `Browser` 改成 `Etc/UTC` ，

![1557900197876](assets/1557900197876.png)

保存后再次点击 `Discover` 会发现问题解决





#### kibana 通过 nginx 权限认证

设置 用户名为 `admin` ,密码为 `123456` 存储到 `htpasswd` 中

```shell
sudo htpasswd -c -b /usr/local/nginx/conf/extra/htpasswd admin 123456
```

`nginx.conf`

```tex
server {
        listen       5602;
        access_log /mnt/logs/nginx/kibana.access.log main;

        location / {
            proxy_redirect         off;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_pass  http://kibanaServer;
            auth_basic "kibana login auth";
            auth_basic_user_file /usr/local/nginx/conf/extra/htpasswd;
        }
}
```

