# docker 

# 安装

[官网地址](https://docs.docker.com/install/linux/docker-ce/centos/)



# 命令

查看 docker 信息

```SHELL
docker info
```

## 镜像命令

查看所有镜像

```shell
docker images
```

查询所有的 tomcat 关键字的 docker 镜像

```shell
docker search tomcat
```

查询所有的 tomcat 关键字的 docker 镜像，且 STARS 大于 30 的

```shell
docker search -s 30 tomcat
```

拉取镜像 最新版本

```shell
docker pull nginx
```

删除镜像，如果正在启动，则使用下面命令

```shell
docker rmi hello-world:latest

docker rmi -f hello-world:latest 
```

删除全部镜像

```shell
docker rmi -f $(docker images -q)
```



## 容器命令

查看当前正在运行的容器

```shell
docker ps
```

查看当前所有运行的容器

```shell
docker ps -a
```

运行一个容器

```shell
docker run -it --name tomcat-demo tomcat
```

退出容器但不停止运行

```shell
Ctrl + q + p
```

启动容器 ：可以指定多个容器ID

```shell
docker start 容器ID 或 容器名称
```

重启容器 ：可以指定多个容器ID

```shell
docker start 容器ID 或 容器名称
```

停止容器 ：可以指定多个容器ID

```shell
docker stop 容器ID 或 容器名称
```

强制停止容器 ：可以指定多个容器ID

```shell
docker kill 容器ID 或 容器名称
```

删除容器 ：可以指定多个容器ID

```shell
docker rm 容器ID
```

删除全部容器

```shell
docker rm  $(docker ps -a -q)
docker ps -a -q |xargs docker rm
```

后台运行容器

```shell
docker run -d tomcat
```

后台运行容器并指定访问端口 8888

```shell
 docker run -d -p 8888:8080 --name mytomcat tomcat
```

进入容器

```shell
docker exec -it 容器ID /bin/bash
```

在现有的容器中生成一个镜像

```shell
docker commit -m="提交信息" -a="作者" 容器ID "镜像名称:TAG"

docker commit -m="create image from mytocmat" -a="guaoran" 1d347a9b70f9 "guaoran-tomcat:1.0"
```

启动自定义的镜像

```shell
docker run -d -p 8888:8080 --name 别名 镜像名称:TAG

docker run -d -p 8888:8080 --name guaoran-tomcat guaoran-tomcat:1.0
```

复制容器中的文件到主机

```shell
docker cp 容器ID:容器内路径 主机路径

docker cp 3ec4dad6f91c:/usr/local/tomcat/BUILDING.txt /root
```





添加数据卷

```shell
docker run -it -v 宿主机目录:容器内路径 镜像名

docker run -it -v /mydata:/datacontainer  centos
```

共享数据卷 (v0、v1 共用数据卷)

```shell
docker run -it --name v0 -v /mydata:/datacontainer  centos
```

```shell
docker run -it --name v1 --volumes-from v0 centos
```





## 网络

查看网卡

```shell
ip link show
ip a
ifconfig
```

网卡添加IP地址

```shell
ip addr add 192.168.45.100/24 dev eth0
```

网卡删除ip地址

```shell
ip addr delete 192.168.45.100/24 dev eth0
```

重启网卡

```shell
service network restart / systemctl restart network
```

启动/关闭某个网卡

```shell
ifup/ifdown eth0 or ip link set eth0 up/down
```

管理 network namespace 

```shell
#查看
ip netns list 
#添加
ip netns add ns1
#删除
ip netns delete ns1
```

创建一个 network namespace

```shell
ip netns add ns1
```

查看该 namespace 下的网卡信息

```shell
ip netns exec ns1 ip a
```

启动ns1上的lo网卡

```shell
ip netns exec ns1 ifup lo

ip netns exec ns1 ip link set lo up
```

再次查看可以发现state变成了UNKOWN

```shell
[root@localhost ~]# ip netns exec ns1 ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
```



## 容器中的网络

创建两个容器

```shelll
docker run -d --name tomcat01 -p 8081:8080 tomcat
docker run -d --name tomcat02 -p 8082:8080 tomcat
```

进入容器中并查看ip

```shell
docker exec -it tomcat01 ip a
docker exec -it tomcat02 ip a
```



```shell
docker network inspect bridge
```



创建一个network，类型为bridge

```shell
docker network create tomcat-net

docker network create --subnet=172.18.0.0/24 tomcat-net
```

查看已有的network

```shell
docker network ls
```

查看tomcat-net详情信息

```shell
docker network inspect tomcat-net
```

创建tomcat的容器，并且指定使用tomcat-net

```shell
docker run -d --name custom-net-tomcat --network tomcat-net tomcat
```

查看custom-net-tomcat的网络信息

```shell
docker exec -it custom-net-tomcat ip a
```

此时在custom-net-tomcat容器中ping一下tomcat01的ip会如何？发现无法ping通

```shell
docker exec -it custom-net-tomcat ping 172.17.0.2
```

此时如果tomcat01容器能够连接到tomcat-net上应该就可以咯

```shell
docker network connect tomcat-net tomcat01
```









疑问：

* 容器和镜像啥区别？

  > 容器是通过镜像产生的
  >
  > 即 docker run tomcat  ，是启动了一个容器

* docker info 中的stop 是什么意思？

  > stop 表示已经停止的容器，可以使用 docker start 再次启动。

* 为什么已经 docker rmi -f 删除过镜像了，然后再下载镜像再删除（docker rmi ）还是会报错？

  > 因为当我们 docker run tomcat 时，是启动了一个容器且。只有没有容器正在使用镜像时才可以删除镜像。因此当我们停止并删除容器之后才可以使用 docker rmi 删除镜像。 
  >
  > 正确的操作步骤是
  >
  > 1. docker ps -a  ,查看 STATUS 是否是启动状态， 
  > 2. 如果启动中则停止（stop）容器，
  > 3. 未启动时删除（rm）容器，
  > 4. 再次删除镜像就可以了

* 怎么下载一个镜像的多个版本？


同机器：bridge 、host、null

跨机器：vxlan 