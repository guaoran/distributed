package com.guaoran.distributed.nacos.demo.test;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.core.env.StandardEnvironment.SYSTEM_PROPERTIES_PROPERTY_SOURCE_NAME;

/**
 * @author : gu cheng
 * @description : nacos 自动刷新
 * @desc 这里如果可以通过 before 或 after 指定优先级，默认优先级最低，如果想优先使用 nacos的配置可以将nacos优先级调高，如果希望 application.properties 配置高，则不需要动
 * @date : 2021/6/18 11:45
 */
@RestController
@RequestMapping("/nacos")
@NacosPropertySource(dataId = "sy_bigdata_center_auto_refreshed", groupId = "DEFAULT_GROUP", before = SYSTEM_PROPERTIES_PROPERTY_SOURCE_NAME, autoRefreshed = true)
public class NacosTestController {
    @NacosValue(value = "${nacos.content}", autoRefreshed = true)
    private String nacosContent;
    @Autowired
    private NacosTestService nacosTestService;

    @GetMapping("/get")
    public String getNacosContent() {
        String nacosService = nacosTestService.getNacosService();

        return nacosContent + "=" + nacosService;
    }
}
