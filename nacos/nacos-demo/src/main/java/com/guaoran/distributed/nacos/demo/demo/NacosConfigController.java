package com.guaoran.distributed.nacos.demo.demo;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : 孤傲然
 * @description : nacos config
 *
 * 获取 nacos 配置的信息，支持动态刷新。
 * @date :2019/11/1 13:35
 */
//@NacosPropertySource(dataId = "example" ,groupId = "DEFAULT_GROUP" ,autoRefreshed = true)
//@RestController
public class NacosConfigController {

//    @NacosValue(value = "${info:hello world}",autoRefreshed = true)
    private String info;

//    @GetMapping("/get")
    public String get (){
        return info;
    }

    public static void main(String[] args) {
        String [] ss = new String[]{"","s",null};
        System.out.println(ss.length);
    }
}
