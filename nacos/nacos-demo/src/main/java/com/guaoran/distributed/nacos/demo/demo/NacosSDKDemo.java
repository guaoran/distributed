package com.guaoran.distributed.nacos.demo.demo;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * @author : 孤傲然
 * @description : Nacos 使用sdk
 * @date :2019/11/2 13:12
 */
public class NacosSDKDemo {

    public static void main(String[] args) throws IOException {
        String serverAddr="localhost:8848";
        String dataId="example";
        String groupId="DEFAULT_GROUP";
        Properties properties=new Properties();
        properties.put("serverAddr",serverAddr);
        try {
            // 进行nacos 连接
            ConfigService configService= NacosFactory.createConfigService(properties);
            // 获取注册中心的内容 根据dataId 和 groupId
            String content = configService.getConfig(dataId,groupId,3000);
            System.out.println(content);
            // 变更监听
            configService.addListener(dataId, groupId, new Listener() {
                @Override
                public Executor getExecutor() {
                    return null;
                }
                @Override
                public void receiveConfigInfo(String configInfo) {
                    System.out.println("变更的内容:"+configInfo);
                }
            });
        } catch (NacosException e) {
            e.printStackTrace();
        }
        System.in.read();
    }

}
