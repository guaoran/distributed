package com.guaoran.distributed.nacos.demo.demo;

import java.net.ConnectException;

/**
 * @author : 孤傲然
 * @description : TODO 加上描述吧
 * @date :2020/7/2 18:53
 */
public class TimeOutTest {
    public static void main(String[] args) throws ConnectException {
        TimeOutTest timeOutTest = new TimeOutTest();
        timeOutTest.test();
    }
    Long readTimeoutMs = 10000L;
    public void test() throws ConnectException {
        final long endTime = System.currentTimeMillis() + readTimeoutMs;
        boolean isSSL = false;

        int maxRetry = 3;

        do {

            out();

            maxRetry --;
            if (maxRetry < 0) {
                throw new ConnectException("[NACOS HTTP-POST] The maximum number of tolerable server reconnection errors has been reached");
            }

        } while (System.currentTimeMillis() <= endTime);

        System.out.println(("no available server, currentServerAddr : "));
    }

    private void out() {
        try {
            Thread.sleep(150000);
            System.err.println("out");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
