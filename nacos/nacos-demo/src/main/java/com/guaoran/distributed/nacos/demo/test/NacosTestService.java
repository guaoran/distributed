package com.guaoran.distributed.nacos.demo.test;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import org.springframework.stereotype.Service;

/**
 * @author : gu cheng
 * @description : TODO 加上描述吧
 * @date : 2021/6/18 18:09
 */
@Service
public class NacosTestService {
    @NacosValue(value = "${nacos.service}",autoRefreshed = true)
    private String nacosService;

    public String getNacosService(){
        return nacosService;
    }
}
