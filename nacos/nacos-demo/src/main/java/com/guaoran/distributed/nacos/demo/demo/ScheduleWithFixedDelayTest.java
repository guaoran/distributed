package com.guaoran.distributed.nacos.demo.demo;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * @author : 孤傲然
 * @description : scheduleWithFixedDelay
 * @date :2020/7/2 16:54
 */
public class ScheduleWithFixedDelayTest {

    public static void main(String[] args) throws IOException {
        ScheduleWithFixedDelayTest test = new ScheduleWithFixedDelayTest();
        System.in.read();
    }
    public ScheduleWithFixedDelayTest() {
        // 创建调度功能的线程池， 核心线程数是1，
        // 进行客户端工作
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setName("com.alibaba.nacos.client.Worker.");
                t.setDaemon(true);
                return t;
            }
        });


        executor.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                try {
                    checkConfigInfo();
                } catch (Throwable e) {
                  e.printStackTrace();
                }
            }
        }, 1L, 10L, TimeUnit.MILLISECONDS);
    }

    private void checkConfigInfo() {
        System.out.println("checkConfigInfo" +"---"+System.currentTimeMillis());
//        try {
////            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }


}
