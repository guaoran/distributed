## 注册中心分析

```xml
<dependency>
    <groupId>com.alibaba.nacos</groupId>
    <artifactId>nacos-client</artifactId>
    <version>1.1.1</version>
</dependency>
```

```java
public static void main(String[] args) {
    String serverAddr="localhost:8848";
    String dataId="example";
    String groupId="DEFAULT_GROUP";
    Properties properties=new Properties();
    properties.put("serverAddr",serverAddr);
    try {
        // 进行nacos 连接
        ConfigService configService= NacosFactory.createConfigService(properties);
        // 获取注册中心的内容 根据dataId 和 groupId
        String content = configService.getConfig(dataId,groupId,3000);
        System.out.println(content);
        // 变更监听
        configService.addListener(dataId, groupId, new Listener() {
            @Override
            public Executor getExecutor() {
                return null;
            }
            @Override
            public void receiveConfigInfo(String configInfo) {
                System.out.println("变更的内容:"+configInfo);
            }
        });
    } catch (NacosException e) {
        e.printStackTrace();
    }
    System.in.read();
}
```



### 源码分析

```java
// 进行nacos 连接
ConfigService configService= NacosFactory.createConfigService(properties);
// 获取注册中心的内容 根据dataId 和 groupId
//先从本地缓存中取，如果没有再从远程服务取
String content = configService.getConfig(dataId,groupId,3000);
```



第一步创建一个配置服务，进行nacos的连接

```java
public static ConfigService createConfigService(Properties properties) throws NacosException {
    try {
        Class<?> driverImplClass = Class.forName("com.alibaba.nacos.client.config.NacosConfigService");
        Constructor constructor = driverImplClass.getConstructor(Properties.class);
        ConfigService vendorImpl = (ConfigService) constructor.newInstance(properties);
        return vendorImpl;
    } catch (Throwable e) {
        throw new NacosException(NacosException.CLIENT_INVALID_PARAM, e);
    }
}
```

这里进行实例化 `NacosConfigService` 

```java
public NacosConfigService(Properties properties) throws NacosException {
    String encodeTmp = properties.getProperty(PropertyKeyConst.ENCODE);
    if (StringUtils.isBlank(encodeTmp)) {
        encode = Constants.ENCODE;
    } else {
        encode = encodeTmp.trim();
    }
    initNamespace(properties);
    // 代理 ServerHttpAgent 调用， 进行超时处理的装饰器
    agent = new MetricsHttpAgent(new  ServerHttpAgent 调用， 进行超时处理的装饰器(properties));
    agent.start();
    // 客户端的工作类 ，核心功能
    worker = new ClientWorker(agent, configFilterChainManager, properties);
}
```

`ClientWorker` 构造方法

```java
public ClientWorker(final HttpAgent agent, final ConfigFilterChainManager configFilterChainManager, final Properties properties) {
    this.agent = agent;
    this.configFilterChainManager = configFilterChainManager;

    // Initialize the timeout parameter

    init(properties);

    // 创建调度功能的线程池， 核心线程数是1，
    // 进行客户端工作
    executor = Executors.newScheduledThreadPool(1, new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setName("com.alibaba.nacos.client.Worker." + agent.getName());
            t.setDaemon(true);
            return t;
        }
    });

    // 创建调度功能的线程池，核心线程数是物理CPU个数
    // 进行长轮询 ,主要是检查 nacos 的配置是否有变更，及时通知
    executorService = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors(), new ThreadFactory() {
        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(r);
            t.setName("com.alibaba.nacos.client.Worker.longPolling." + agent.getName());
            t.setDaemon(true);
            return t;
        }
    });

    // 使客户端工作线程池以固定延迟（时间）来执行线程任务，
    // 每1毫秒，执行一次任务， 当任务执行完之后再延迟10毫秒再执行
    executor.scheduleWithFixedDelay(new Runnable() {
        @Override
        public void run() {
            try {
                checkConfigInfo();
            } catch (Throwable e) {
                LOGGER.error("[" + agent.getName() + "] [sub-check] rotate check error", e);
            }
        }
    }, 1L, 10L, TimeUnit.MILLISECONDS);
}
```

这里来看有两个线程池，一个是 `executor` 这里称为`检查配置线程池`， 一个是 `executorService` 这里称为`长轮询线程池` ，现在来看 `检查配置线程池`的 `checkConfigInfo()` 方法。

这里采用分而治之的思想，分批次的来通过`长轮询线程池`进行检查配置。根据批次创建多个线程进行长轮询

```java
public void checkConfigInfo() {
    // 分任务
    int listenerSize = cacheMap.get().size();
    // 向上取整为批数
    int longingTaskCount = (int) Math.ceil(listenerSize / ParamUtil.getPerTaskConfigSize());
    if (longingTaskCount > currentLongingTaskCount) {
        for (int i = (int) currentLongingTaskCount; i < longingTaskCount; i++) {
            // 要判断任务是否在执行 这块需要好好想想。 任务列表现在是无序的。变化过程可能有问题
            executorService.execute(new LongPollingRunnable(i));
        }
        currentLongingTaskCount = longingTaskCount;
    }
}
```



这里来看 `LongPollingRunnable`  线程的执行代码

```java
class LongPollingRunnable implements Runnable {
    private int taskId;

    public LongPollingRunnable(int taskId) {
        this.taskId = taskId;
    }

    @Override
    public void run() {

        // 这里是进行检查本地缓存
        // 主要是：有一个内存的缓存，还有一个本地文件的缓存
        
        List< CacheData> cacheDatas = new ArrayList<CacheData>();
        List<String> inInitializingCacheList = new ArrayList<String>();
        try {
            // check failover config
            for (CacheData cacheData : cacheMap.get().values()) {
                if (cacheData.getTaskId() == taskId) {
                    cacheDatas.add(cacheData);
                    try {
                        // 如果 CacheData 没有设置使用本地缓存 且 本地文件存在，则开启使用本地缓存。将本地文件的值设置到内存缓存中
                        // 如果 CacheData 设置了使用本地缓存 且 本地文件不存在，则关闭使用本地缓存
                        // 如果 CacheData 设置了使用本地缓存 且 本地文件存在 ，但是两者的修改时间不一致，则以本地文件覆盖到内存缓存中。 
                        checkLocalConfig(cacheData);
                        
                        // 如果使用了本地缓存，则校验md5是否相同，如果不同，说明本地内存缓存和本地缓存文件有出入， 则进行通知 ： 方法是， 开始的实例中的 变更通知的方法 ：receiveConfigInfo()
                        if (cacheData.isUseLocalConfigInfo()) {
                            cacheData.checkListenerMd5();
                        }
                    } catch (Exception e) {
                        LOGGER.error("get local config info error", e);
                    }
                }
            }

            // 检查远程服务
            // check server config
            
            // 从Server获取值变化了的DataID列表。
            List<String> changedGroupKeys = checkUpdateDataIds(cacheDatas, inInitializingCacheList);

            for (String groupKey : changedGroupKeys) {
                String[] key = GroupKey.parseKey(groupKey);
                String dataId = key[0];
                String group = key[1];
                String tenant = null;
                if (key.length == 3) {
                    tenant = key[2];
                }
                try {
                    // 获得远程服务器上的内容： 会从远程服务器中拿到最新的内容，并进行快照保存到本地文件中， 所以在上面的本地缓存的校验中，是以本地文件缓存为主：本地文件缓存覆盖本地内存缓存。
                    String content = getServerConfig(dataId, group, tenant, 3000L);
                    // 重新将远程服务器的内容设置到内存缓存中
                    CacheData cache = cacheMap.get().get(GroupKey.getKeyTenant(dataId, group, tenant));
                    cache.setContent(content);
                    LOGGER.info("[{}] [data-received] dataId={}, group={}, tenant={}, md5={}, content={}",
                                agent.getName(), dataId, group, tenant, cache.getMd5(),
                                ContentUtils.truncateContent(content));
                } catch (NacosException ioe) {
                    String message = String.format(
                        "[%s] [get-update] get changed config exception. dataId=%s, group=%s, tenant=%s",
                        agent.getName(), dataId, group, tenant);
                    LOGGER.error(message, ioe);
                }
            }
            // 再次校验远程服务的 CacheData Md5是否有变化，在进行通知 
            for (CacheData cacheData : cacheDatas) {
                if (!cacheData.isInitializing() || inInitializingCacheList
                    .contains(GroupKey.getKeyTenant(cacheData.dataId, cacheData.group, cacheData.tenant))) {
                    cacheData.checkListenerMd5();
                    cacheData.setInitializing(false);
                }
            }
            inInitializingCacheList.clear();

            executorService.execute(this);

        } catch (Throwable e) {

            // If the rotation training task is abnormal, the next execution time of the task will be punished
            LOGGER.error("longPolling error : ", e);
            executorService.schedule(this, taskPenaltyTime, TimeUnit.MILLISECONDS);
        }
    }
}
```







