dubbo 支持多注册中心 ：redis / zookeeper / eureka / multicast / nacos / consul / etcd3 / sofa 

dubbo 支持多协议 : dubbo / hessian / http / redis / rest /thrift / webservice / xml /grpc

dubbo 支持配置中心: apollo / consul / etcd / nacos / zookeeper



 负载均衡：

* 权重随机（默认）`random`
* 轮询 `roundrobin`
* 一致性hash
* 最小活跃数

容错策略：

* `failover` 重试 （默认容错策略）， 默认重试3次， retries=2
* `failfast`  快速失败 
* `failback` 失败之后记录日志
* `failsafe` 失败安全,选择忽略错误
* `forking` 并行调用多个服务，一个成功就返回。对实时性有更高的要求
* `broadcast` 广播 ，任何一台报错就返回，比如：更新缓存

 