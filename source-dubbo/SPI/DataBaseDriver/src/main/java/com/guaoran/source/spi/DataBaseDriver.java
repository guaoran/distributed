package com.guaoran.source.spi;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2018/10/30 22:34
 */
public interface DataBaseDriver {
    /**
     * 执行连接
     * @param host
     * @return
     */
    public String connect(String host);
}
