package com.guaoran.source.dubbo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : 孤傲然
 * @description : client
 * @date :2020/7/7 16:01
 */
@SpringBootApplication
public class DubboClientBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(DubboClientBootApplication.class, args);
    }

}
