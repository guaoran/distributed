package com.guaoran.source.dubbo;

import com.guaoran.source.dubbo.demo.IHelloService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : 孤傲然
 * @description : 隐式传参
 * @date :2020/7/7 16:04
 */
@RestController
public class HelloController {

    @Reference
    private IHelloService helloService;

    @GetMapping("/")
    public String hello(){
        RpcContext.getContext().isConsumerSide();
        RpcContext.getContext().isProviderSide();

        return helloService.sayHello("dubbo");
    }


}
