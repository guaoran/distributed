package com.guaoran.source.dubbo;

import com.guaoran.source.dubbo.demo.IHelloService;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author : 孤傲然
 * @description : HelloService
 * @date :2020/7/7 17:20
 */
@Service(loadbalance = "random")
public class HelloService implements IHelloService {
    @Override
    public String sayHello(String message) {
        return "Hello " + message;
    }
}
