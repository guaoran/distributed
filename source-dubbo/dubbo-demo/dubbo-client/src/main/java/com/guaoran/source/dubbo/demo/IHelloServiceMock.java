package com.guaoran.source.dubbo.demo;

/**
 * @author : guaoran
 * @Description : <br/>
 *
 *  服务降级
 * @date :2018/10/30 22:20
 */
public class IHelloServiceMock implements IHelloService {
    @Override
    public String sayHello(String message) {
        return "系统繁忙："+message;
    }
}
