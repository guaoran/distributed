package com.guaoran.source.dubbo.spi;

import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.common.extension.Adaptive;
import com.alibaba.dubbo.common.extension.ExtensionLoader;
import com.alibaba.dubbo.common.extension.factory.AdaptiveExtensionFactory;
import com.alibaba.dubbo.monitor.Monitor;
import com.alibaba.dubbo.monitor.MonitorFactory;
import com.alibaba.dubbo.rpc.Protocol;
import com.alibaba.dubbo.rpc.protocol.ProtocolFilterWrapper;
import com.alibaba.dubbo.rpc.protocol.dubbo.DubboProtocol;

import java.lang.reflect.Constructor;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2019/1/7 13:31
 */
public class SPITest {
    public static void main(String[] args) throws NoSuchMethodException {
//        Protocol protocol =
//                ExtensionLoader.
//                        getExtensionLoader(Protocol.class).
//                        getExtension("myProtocol");
//        System.out.println(protocol.getDefaultPort());
//        Protocol pr = ExtensionLoader.getExtensionLoader(Protocol.class).getDefaultExtension();
//        System.out.println(pr.getDefaultPort());
//        System.out.println(ExtensionLoader.getExtensionLoader(Protocol.class).getExtension("dubbo").getDefaultPort());;
        ExtensionLoader.getExtensionLoader(Protocol.class).getAdaptiveExtension().getDefaultPort();

        // 判断该类上是否有 Adaptive 注解，如果有返回 true，否则返回 false
        System.out.println(MonitorFactory.class.isAnnotationPresent(Adaptive.class));
        System.out.println(AdaptiveExtensionFactory.class.isAnnotationPresent(Adaptive.class));
        System.out.println(Protocol.class.isAnnotationPresent(Adaptive.class));

        //判断 ProtocolFilterWrapper 类 是否存在 public ProtocolFilterWrapper(Protocol protocol) 的构造方法，
        // 如果存在，返回该构造方法，不存在则报错
        ProtocolFilterWrapper.class.getConstructor(Protocol.class);


    }
}
