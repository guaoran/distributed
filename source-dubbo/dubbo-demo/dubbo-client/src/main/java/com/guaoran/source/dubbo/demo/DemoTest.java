package com.guaoran.source.dubbo.demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 18:30
 */
public class DemoTest {
    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext classPathXmlApplicationContext =
                new ClassPathXmlApplicationContext("dubbo-client.xml");
        IHelloService helloService = (IHelloService) classPathXmlApplicationContext.getBean("helloService");
        System.out.println(helloService.sayHello("World"));
        System.in.read();
    }
}
