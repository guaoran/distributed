package com.guaoran.source.dubbo.spi;


import com.alibaba.dubbo.common.URL;
import com.alibaba.dubbo.common.extension.ExtensionLoader;
import com.alibaba.dubbo.registry.Registry;
import com.alibaba.dubbo.registry.RegistryFactory;

/**
 * @author : guaoran
 * @Description : <br/>
 * 注册中心
 * 根据 createAdaptiveExtensionClass() 动态获得的 适配器扩展点
 * @date :2019/1/7 13:31
 */
public class RegistryFactory$Adpative implements RegistryFactory {
    public Registry getRegistry(URL arg0) {
        if (arg0 == null) throw new IllegalArgumentException("url == null");
        URL url = arg0;
        String extName = (url.getProtocol() == null ? "dubbo" : url.getProtocol());
        if (extName == null)
            throw new IllegalStateException("Fail to get extension(RegistryFactory) name from url(" + url.toString() + ") use keys([protocol])");
        // 当 url = zookeeper://..的时候，获得的是 ZookeeperRegistryFactory
        RegistryFactory extension = (RegistryFactory) ExtensionLoader.getExtensionLoader(RegistryFactory.class).getExtension(extName);
        return extension.getRegistry(arg0);
    }
}
