package com.guaoran.source.dubbo.spi;

import com.alibaba.dubbo.config.ServiceConfig;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

/**
 * @author : guaoran
 * @Description : <br/>
 *  服务端发布入口
 * @date :2019/1/8 15:47
 */
public class ServiceBean<T> extends ServiceConfig<T> implements InitializingBean, DisposableBean, ApplicationContextAware, ApplicationListener, BeanNameAware {
    // InitializingBean 为接口 bean 提供了初始化方法的方式，凡是实现了该接口的类，在初始化 bean 的时候会执行该方法
    @Override
    public void afterPropertiesSet() throws Exception {
        export();
    }

    // DisposableBean ，bean 被销毁的时候，spring 容器会自动执行 destroy 方法，比如释放资源
    @Override
    public void destroy() throws Exception {

    }

    // ApplicationContextAware 实现了这个接口的类，当 spring 容器初始化的时候，会自动的将 ApplicationContext 注入进来
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }

    // ApplicationListener ,ApplicationEvent 事件监听，spring 容器启动后会发一个事件通知
    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {

    }

    // BeanNameAware 获得自身初始化时，本身 bean 的 id 属性
    @Override
    public void setBeanName(String s) {

    }

}
