package com.guaoran.source.dubbo.demo;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 17:58
 */
public interface IHelloService {
    /**
     * hello
     * @param message
     * @return
     */
    public String sayHello(String message);
}
