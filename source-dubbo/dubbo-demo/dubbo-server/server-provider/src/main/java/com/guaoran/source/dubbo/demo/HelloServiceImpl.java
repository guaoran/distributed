package com.guaoran.source.dubbo.demo;

/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 17:59
 */
public class HelloServiceImpl implements IHelloService {
    @Override
    public String sayHello(String message) {
        return "Hello "+message;
    }
}
