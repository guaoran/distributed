package com.guaoran.source.dubbo.demo;


import com.guaoran.source.spi.DataBaseDriver;

import java.util.ServiceLoader;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2018/10/30 22:46
 */
public class DataBaseConnection {
    public static void main(String[] args) {
        ServiceLoader<DataBaseDriver> serviceLoader =
                ServiceLoader.load(DataBaseDriver.class);
        for(DataBaseDriver driver :serviceLoader){
            System.out.println(driver.connect("localhost"));
        }

    }
}
