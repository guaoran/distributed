package com.guaoran.source.dubbo.demo;

import com.alibaba.dubbo.container.Main;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;


/**
 * @author : 孤傲然
 * @Description :
 * @date :2018/6/14 18:50
 */
public class BootStrapMain {
    public static void main(String[] args) {
        Main.main(new String[]{"spring","log4j"});
    }
}
