# 目录

-   [dubbo](#dubbo)
    -   [能做什么？](#能做什么)
    -   [基本配置](#基本配置)
    -   [常用配置分析](#常用配置分析)
        -   [集权容错](#集权容错)
        -   [服务降级](#服务降级)
        -   [配置优先级别](#配置优先级别)
-   [Dubbo 的概略](#dubbo-的概略)
    -   [SPI 机制](#spi-机制)
        -   [Java SPI](#java-spi)
            -   [规范总结](#规范总结)
            -   [SPI 的缺点](#spi-的缺点)
        -   [Dubbo SPI](#dubbo-spi)
            -   [Dubbo的SPI机制规范](#dubbo的spi机制规范)
            -   [源码分析](#源码分析)
                -   [SPI 分析入口](#spi-分析入口)
                    -   [首先说的是
                        `ExtensionLoader.getExtensionLoader(Protocol.class).getAdaptiveExtension();`](#首先说的是-extensionloader.getextensionloaderprotocolclassgetadaptiveextension)
                    -   [最后要说的就是
                        `ExtensionLoader.getExtensionLoader(Protocol.class).getExtension("dubbo");`](#最后要说的就是-extensionloader.getextensionloaderprotocolclassgetextensiondubbo)
    -   [源码分析](#源码分析-1)
        -   [服务发布及注册流程](#服务发布及注册流程)
            -   [服务发布主要做了什么？](#服务发布主要做了什么)
        -   [客户端初始化启动过程](#客户端初始化启动过程)
            -   [客户端启动主要做了什么？](#客户端启动主要做了什么)
        -   [客户端调用过程](#客户端调用过程)
            -   [客户端调用主要做了什么？](#客户端调用主要做了什么)
    -   [LoadBalance](#loadbalance)
        -   [Random LoadBalance](#random-loadbalance)
        -   [RoundRobin LoadBalance](#roundrobin-loadbalance)
        -   [LeastActive LoadBalance](#leastactive-loadbalance)
        -   [ConsistentHash LoadBalance](#consistenthash-loadbalance)


# dubbo

## 能做什么？

> 透明化的远程方法调用，就像调用本地方法一样调用远程方法，
>
> 软负载均衡及容错机制，可在内网替代F5 等硬件负载均衡器，降低成本，减少单点
>
> 服务自动注册于发现，不再需要写死服务提供方地址，注册中心基于接口名查询服务提供者的IP地址

## 基本配置

ServerProvider

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://code.alibabatech.com/schema/dubbo
       http://code.alibabatech.com/schema/dubbo/dubbo.xsd">
    <!--应用中心-->
    <dubbo:application name="dubbo-server" owner="xxx"/>
    <!--注册中心 多个的话需要指定具体的哪个注册中心-->
    <dubbo:registry id="zk0" protocol="zookeeper" address="192.168.45.131:2181,192.168.45.134:2181"/>
    <dubbo:registry id="zk1" protocol="zookeeper" address="192.168.45.131:2181,192.168.45.134:2181"/>
    <!--协议-->
    <dubbo:protocol name="dubbo" port="20880"/>
    <dubbo:protocol name="hessian" port="20881"/>
    <!--服务对应的实现，注册中心和协议如果是多个的话，
		需要指定使用的是哪个注册中心和协议，也可以指定双协议,
		以及可以指定使用version进行多个版本的控制-->
    <dubbo:service interface="com.guaoran.source.dubbo.demo.IHelloService" 
                   ref="helloService" protocol="dubbo" registry="zk1" version="1.0.0" />
    <!--<dubbo:service interface="com.guaoran.source.dubbo.demo.IHelloService" 
				ref="helloService" protocol="hessian,dubbo"/> &lt;!&ndash;双协议&ndash;&gt;-->

    <dubbo:service interface="com.guaoran.source.dubbo.demo.IHelloService" 
                   ref="versionService" protocol="dubbo" registry="zk1"  version="1.0.1"/>

    <!--服务实现类 -->
    <bean id="helloService" class="com.guaoran.source.dubbo.demo.HelloServiceImpl"/>
    <!--服务实现类   version:多版本实现-->
    <bean id="versionService" class="com.guaoran.source.dubbo.demo.VersionServiceImpl"/>

</beans>
```

client

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:dubbo="http://code.alibabatech.com/schema/dubbo"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://code.alibabatech.com/schema/dubbo
       http://code.alibabatech.com/schema/dubbo/dubbo.xsd">
    <!--应用中心-->
    <dubbo:application name="dubbo-client" owner="xxx"/>
    <!--注册中心-->
    <dubbo:registry protocol="zookeeper" address="192.168.45.131:2181,192.168.45.134:2181"/>
    <!--可以指定服务的缓存路径-->
    <!--<dubbo:registry protocol="zookeeper" address="192.168.45.131:2181,192.168.45.134:2181" file="d:/dubbo-server"/>-->

    <!--check=false ：可以解决循环依赖的问题
        version :可以实现多版本控制，
        cluster :容错机制，有6种
        timeout ：超时时间
        mock    ：服务降级
            Class<?> mockClass = ConfigUtils.isDefault(mock) ? ReflectUtils.forName(interfaceClass.getName() + "Mock") : ReflectUtils.forName(mock);
            该参数有四个值，false,default,true,或者Mock类的类名。分别代表如下含义:
            false，不调用mock服务。
            true，当服务调用失败时，使用mock服务。 需要存在一个 《interfaceClass.getName() + "Mock"》的实现类，
            default，当服务调用失败时，使用mock服务。
            force，强制使用Mock服务(不管服务能否调用成功)。(使用xml配置不生效,使用ReferenceConfigAPI可以生效)
           
        -->
    <dubbo:reference id="helloService"
                     interface="com.guaoran.source.dubbo.demo.IHelloService"
                     version="1.0.0"
                     cluster="failfast"
                     timeout="5000"
                     mock="com.guaoran.source.dubbo.demo.MockServiceImpl"/>
    <!--
     Dubbo提供了6种容错机制，分别如下
        1.	failsafe 失败安全，可以认为是把错误吞掉（记录日志）
        2.	failover(默认)   重试其他服务器；失败后再重试两次retries（2） 	：适用于sql查询
        3.	failfast 快速失败， 失败以后立马报错     					：适用于事务操作
        4.	failback  失败后自动恢复。             	 					：适用于事务操作
        5.	forking  forks. 设置并行数
        6.	broadcast  广播，任意一台报错，则执行的方法报错
    -->
    <!--
        服务降级
        timeOut: mock
    -->

</beans>
```

## 常用配置分析

[精通Dubbo——Dubbo配置文件详解](https://blog.csdn.net/fuyuwei2015/article/details/72836075/)

### 集权容错

>什么是容错机制？ 容错机制指的是某种系统控制在一定范围内的一种允许或包容犯错情况的发生，举个简单例子，我们在电脑上运行一个程序，有时候会出现无响应的情况，然后系统会弹出一个提示框让我们选择，是立即结束还是继续等待，然后根据我们的选择执行对应的操作，这就是“容错”。
>
>在分布式架构下，网络、硬件、应用都可能发生故障，由于各个服务之间可能存在依赖关系，如果一条链路中的其中一个节点出现故障，将会导致雪崩效应。为了减少某一个节点故障的影响范围，所以我们才需要去构建容错服务，来优雅的处理这种中断的响应结果

 Dubbo提供了6种容错机制，分别如下

1. failsafe 失败安全，可以认为是把错误吞掉（记录日志）
2. failover(默认)   重试其他服务器；失败后再重试两次retries（2） 	：适用于sql查询
3. failfast 快速失败， 失败以后立马报错     					         ：适用于事务操作
4. failback  失败后自动恢复。             	 				                  ：适用于事务操作
5. forking  forks. 设置并行数
6. broadcast  广播，任意一台报错，则执行的方法报错



### 服务降级

> 降级的目的是为了保证核心服务可用

dubbo的降级方式： Mock

实现步骤

1.  在client端创建一个TestMock类，实现对应IHello的接口（需要对哪个接口进行mock，就实现哪个）
2.  在client端的xml配置文件中，添加如下配置，增加一个mock属性指向创建的TestMock
3.  模拟错误（设置timeout），模拟超时异常，运行测试代码即可访问到TestMock这个类。当服务端故障解除以后，调用过程将恢复正常



### 配置优先级别

以timeout为例，显示了配置的查找顺序，其它retries, loadbalance等类似。

1. 方法级优先，接口级次之，全局配置再次之。

   ```xml
    <dubbo:service interface="com.guaoran.source.dubbo.demo.IHelloService"
                      ref="versionService" timeout="30">
           <dubbo:method name="test" timeout="20"/>
       </dubbo:service>
   ```

2. 如果级别一样，则消费方优先，提供方次之。

其中，服务提供方配置，通过URL经由注册中心传递给消费方。

建议由服务提供方设置超时，因为一个方法需要执行多长时间，服务提供方更清楚，如果一个消费方同时引用多个服务，就不需要关心每个服务的超时设置。



# Dubbo 的概略

由于本人也是在学习中，在此记录只是为了学习和复习使用。不对之处希望大佬们能够指出。

在 dubbo 中 的核心的机制主要是 dubbo SPI 、服务的发布(export)、客户端初始化(refer)、服务调用(invoke)。其中贯彻至终的就是SPI。

## SPI 机制

### Java SPI

[参考配置](https://gitee.com/guaoran/distributed/tree/master/source-dubbo/SPI)

> SPI全称（service provider interface），是JDK内置的一种服务提供发现机制，目前市面上有很多框架都是用它来做服务的扩展发现，大家耳熟能详的如JDBC、日志框架都有用到；
>
> 简单来说，它是一种动态替换发现的机制。举个简单的例子，如果我们定义了一个规范，需要第三方厂商去实现，那么对于我们应用方来说，只需要集成对应厂商的插件，既可以完成对应规范的实现机制。 形成一种插拔式的扩展手段。

#### 规范总结

实现SPI，就需要按照SPI本身定义的规范来进行配置，SPI规范如下

1. 需要在classpath下创建一个目录，该目录命名必须是：META-INF/services

2. 在该目录下创建一个properties文件，该文件需要满足以下几个条件

   a)  文件名必须是扩展的接口的全路径名称

   b)  文件内部描述的是该扩展接口的所有实现类

   c)  文件的编码格式是UTF-8

3. 通过java.util.ServiceLoader的加载机制来发现

#### SPI 的缺点

1. JDK标准的SPI会一次性加载实例化扩展点的所有实现，什么意思呢？就是如果你在META-INF/service下的文件里面加了N个实现类，那么JDK启动的时候都会一次性全部加载。那么如果有的扩展点实现初始化很耗时或者如果有些实现类并没有用到，那么会很浪费资源
2. 如果扩展点加载失败，会导致调用方报错，而且这个错误很难定位到是这个原因

### Dubbo SPI

#### Dubbo的SPI机制规范

大部分的思想都是和SPI是一样，只是下面两个地方有差异。

1. 需要在resource目录下配置`META-INF/dubbo`或者`META-INF/dubbo/internal`或者`META-INF/services`，并基于SPI接口去创建一个文件
2. 文件名称和接口名称保持一致，文件内容和SPI有差异，内容是KEY对应Value

```java
public class MyProtocol implements Protocol {
    @Override
    public int getDefaultPort() {
        return 1111;
    }
    public static void main(String[] args) {
        Protocol protocol =
                ExtensionLoader.
                        getExtensionLoader(Protocol.class).
                        getExtension("myProtocol");
        System.out.println(protocol.getDefaultPort());
    }
}
```

![1546839571821](assets/1546839571821.png)



#### 源码分析

[Dubbo 官网](https://dubbo.incubator.apache.org/zh-cn/docs/source_code_guide/dubbo-spi.html)

```java
@SPI("dubbo")//扩展点
public interface MonitorFactory {
    @Adaptive("protocol")//适配器
    Monitor getMonitor(URL url);
}
@Adaptive
public class AdaptiveExtensionFactory implements ExtensionFactory {}
```

##### SPI 分析入口

```java
public static void main(String[] args) {
    //根据名称获得一个对应的扩展点
	ExtensionLoader.getExtensionLoader(Protocol.class).getExtension("dubbo");
    //获得自适应的扩展点
    ExtensionLoader.getExtensionLoader(Protocol.class).getAdaptiveExtension();
}
```

###### 首先说的是 `ExtensionLoader.getExtensionLoader(Protocol.class).getAdaptiveExtension();`

源码之前需要了解一些反射的方法

```java
public static void main(String[] args) throws NoSuchMethodException {
	// 判断该类上是否有 Adaptive 注解，如果有返回 true，否则返回 false
    System.out.println(MonitorFactory.class.isAnnotationPresent(Adaptive.class));
    System.out.println(AdaptiveExtensionFactory.class.isAnnotationPresent(Adaptive.class));
    System.out.println(Protocol.class.isAnnotationPresent(Adaptive.class));

    // 判断 ProtocolFilterWrapper 类是否存在 
    // public ProtocolFilterWrapper(Protocol protocol) 的构造方法，
    // 如果存在，返回该构造方法，不存在则报错
    ProtocolFilterWrapper.class.getConstructor(Protocol.class);
}
```



`ExtensionLoader.getExtensionLoader(Class<T> type)`  是ExtensionLoader的一个静态方法，在该方法中会从缓存中获取 `ExtensionLoader ` 对象，如果不存在则去调用私有的构造方法去 `new ExtensionLoader ()`  。

```java
//初始化一个 ExtensionLoader,如果缓存有，则从缓存获得，如果缓存没有，则初始化一个 ExtensionLoader
@SuppressWarnings("unchecked")
public static <T> ExtensionLoader<T> getExtensionLoader(Class<T> type) {
    if (type == null)
        throw new IllegalArgumentException("Extension type == null");
    // 如果 class 不是一个接口的话，则报错
    if (!type.isInterface()) {
        throw new IllegalArgumentException("Extension type(" + type + ") is not interface!");
    }
    //如果类上没有SPI注解，则报错
    if (!withExtensionAnnotation(type)) {
        throw new IllegalArgumentException
            ("Extension type(" + type +") is not extension, because WITHOUT @" + 
             SPI.class.getSimpleName() + " Annotation!");
    }
    ExtensionLoader<T> loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    if (loader == null) {
        // 调用私有的构造方法获得一个ExtensionLoader 实例
        EXTENSION_LOADERS.putIfAbsent(type, new ExtensionLoader<T>(type));
        loader = (ExtensionLoader<T>) EXTENSION_LOADERS.get(type);
    }
    return loader;
}
```

`getAdaptiveExtension()`  获得一个自适应扩展点,在该方法中，会通过双重校验从缓存中去取，取不出来会进行创建一个自适应的扩展点 `createAdaptiveExtension()` -> `getAdaptiveExtensionClass()` 去获得一个扩展点的class对象。

`getAdaptiveExtensionClass()` 是比较核心的方法，在此处会根据 `cachedAdaptiveClass` 是否为空去选择直接返回还是走下面的方法。

 ```java
//获得适配器的扩展点类，分两种，
//一种是自适应的类，一种是获得动态代理的
private Class<?> getAdaptiveExtensionClass() {
    //加载所有路径下的扩展点的实现类
    getExtensionClasses();
    if (cachedAdaptiveClass != null) {
        return cachedAdaptiveClass;
    }
    //TODO 什么时候会不执行下面的方法，?
    //TODO 当 clazz.isAnnotationPresent(Adaptive.class) 返回 true 时，
    //TODO 即当 Adaptive 注解在类级别上的时候，会认为是一个自适应的扩展点，会将该类直接赋值到 cachedAdaptiveClass 中

    //动态创建一个扩展点，即：type$Adpative 如：Protocol -> Protocol$Adpative
    //TODO 什么时候会动态去创建扩展点，
    //TODO 当 clazz.isAnnotationPresent(Adaptive.class) 返回 false 时，
    return cachedAdaptiveClass = createAdaptiveExtensionClass();
}
 ```

此时进入 ` getExtensionClasses();` 中，在方法中会去加载 `/META-INF/` 三个目录下的文件，即SPI的信息

```java
//加载所有路径下的扩展点的实现类
//这里获得的扩展点不包括 clazz.isAnnotationPresent(Adaptive.class) 和 clazz.getConstructor(type);
// 即loadFile() 方法里进入catch 块中的内容,不包含wrapper 和 类级别 Adaptive 注解
private Map<String, Class<?>> getExtensionClasses() {
    Map<String, Class<?>> classes = cachedClasses.get();
    if (classes == null) {
        synchronized (cachedClasses) {
            classes = cachedClasses.get();
            if (classes == null) {
                // 这里获得的扩展点不包括 
                // clazz.isAnnotationPresent(Adaptive.class) 和 clazz.getConstructor(type); 
                // 即只包含loadFile() 方法里进入catch 块中的内容,不包含wrapper 和 类级别 Adaptive 注解
                classes = loadExtensionClasses();
                cachedClasses.set(classes);
            }
        }
    }
    return classes;
}
// 分别从三个路径下去加载SPI扩展点
// 此方法已经getExtensionClasses方法同步过。
private Map<String, Class<?>> loadExtensionClasses() {
    //得到 SPI的注解
    final SPI defaultAnnotation = type.getAnnotation(SPI.class);
    if (defaultAnnotation != null) {
        //获得@SPI("dubbo") 注解里的value 值，且该值只能为一个
        String value = defaultAnnotation.value();
        if (value != null && (value = value.trim()).length() > 0) {
            String[] names = NAME_SEPARATOR.split(value);
            if (names.length > 1) {
                throw new IllegalStateException
                    ("more than 1 default extension name on extension " + type.getName()
                    + ": " + Arrays.toString(names));
            }
            if (names.length == 1) cachedDefaultName = names[0];
        }
    }

    Map<String, Class<?>> extensionClasses = new HashMap<String, Class<?>>();
    // dubbo 指定了三个路径，会从三个路径下去加载文件
    // META-INF/services/internal/
    loadFile(extensionClasses, DUBBO_INTERNAL_DIRECTORY);
    // META-INF/dubbo/
    loadFile(extensionClasses, DUBBO_DIRECTORY);
    // META-INF/services/
    loadFile(extensionClasses, SERVICES_DIRECTORY);
    return extensionClasses;
}
```

在loadFile 方法中，会去解析三个目录下的spi文件信息

`/META-INF/dubbo/internal/com.alibaba.dubbo.rpc.Protocol ` 存放的是接口的实现类

```properties
registry=com.alibaba.dubbo.registry.integration.RegistryProtocol
dubbo=com.alibaba.dubbo.rpc.protocol.dubbo.DubboProtocol
filter=com.alibaba.dubbo.rpc.protocol.ProtocolFilterWrapper
listener=com.alibaba.dubbo.rpc.protocol.ProtocolListenerWrapper
mock=com.alibaba.dubbo.rpc.support.MockProtocol
injvm=com.alibaba.dubbo.rpc.protocol.injvm.InjvmProtocol
rmi=com.alibaba.dubbo.rpc.protocol.rmi.RmiProtocol
hessian=com.alibaba.dubbo.rpc.protocol.hessian.HessianProtocol
```

该方法会去循环的去读取并实例化spi文件中的对象，并放到缓存中，而此时会有几处需要说下。

1. 当该实现类上存在 `Adaptive` 时，会将实例的对象进行缓存起来，这个缓存的值，就是 `getAdaptiveExtensionClass()` 中的 **cachedAdaptiveClass** ,所以在该方法里，只有当类上存在  `Adaptive` 注解时，会将该类直接返回。如果该接口类型下所有的实现类的类上都没有 `Adaptive` 注解的话，则走该方法下面的逻辑

2. 当该实现类上没有存在`@Adaptive`该注解时，则判断 `clazz.getConstructor(type);` ,即是否存在该实现类存在一个参数的构造方法，且这个构造方法的参数就是这个类的接口类。

   如：`Protocol.class ->  public ProtocolFilterWrapper(Protocol protocol)` ,

   如果存在则进行缓存到 **cachedWrapperClasses** ，该缓存的值会在 **`ExtensionLoader.getExtensionLoader(Protocol.class).getExtension("dubbo");`** 方法中用到，是对获得的实例对象进行包装，这块后面再说。

   如果不存在则报错，此时需要进入到catch 语句块中。

3. 当进入到catch语句中时，说明此时的实现类既不是 类上存在 `@Adaptive` 注解，也不存在 持有接口类作为参数的构造方法。这些类进行存储到 **extensionClasses**，在上面提到的 `loadExtensionClasse()` 方法中进行返回

这块代码比较长，就删除一些不重要的逻辑，需要看完整的可以到源码中查看 `com.alibaba.dubbo.common.extension.ExtensionLoader#loadFile`

```java
//解析指定路径下的文件，获取对应的扩展点，通过反射的方式进行实例化以后，put到extensionClasses这个Map集合中
private void loadFile(Map<String, Class<?>> extensionClasses, String dir) {
    
	//加载对应的实现类，并且判断实现类必须是当前的加载的扩展点的实现
    // @Adaptive如果是加在类上， 表示当前类是一个自定义的自适应扩展点 ，直接赋值
    //如果是加在方法级别上，表示需要动态创建一个自适应扩展点，也就是Protocol$Adaptive
    //TODO 获得该类上是否有 Adaptive 注解，存在返回 true
    if (clazz.isAnnotationPresent(Adaptive.class)) {
        if (cachedAdaptiveClass == null) {
            cachedAdaptiveClass = clazz;
        } else if (!cachedAdaptiveClass.equals(clazz)) {
            throw new IllegalStateException
                ("More than 1 adaptive class found: " 
                 + cachedAdaptiveClass.getClass().getName()
                 + ", " + clazz.getClass().getName());
        }
    } else {
        try {
            //判断 ProtocolFilterWrapper 
            // 类是否存在 public ProtocolFilterWrapper(Protocol protocol) 的构造方法，
            //判断 clazz 类 是否存在 public clazz(type t) 的构造方法，
            // 如果存在，返回该构造方法，不存在则报错
            clazz.getConstructor(type);

            //如果没有Adaptive注解，则判断当前类是否带有参数是type类型的构造函数，
            //如果没有，报错；如果有，则认为是wrapper类。这个wrapper实际上就是对扩展类进行装饰.
            //可以在dubbo-rpc-api/internal下找到Protocol文件，发现Protocol配置了2个装饰
            //分别是,filter/listener. 所以Protocol这个实例来说，会增加对应的装饰器
            Set<Class<?>> wrappers = cachedWrapperClasses;
            if (wrappers == null) {
                cachedWrapperClasses = new ConcurrentHashSet<Class<?>>();
                wrappers = cachedWrapperClasses;
            }
            //包装类：ProtocolFilterWrapper(ProtocolListenerWrapper(protocol))
            wrappers.add(clazz);
        } catch (NoSuchMethodException e) {
            clazz.getConstructor();
            //.........省略部分...
            String[] names = NAME_SEPARATOR.split(name);
            if (names != null && names.length > 0) {
                Activate activate = clazz.getAnnotation(Activate.class);
                if (activate != null) {
                    cachedActivates.put(names[0], activate);
                }
                for (String n : names) {
                    if (!cachedNames.containsKey(clazz)) {
                        cachedNames.put(clazz, n);
                    }
                    Class<?> c = extensionClasses.get(n);
                    if (c == null) {
                        // 这里的是对象的类上没有Adaptive 注解，在属性方法中，
                        // 也没有存放 public ProtocolFilterWrapper(Protocol protocol) 的构造方法，
                        // 这里进行缓存起来，在注入实例的时候用到 todo
                        extensionClasses.put(n, clazz);
                    } else if (c != clazz) {
                        throw new IllegalStateException
                            ("Duplicate extension " + type.getName() + " name " + n + " on " 
                             + c.getName() + " and " + clazz.getName());
                    }
                }
            }
        }
    }
}
```

###### 最后要说的就是 `ExtensionLoader.getExtensionLoader(Protocol.class).getExtension("dubbo");`

根据上面所述，这块就好理解了

```java
/**
  * 返回指定名字的扩展。如果指定名字的扩展不存在，则抛异常 {@link IllegalStateException}.
  *
  * @param name
  * @return
  */
@SuppressWarnings("unchecked")
public T getExtension(String name) {
    if (name == null || name.length() == 0)
        throw new IllegalArgumentException("Extension name == null");
    if ("true".equals(name)) {
        return getDefaultExtension();
    }
    Holder<Object> holder = cachedInstances.get(name);
    if (holder == null) {
        cachedInstances.putIfAbsent(name, new Holder<Object>());
        holder = cachedInstances.get(name);
    }
    Object instance = holder.get();
    if (instance == null) {
        synchronized (holder) {
            instance = holder.get();
            if (instance == null) {
                // 创建一个扩展点
                instance = createExtension(name);
                holder.set(instance);
            }
        }
    }
    return (T) instance;
}
// 获得指定名称的 扩展点，如果存在wrapper ，则会再此进行封装
@SuppressWarnings("unchecked")
private T createExtension(String name) {
    Class<?> clazz = getExtensionClasses().get(name);
    if (clazz == null) {
        throw findException(name);
    }
    try {
        T instance = (T) EXTENSION_INSTANCES.get(clazz);
        if (instance == null) {
            EXTENSION_INSTANCES.putIfAbsent(clazz, (T) clazz.newInstance());
            instance = (T) EXTENSION_INSTANCES.get(clazz);
        }
        injectExtension(instance);
        //如果 存在 clazz.getConstructor(type); 不报错， 则 cachedWrapperClasses 不为空
        //在 loadFile() 方法中 赋值的
        Set<Class<?>> wrapperClasses = cachedWrapperClasses;
        if (wrapperClasses != null && wrapperClasses.size() > 0) {
            for (Class<?> wrapperClass : wrapperClasses) {
                instance = injectExtension((T) 
                 wrapperClass.getConstructor(type).newInstance(instance));
            }
        }
        // 例如：如果是 DubboProtocol -> instance =
        // ProtocolFilterWrapper(ProtocolListenerWrapper(DubboProtocol))
        return instance;
    } catch (Throwable t) {
        throw new IllegalStateException
            ("Extension instance(name: " + name + ", class: " +
            type + ")  could not be instantiated: " + t.getMessage(), t);
    }
}
```



![getAdaptiveExtension](assets/getAdaptiveExtension.png)



## 源码分析

服务发布与注册、客户端注册、客户端调用，这个几块总结下来就是：

1. 服务发布：

   > export() 进行服务发布
   >
   > 1. 首先会获得注册中心集合列表以及协议集合列表，遍历协议中心列表进行多协议发布服务
   >
   > 2. 服务发布时，会根据配置文件的参数信息封装成map对象，最后将map对象转换成URL地址的参数
   >
   > 3. 根据URL地址的协议头：registry://xxx ,会在 `protocol.exort(url)` 中，进入到RegistryProtocol#export()方法中，在该方法中主要做了一下几步
   >
   >    > 1. 做本地发布：会根据协议将协议头由 `registry://` 转换成 `dubbo://` 协议，再次调用Protocol#export进入到DubboProtocol#exprot 中，这里是通过Netty 打开了一个监听
   >    > 2. 会根据注册中心的获得一个Registry, 即ZookeeperRegistry 并将服务注册到 zk上。
   >    > 3. 最后是发起一个订阅的功能，主要是能动态的去改变服务提供方提供的服务信息。
   >
   > 

2. 客户端注册：

   > getObject() 获得服务的代理对象
   >
   > 1. 首先或得在配置文件中的配置的参数信息，封装成map，进行创建代理对象
   >
   > 2. 在createProxy() 方法中，主要执行以下两步操作
   >
   >    > 1. 进行 Protocol#refer()操作，该方法主要有以下几步
   >    >
   >    >    > 1. 进入到RegistryProtocol#refer()方法中，打开一个zookeeper的连接 zookeeperRegistry
   >    >    > 2. 将consumer 协议注册到zk上，并进行订阅 provide、configurators、routes的变化，这里主要是将服务的列表信息存储到RegistryDirectory,并进行notify()进行及时的获得服务列表的变化以及将服务列表进行持久化到本地地址中。
   >    >    > 3. 最后根据熔断和降级的策略，一层层的将RegistryDirectory 封装成Invoker ：**`MockClusterInvoker(FailoverClusterInvoker(Directory)))`**
   >    >
   >    > 2. 获得代理对象，将上步获得Invoker 进行封装获得代理对象
   >    >
   >    >    > 此时获得一个代理对象，并将上步中的Invoke通过JavassistProxyFactory 再次进行包装，最后的结果是：**`Proxy0(InvokerInvocationHandler(MockClusterInvoker(FailoverClusterInvoker(Directory)))))`**
   >    >
   >    >    
   >
   > 

3. 客户端调用：

   > 即：我们调用服务的具体方法时 : `sayHello()`
   >
   > ```java
   > IHelloService helloService = (IHelloService) context.getBean("helloService");
   > // helloService = 
   > // Proxy0(InvokerInvocationHandler(MockClusterInvoker(FailoverClusterInvoker()))))
   > helloService.sayHello();
   > ```
   >
   > 会根据代理对象进行一层层的调用，
   >
   > 1. 首先会从InvokerInvocationHandler进入到MockClusterInvoker中 ，
   >
   >    > 会根据是否配置了Mock机制进行不同的逻辑操作，主要是直接调用目标方法、直接调用Mock方法、先调用目标方法，当出现RPCException时，进行调用Mock方法。这是降级的策略
   >
   > 2. 然后会进入到ClusterInvoker中，
   >
   >    > 1. 在服务熔断中，默认是Failover(失败重试)，此时会走FailoverClusterInvoker方法,根据不同的功能，会有不同的操作，Failover中会存在重试机制，如果失败后悔根据你是否配置了重试次数，如果没有重试次数，默认是再次重试两次
   >    > 2. 在进行调用时，会去获得一个服务，此时会从Directory 中获得维护的Invoker列表，并根据负载机制（随机、轮询、一致性hash、最少活跃数），去选择一个服务进行调用，
   >
   > 3. 最终通过 DubboInvoker 通过 Netty 进行与服务端进行通信
   >
   > 

   

实现 自定义的 schemas

![1546865616484](assets/1546865616484.png)



### 服务发布及注册流程

`ServiceBean` 发布(export)入口 

org.springframework.context.support.AbstractApplicationContext#refresh -> finishRefresh()  -> publishEvent()  ->  ...  ->  com.alibaba.dubbo.config.spring.ServiceBean#onApplicationEvent()



```java
public class ServiceBean<T> extends ServiceConfig<T> 
	implements InitializingBean, DisposableBean, 
		ApplicationContextAware, ApplicationListener, BeanNameAware {
    // InitializingBean 
    //为接口 bean 提供了初始化方法的方式，凡是实现了该接口的类，在初始化 bean 的时候会执行该方法
    @Override
    public void afterPropertiesSet() throws Exception {
    	//发布入口
        if (!isDelay()) {
        	export();
         }
    }

    // DisposableBean ，bean 被销毁的时候，spring 容器会自动执行 destroy 方法，比如释放资源
    @Override
    public void destroy() throws Exception {}

    // ApplicationContextAware 
    //实现了这个接口的类，当 spring 容器初始化的时候，会自动的将 ApplicationContext 注入进来
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) 
    	throws BeansException {}

    // ApplicationListener ,ApplicationEvent 事件监听，spring 容器启动后会发一个事件通知
    @Override
    public void onApplicationEvent(ApplicationEvent applicationEvent) {
		//发布入口
        export();
    }

    // BeanNameAware 获得自身初始化时，本身 bean 的 id 属性
    @Override
    public void setBeanName(String s) {}
}
```

#### 服务发布主要做了什么？

从 `com.alibaba.dubbo.config.ServiceConfig#doExportUrlsFor1Protocol`  核心入口开始说起

进行服务发布之前，会获得配置的注册中心集合、以及多协议集合，进行多协议的发布

```java
//发布服务
@SuppressWarnings({"unchecked", "rawtypes"})
private void doExportUrls() {
    //获得注册中心的url地址
    List<URL> registryURLs = loadRegistries(true);
    //根据 配置的多个协议，进行依次发布
    for (ProtocolConfig protocolConfig : protocols) {
        //发布服务根据协议
        doExportUrlsFor1Protocol(protocolConfig, registryURLs);
    }
}
//发布服务的核心入口
private void doExportUrlsFor1Protocol
	(ProtocolConfig protocolConfig, List<URL> registryURLs) {
	// 执行具体的远程调用
    // registryURL = registry://192.168.1.1.1...
    // proxyFactory = ProxyFactory$Adpative
    // 通过 ProxyFactory 获得 invoker 对象 ,
    // invoker = JavassistProxyFactory$1 代理对象 {url=registry://127.0.0.1..}
	Invoker<?> invoker = proxyFactory.
		getInvoker(ref, (Class) interfaceClass, 
			registryURL.
			addParameterAndEncoded(Constants.EXPORT_KEY, url.toFullString()));
    // 服务发布
    // protocol :Protocol$Adpative
    // invoker.url:registry://192.168.1.1.1...
    Exporter<?> exporter = protocol.export(invoker);
    exporters.add(exporter);
}
```

`Protocol$Adpative` 类信息

```java
/**
 * @author : guaoran
 * @Description : <br/>
 *  根据 createAdaptiveExtensionClass() 动态获得的 适配器扩展点
 * @date :2019/1/7 13:31
 */
public class Protocol$Adpative implements Protocol {
    //....
    //服务发布
    public Exporter export(Invoker arg0) throws RpcException {
        if (arg0 == null) throw new IllegalArgumentException("Invoker argument == null");
        if (arg0.getUrl() == null)
            throw new IllegalArgumentException("Invoker argument getUrl() == null");
        //当url=registry://192.168.45.123....时，extension = RegistryProtocol
        URL url = arg0.getUrl();
        String extName = (url.getProtocol() == null ? "dubbo" : url.getProtocol());
        if (extName == null)
            throw new IllegalStateException
            ("Fail to get extensionProtocol) name from url(" + 
             url.toString() + ") use keys([protocol])");
        // 当url=registry://192.168.45.1...时，
        //  extension = ProtocolListenerWrapper(ProtocolFilterWrapper(RegistryProtocol))
        // 当url=dubbo://192.168.45.1...时，
        //  extension = ProtocolListenerWrapper(ProtocolFilterWrapper(DubboProtocol)) ,
        // todo 为什么 extension 不是 [DubboProtocol] ,
        // todo 因为 在 Protocol 进行loadFile 的时候，会加载 到 clazz.getConstructor(type); 
        //  不报错则 cachedWrapperClasses 不为空,
        // todo 所以在 getExtension("dubbo") 的时候会首先获得 DubboProtocol,
        //  然后会进行封装成 wrapper
        Protocol extension = (Protocol) 
            ExtensionLoader.getExtensionLoader(Protocol.class).getExtension(extName);
        return extension.export(arg0);
    }

    //客户端引用
    public Invoker refer(Class arg0, URL arg1) throws RpcException {
        if (arg1 == null) throw new IllegalArgumentException("url == null");
        URL url = arg1;
        String extName = (url.getProtocol() == null ? "dubbo" : url.getProtocol());
        if (extName == null)
            throw new IllegalStateException
            ("Fail to get extension(Protocol) name from url(" + 
             url.toString() + ") use keys([protocol])");
        //此时通过url = registry 时 
        // extension = ProtocolListenerWrapper(ProtocolFilterWrapper(RegistryProtocol))
        Protocol extension = (Protocol) 
            ExtensionLoader.getExtensionLoader(Protocol.class).getExtension(extName);
        return extension.refer(arg0, arg1);
    }
}
```

继续接着 **`Exporter<?> exporter = protocol.export(invoker);`**  走，此时会调用 Protocol$Adpative#export()方法，会通过 `getExtensionLoader("registry")` 方法获得到 进行Wrapper 包装后的 `RegistryProtocol`,进入到该类的 `RegistryProtocol#export()` 方法中。

**`RegistryProtocol#export()`** 方法主要做了以下几步：

1. 做本地发布，在 `DubboProtocol` 打开一个 Netty 服务进行监听

2. 获得一个 ZookeeperRegistry 的注册中心的连接，并将服务注册到 zk 上

   `/dubbo-demo/com.guaoran.source.dubbo.demo.IHelloService/providers/dubbo%3A%2F%2F19..`

3. 开启一个订阅

   在 zk 上注册一个 `/dubbo-demo/com.guaoran.source.dubbo.demo.IHelloService/configurators` 节点

```java
public <T> Exporter<T> export(final Invoker<T> originInvoker) throws RpcException {
    //export invoker
    // 做本地发布
    final ExporterChangeableWrapper<T> exporter = doLocalExport(originInvoker);
    //registry provider
    // 获得一个注册中心的连接 ZookeeperRegistry
    final Registry registry = getRegistry(originInvoker);
    // 获得需要注册到 zk 上的协议地址，即：dubbo://xxx
    final URL registedProviderUrl = getRegistedProviderUrl(originInvoker);
    // 注册到 zk ：zkClient.create() ， ZookeeperRegistry 的父类
    registry.register(registedProviderUrl);
    // 订阅override数据
    // FIXME 提供者订阅时，会影响同一JVM即暴露服务，又引用同一服务的的场景，
    // FIXME 因为subscribed以服务名为缓存的key，导致订阅信息覆盖。
    final URL overrideSubscribeUrl = getSubscribedOverrideUrl(registedProviderUrl);
    final OverrideListener overrideSubscribeListener = 
        new OverrideListener(overrideSubscribeUrl);
    overrideListeners.put(overrideSubscribeUrl, overrideSubscribeListener);
    registry.subscribe(overrideSubscribeUrl, overrideSubscribeListener);
    //保证每次export都返回一个新的exporter实例
    return new Exporter<T>() {
    }
```



![1546940235215](assets/1546940235215.png)





### 客户端初始化启动过程

`ReferenceBean` 启动入口

```java
public class ReferenceBean<T> extends ReferenceConfig<T> 
	implements FactoryBean, ApplicationContextAware, InitializingBean, DisposableBean {
    @Override
    public Object getObject() throws Exception {
        return get();
    }
    @Override
    public Class getObjectType() {return null;}

    @Override
    public boolean isSingleton() {return false;}

    @Override
    public void afterPropertiesSet() throws Exception {
    	//启动入口
        getObject();
    }
    
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) 
    	throws BeansException {}
}
```

#### 客户端启动主要做了什么？

从 `com.alibaba.dubbo.config.ReferenceConfig#createProxy` 创建代理对象核心入口开始说起

将`consumer://` 协议注册到zk上，并订阅zk其他三个目录的变化,并拿到服务方提供的服务列表，并创建 `Proxy0(InvokerInvocationHandler(MockClusterInvoker))` 代理对象

 ```java
@SuppressWarnings({"unchecked", "rawtypes", "deprecation"})
private T createProxy(Map<String, String> map) {
    // 当只有一个注册中心的时候
    if (urls.size() == 1) {
        // refprotocol = Protocol$Adaptive ，==> 
        // ProtocolListenerWrapper(ProtocolFilterWrapper(RegistryProtocol()))
        // invoker = MockClusterInvoker
        invoker = refprotocol.refer(interfaceClass, urls.get(0));
    }

    // 创建服务代理
    // proxyFactory = ProxyFactory$Adaptive  ==> 
    // StubProxyFactoryWrapper(JavassistProxyFactory())
    // todo 代理的是什么？
    // 由于Dubbo 支持服务降级和熔断机制，这里是服务的Invoker实例进行一层层的包装
    // 根据顺序依次是Proxy0(InvokerInvocationHandler(Invoker()))
    // 由于在Protocol.refer()中进行cluster.join()操作，
    // cluster.join() 该方法是将服务封装到RegistryDirectory中，并根据服务的降级和熔断一层层的进行封装
    // 最后会封装成 MockClusterInvoker(FailoverClusterInvoker(Directory)))
    // 最终返回的代理对象就是 Proxy0(InvokerInvocationHandler(MockClusterInvoker(FailoverClusterInvoker(Directory)))))
    return (T) proxyFactory.getProxy(invoker);
}
 ```



![client_init](assets/client_init.png)



### 客户端调用过程

`InvokerInvocationHandler` 调用入口

```java
public static void main(String[] args) throws IOException {
    ClassPathXmlApplicationContext classPathXmlApplicationContext =
        new ClassPathXmlApplicationContext("dubbo-client.xml");
    IHelloService helloService = (IHelloService) 
        	classPathXmlApplicationContext.getBean("helloService");
    // 因为 helloService 实际上是 Proxy0(InvokerInvocationHandler(MockClusterInvoker()))
    // 所以会走到 InvokerInvocationHandler.invoke() 中
    System.out.println(helloService.sayHello("World"));
}
```

#### 客户端调用主要做了什么？

客户端对象拿到代理对象后，进行调用，调用时会根据熔断和降级的策略 以及获得提供方提供的所有服务列表根据负载均衡规则，进行服务调用，最终通过 DubboProtocol 调用 Netty 进行与服务端进行通信

`com.alibaba.dubbo.rpc.cluster.support.wrapper.MockClusterInvoker#invoke` 开始主要有以下几步：

1. 判断配置是否配置了mock 规则，按照规则进行调用，
2. 调用到 `FailoverClusterInvoker#doInvoke()` ，这里是进行熔断处理，默认是failover:失败重试（2）
3. 获得到 Invoker 列表根据LoadBalance 规则进行负载
4. 最终调用到`DubboInvoker#doInvoke()` 调用Netty 与服务端进行通信

```java
// MockClusterInvoker#invoke
// 远程调用 ：根据 是否配置了 降级（mock） 进行调用
public Result invoke(Invocation invocation) throws RpcException {
    Result result = null;
    /*
    <dubbo:reference id="helloService"
    interface="com.guaoran.source.dubbo.demo.IHelloService"
    version="1.0.0"
    cluster="failfast"
    timeout="5000"
    mock="com.guaoran.source.dubbo.demo.MockServiceImpl"/>
    */

    // 获取 dubbo 配置文件中是否配置了 mock 机制
    String value = directory.getUrl().
        getMethodParameter
        (invocation.getMethodName(), Constants.MOCK_KEY, 
         Boolean.FALSE.toString()).trim();

    if (value.length() == 0 || value.equalsIgnoreCase("false")) {
        //no mock
        // 直接调用 invoker = 配置文件中的 cluster
        result = this.invoker.invoke(invocation);
    } else if (value.startsWith("force")) {
        if (logger.isWarnEnabled()) {
            logger.info("force-mock: " + 
                        invocation.getMethodName() + 
                        " force-mock enabled , url : " + 
                        directory.getUrl());
        }
        //force:direct mock
        // 表示强制调用 mock 的方法，不再调用原始的方法
        result = doMockInvoke(invocation, null);
    } else {
        //fail-mock
        try {
            // 调用原始的方法，如果出现 RpcException 异常 ,则执行 mock 方法
            result = this.invoker.invoke(invocation);
        } catch (RpcException e) {
            if (e.isBiz()) {
                throw e;
            } else {
                if (logger.isWarnEnabled()) {
                    logger.info("fail-mock: " + 
                                invocation.getMethodName() + 
                                " fail-mock enabled , url : " + 
                                directory.getUrl(), e);
                }
                result = doMockInvoke(invocation, e);
            }
        }
    }
    return result;
}
```

```java
// FailoverClusterInvoker#doInvoke
public Result doInvoke(Invocation invocation, final List<Invoker<T>> invokers, LoadBalance loadbalance) throws RpcException {
    List<Invoker<T>> copyinvokers = invokers;
    checkInvokers(copyinvokers, invocation);
    // 重试次数，默认两次， 加上第一次调用总共是三次，如果配置重试次数是0，则不进行重试，只走原始的一次调用
    int len = getUrl().getMethodParameter(invocation.getMethodName(), Constants.RETRIES_KEY, Constants.DEFAULT_RETRIES) + 1;
    if (len <= 0) {
        len = 1;
    }
    // retry loop.
    RpcException le = null; // last exception.
    List<Invoker<T>> invoked = new ArrayList<Invoker<T>>(copyinvokers.size()); 
    Set<String> providers = new HashSet<String>(len);
    for (int i = 0; i < len; i++) {
        //重试时，进行重新选择，避免重试时invoker列表已发生变化.
        //注意：如果列表发生了变化，那么invoked判断会失效，因为invoker示例已经改变
        if (i > 0) {
            checkWhetherDestroyed();
            copyinvokers = list(invocation);
            //重新检查一下
            checkInvokers(copyinvokers, invocation);
        }
        // 随机负载机制 去获得一个服务进行调用
        Invoker<T> invoker = select(loadbalance, invocation, copyinvokers, invoked);
        invoked.add(invoker);
        RpcContext.getContext().setInvokers((List) invoked);
        try {
            Result result = invoker.invoke(invocation);

            // 如果成功，直接返回
            return result;
        } catch (RpcException e) {
            if (e.isBiz()) { // biz exception.
                throw e;
            }
            le = e;
        } catch (Throwable e) {
            le = new RpcException(e.getMessage(), e);
        } finally {
            providers.add(invoker.getUrl().getAddress());
        }
    }
    throw new RpcException(...);
}
```



![client_transport_server](assets/client_transport_server.png)



## LoadBalance

### Random LoadBalance

  随机负载 引入了权重，判断权重是否相同，如果相同，就随机取一个，如果权重不同，获得总权重的随机值，然后通过权重来判断该随机值会落到哪个片段上

- **随机**，按权重设置随机概率。
- 在一个截面上碰撞的概率高，但调用量越大分布越均匀，而且按概率使用权重后也比较均匀，有利于动态调整提供者权重。

```java
public class RandomLoadBalance extends AbstractLoadBalance {
    public static final String NAME = "random";
    private final Random random = new Random();
    protected <T> Invoker<T> doSelect(List<Invoker<T>> invokers, URL url, Invocation invocation) {
        // 如：四个服务，权重分别是 1,2,3,4
        int length = invokers.size(); // 总个数 ：4
        int totalWeight = 0; // 总权重 ：10
        boolean sameWeight = true; // 权重是否都一样
        for (int i = 0; i < length; i++) {
            int weight = getWeight(invokers.get(i), invocation);
            totalWeight += weight; // 累计总权重
            if (sameWeight && i > 0
                && weight != getWeight(invokers.get(i - 1), invocation)) {
                sameWeight = false; // 计算所有权重是否一样
            }
        }
        if (totalWeight > 0 && !sameWeight) {
            // 如果权重不相同且权重大于0则按总权重数随机
            int offset = random.nextInt(totalWeight);
            // 并确定随机值落在哪个片断上
            // 片段的规则：10%，20%，30%，40%
            for (int i = 0; i < length; i++) {
                offset -= getWeight(invokers.get(i), invocation);
                if (offset < 0) {
                    return invokers.get(i);
                }
            }
        }
        // 如果权重相同或权重为0则均等随机
        return invokers.get(random.nextInt(length));
    }
}
```



### RoundRobin LoadBalance

- **轮询**，按公约后的权重设置轮询比率。
- 存在慢的提供者累积请求的问题，比如：第二台机器很慢，但没挂，当请求调到第二台时就卡在那，久而久之，所有请求都卡在调到第二台上。

### LeastActive LoadBalance

- **最少活跃调用数**，相同活跃数的随机，活跃数指调用前后计数差。
- 使慢的提供者收到更少请求，因为越慢的提供者的调用前后计数差会越大。

### ConsistentHash LoadBalance

- **一致性 Hash**，相同参数的请求总是发到同一提供者。
- 当某一台提供者挂时，原本发往该提供者的请求，基于虚拟节点，平摊到其它提供者，不会引起剧烈变动。
- 算法参见：<http://en.wikipedia.org/wiki/Consistent_hashing>
- 缺省只对第一个参数 Hash，如果要修改，请配置 `<dubbo:parameter key="hash.arguments" value="0,1" />`
- 缺省用 160 份虚拟节点，如果要修改，请配置 `<dubbo:parameter key="hash.nodes" value="320" />`