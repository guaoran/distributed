# 目录
-   [安装 Nagios](#安装-nagios)
    -   [安装 Apache](#安装-apache)
    -   [安装Nagios](#安装nagios)
        -   [安装环境包](#安装环境包)
        -   [创建管理用户](#创建管理用户)
        -   [创建安装目录，指定属主属组](#创建安装目录指定属主属组)
        -   [编译安装nagios](#编译安装nagios)
        -   [安装nagios-plugins](#安装nagios-plugins)
    -   [配置nagios 监控系统](#配置nagios-监控系统)
    -   [配置nagios 监控MySQL](#配置nagios-监控mysql)
        -   [**使用check\_mysql\_health**](#使用check_mysql_health)
            -   [创建一个用户](#创建一个用户)
            -   [校验 check\_mysql\_health
                插件](#校验-check_mysql_health-插件)
            -   [添加配置](#添加配置)
            -   [其他命令-具体看官方文档](#其他命令-具体看官方文档)
    -   [配置nagios 监控磁盘](#配置nagios-监控磁盘)
        -   [安装 nrpe](#安装-nrpe)
        -   [配置监控本地主机磁盘](#配置监控本地主机磁盘)
        -   [配置监控远程主机磁盘](#配置监控远程主机磁盘)
    -   [配置监控CPU、内存](#配置监控cpu内存)
        -   [查看文件是否可执行](#查看文件是否可执行)
        -   [修改配置文件](#修改配置文件)

# 安装 Nagios

打开Nagios官方的文档，会发现Nagios基本上没有什么依赖包，只要求系统是Linux或者其他Nagios支持的统。不过如果你没有安装apache（http服务），那么你就没有那么直观的界面来查看监控信息了，所以apache姑且算是一个前提条件。

## 安装 Apache

```shell
sudo yum install httpd
sudo service httpd start
```

```bash
./configure --prefix=/usr/local/nagios --with-nagios-user=nagios --with-nagios-group=nagios --with-perl --with-statefiles-dir=/tmp
```



## 安装Nagios

### 安装环境包

```shell
yum install -y \
-- gcc \
-- glibc \
-- glibc-common \
-- gd \
-- gd-devel \
-- xinetd \
-- openssl-devel
```

### 创建管理用户

```shell
useradd -s /sbin/nologin nagios
```

### 创建安装目录，指定属主属组

```shell
mkdir /usr/local/nagios
chown -R nagios.nagios /usr/local/nagios
```

### 编译安装nagios

[Nagios下载地址](https://www.nagios.org/downloads/nagios-core/thanks/?product_download=nagioscore-source)

```shell
wget https://assets.nagios.com/downloads/nagioscore/releases/nagios-4.4.3.tar.gz
tar -zxvf nagios-4.4.3.tar.gz
cd nagios-4.4.3
./configure --prefix=/usr/local/nagios
make all
make install
make install-init
make install-commandmode
make install-config
```

### 安装nagios-plugins

[Nagios-plugins下载地址](https://www.nagios.org/downloads/nagios-plugins)

```shell
wget https://nagios-plugins.org/download/nagios-plugins-2.2.1.tar.gz
tar -zxvf nagios-plugins-2.2.1.tar.gz
cd nagios-plugins-2.2.1
./configure --prefix=/usr/local/nagios
make
make install
```



## 配置nagios 监控系统

[监控三剑客之Nagios](https://blog.51cto.com/13643643/2163630)

## 配置nagios 监控MySQL

###  **使用check_mysql_health**

[check_mysql_health官方介绍主页](http://labs.consol.de/nagios/check_mysql_health/)

```shell
wget https://labs.consol.de/assets/downloads/nagios/check_mysql_health-2.2.2.tar.gz
tar -zxvf check_mysql_health-2.2.2.tar.gz
cd check_mysql_health-2.2.2
./configure --prefix=/usr/local/nagios --with-nagios-user=nagios --with-nagios-group=nagios --with-perl=/usr/bin/perl --with-statefiles-dir=/usr/local/nagios/var/tmp
```

#### 创建一个用户

```mysql
grant usage on *.* to 'nagios'@'%' identified by 'nagios';
```

#### 校验 check_mysql_health 插件

```shell
./check_mysql_health --hostname 192.168.192.129 --port 3306 -username nagios --password nagios --mode connection-time  --warning 0.001 --critical 0.0011

-- 如果没有 Perl 
yum install 'perl(Data::Dumper)'
-- 如果没有 DBI.pm
yum install perl-DBI perl-DBD-MySQL -y
```

#### 添加配置

服务器监控(自定义)  : `vim /usr/local/nagios/etc/objects/hosts.cfg`

```properties
define host{   
        use                     linux-server     ; 引用主机linux-server的属性信息，linux-server主机在templates.cfg文件中进行了定义。
        host_name               monitor_mysql_60 ; 主机名，自定义
        alias                   mon_60           ; 主机别名
        address                 192.168.20.60    ; 被监控的主机地址，这个地址可以是ip，也可以是域名。
}   

define hostgroup{                                 ; 定义一个主机组
        hostgroup_name          monitor-servers   ; 主机组名称，可以随意指定。
        alias                   monitorServers    ; 主机组别名
        members                 monitor_mysql_60  ; 主机组成员，其中"monitor_60”就是上面定义的主机。     
}
```

服务器监控(自定义) : `vim /usr/local/nagios/etc/objects/services.cfg`

```properties
define service{  
        use                     local-service      ; 引用local-service服务的属性值，local-service在templates.cfg文件中进行了定义。
        host_name               monitor_mysql_60   ; 指定要监控哪个主机上的服务，monitor_mysql_60 在hosts.cfg文件中进行了定义。
        service_description     check-host-alive   ; 对监控服务内容的描述，以供维护人员参考。
        check_command           check-host-alive   ; 指定检查的命令。
}
```

配置nagios命令行文件 : `vim /usr/local/nagios/etc/objects/commands.cfg`

```properties
# check_mysql_health command definition
define command{
	command_name check_mysql_health
	command_line $USER1$/check_mysql_health --hostname $ARG1$ --port $ARG2$ --username $ARG3$ --password $ARG4$ --mode $ARG5$ --warning $ARG6$ --critical $ARG7$
}
```

配置监控服务项(自定义) : `vim /usr/local/nagios/etc/objects/mysqlservices.cfg`

```properties
define service{
        use local-service
        host_name monitor_mysql_60
        service_description check_mysql_connection_time
        check_command check_mysql_health!192.168.20.60!3306!nagios!nagios!connection-time!2!10
}
define service{
        use local-service
        host_name monitor_mysql_60
        service_description check_mysql_uptime
        check_command check_mysql_health!192.168.20.60!3306!nagios!nagios!uptime!10080!44640
}
define service{
        use local-service
        host_name monitor_mysql_60
        service_description check_mysql_connections
        check_command check_mysql_health!192.168.20.60!3306!nagios!nagios!threads-connected!70!150
}
define service{
        use local-service
        host_name monitor_mysql_60
        service_description check_mysql_table_lock
        check_command check_mysql_health!192.168.20.60!3306!nagios!nagios!table-lock-contention!1!2
}
define service{
        use local-service
        host_name monitor_mysql_60
        service_description check_mysql_slow_queries
        check_command check_mysql_health!192.168.20.60!3306!nagios!nagios!slow-queries!1!2
}
```

添加自定义的`mysqlservices.cfg` 到配置文件中 : `/usr/local/nagios/etc/nagios.cfg`

```properties
cfg_file=/usr/local/nagios/etc/objects/hosts.cfg
cfg_file=/usr/local/nagios/etc/objects/services.cfg
cfg_file=/usr/local/nagios/etc/objects/mysqlservices.cfg
```

校验配置

```shell
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
```

重启服务

```shell
systemctl restart httpd.service 
systemctl restart nagios.service
```



#### 其他命令-具体看官方文档

```shell
-- 连接服务器的时间
sudo ./check_mysql_health --hostname 192.168.20.60 --username nagios --password nagios --mode connection-time

sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode connection-time  --warning 0.001 --critical 0.0011

-- 服务器运行的时间
sudo ./check_mysql_health --hostname 192.168.20.60 --username nagios --password nagios --mode uptime-time

-- 当前打开的连接数
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode threads-connected

-- 线程缓存的命中率
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode threadcache-hitrate

-- 每秒创建的线程数
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode threads-created

-- 当前运行的线程数
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode threads-running

-- 当前缓存的线程数
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode threads-cached

-- 连接终止：每秒中止连接的数量
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode connects-aborted

-- 客户端终止
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode clients-aborted

-- 查询缓存命中率
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode querycache-hitrate 

-- 由于内存不足而修剪的查询缓存条目
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode qcache-lowmem-prunes

-- MyISAM key 缓存命中率
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode keycache-hitrate

-- InnoDB 缓冲池命中率
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode bufferpool-hitrate

-- InnoDB缓冲池等待可用的
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode bufferpool-wait-free

-- 由于日志缓冲区太小，InnoDB日志等待
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode log-waits

-- 表缓存命中率
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode tablecache-hitrate

-- 表锁争用
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode table-lock-contention

-- 在磁盘上创建的临时表的百分比
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode tmp-disk-tables


-- 长时间运行的进程
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode long-running-procs

-- 索引
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode index-usage

-- 显示应优化的表
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode table-fragmentation

-- 已打开文件的百分比
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode open-files

-- 慢查询
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode slow-queries

-- 任何返回单个数字的sql命令
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode sql --name 'select 111 from dual'
 
sudo ./check_mysql_health --hostname 192.168.20.60 --port 3306 --username nagios --password nagios --mode sql --name 'select 111 from dual' --name2 myval --units GB

```



## 配置nagios 监控磁盘

NRPE是监控软件nagios的一个扩展，它被用于被监控的服务器上，向nagios监控平台提供该服务器的一些本地的情况。例如，cpu负载、内存使用、硬盘使用等等。NRPE可以称为nagios的for linux 客户端。

### 安装 nrpe

```shell
yum install epel-release -y
yum install nrpe -y

wget https://jaist.dl.sourceforge.net/project/nagios/nrpe-3.x/nrpe-3.1.0.tar.gz
tar -zxvf nrpe-3.1.0.tar.gz
cd nrpe-3.1.0/
./configure --prefix=/usr/local/nagios
make all
make install-plugin
make install-daemon
make install-daemon-config
```

完成后查看 对应的 `bin` / `libexec` 是否出现 该文件

```shell
[guaoran@localhost nagios]$ ls -l /usr/local/nagios/bin/nrpe
-rwxr-xr-x. 1 root root 215928 6月  12 15:34 /usr/local/nagios/bin/nrpe
[guaoran@localhost nagios]$ ls -l /usr/local/nagios/libexec/check_nrpe 
-rwxrwxr-x. 1 nagios nagios 142256 6月  12 15:34 /usr/local/nagios/libexec/check_nrpe
[guaoran@localhost nagios]$ 
```

启动 `nrpe` 服务

```shell
[guaoran@localhost nrpe.d]$ sudo service nrpe start
Redirecting to /bin/systemctl start  nrpe.service
[guaoran@localhost nrpe.d]$ ps -ef|grep nrpe
nrpe     24154     1  0 16:04 ?        00:00:00 /usr/sbin/nrpe -c /etc/nagios/nrpe.cfg -f
guaoran  24167 29334  0 16:04 pts/1    00:00:00 grep --color=auto nrpe
```

从进程中可以看到 `/etc/nagios/nrpe.cfg` 的位置，指定软连接到 `/usr/local/nagios/etc/nrpe.cfg`

```shell
ln -s /etc/nagios/nrpe.cfg /usr/local/nagios/etc/
```

### 配置监控本地主机磁盘

这里说明一下

* 本地主机指：`192.168.20.2`  

* 远程主机指：`192.168.20.60`

HOSTS 配置文件 ： `cat /usr/local/nagios/etc/objects/hosts.cfg `

```properties
define host{
        use                     linux-server         ; 引用主机linux-server的属性信息，linux-server主机在templates.cfg文件中进
行了定义。
        host_name               monitor_mysql_local  ; 主机名，自定义
        alias                   monitor-local        ; 主机别名
        address                 192.168.20.2         ; 被监控的主机地址，这个地址可以是ip，也可以是域名。
} 
define host{
        use                     linux-server        ; 引用主机linux-server的属性信息，linux-server主机在templates.cfg文件中进行了定义。
        host_name               monitor_mysql_60    ; 主机名，自定义
        alias                   monitor_60          ; 主机别名
        address                 192.168.20.60       ; 被监控的主机地址，这个地址可以是ip，也可以是域名。
}   

define hostgroup{                                 					  ; 定义一个主机组
        hostgroup_name          monitor-servers   					  ; 主机组名称，可以随意指定。
        alias                   monitorServers    					  ; 主机组别名
        members                 monitor_mysql_60,monitor_mysql_local  ; 主机组成员，其中 "monitor_60”就是上面定义的主机。   
}
```

修改命令配置文件 ： `vim /usr/local/nagios/etc/objects/commands.cfg `

```properties
# 'check_nrpe ' command definition
define command{
        command_name check_nrpe
        command_line $USER1$/check_nrpe -H $ARG1$ -c $ARG2$       
} 
```

修改服务配置文件 ： `vim /usr/local/nagios/etc/objects/services.cfg `

```properties
define service{  
        use                     generic-service      ; 引用local-service服务的属性值，local-service在templates.cfg文件中进行了定义。
        host_name               monitor_mysql_60   ; 指定要监控哪个主机上的服务，monitor_60 在hosts.cfg文件中进行了定义。
        service_description     check-host-alive   ; 对监控服务内容的描述，以供维护人员参考。
        check_command           check-host-alive   ; 指定检查的命令。
}

define service{
         use                    generic-service
         host_name              monitor_mysql_60
         service_description    check-disk-mysql-data
         check_command          check_nrpe!192.168.20.60!check-disk-mysql-data
}
define service{
         use                    generic-service
         host_name              monitor_mysql_local
         service_description    check-disk-root
         check_command          check_nrpe!127.0.0.1!check-disk-root
}   
```

修改 nrpe 配置文件 ： `vim /usr/local/nagios/etc/nrpe.cfg `

```properties
#allowed_hosts=127.0.0.1,::1
allowed_hosts=127.0.0.1,192.168.20.2

command[check-disk-mysql-data]=/usr/local/nagios/libexec/check_disk -w 20% -c 10% -p /mnt
command[check-disk-root]=/usr/local/nagios/libexec/check_disk -w 20% -c 10% -p /
```

校验配置

```shell
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg
```

重启服务

```shell
systemctl restart httpd.service 
systemctl restart nagios.service
systemctl restart nrpe.service
```

使用 `nrpe` 测试下

```shell
[guaoran@localhost etc]$  /usr/local/nagios/libexec/check_nrpe -H 192.168.20.2 -c check-disk-root
DISK WARNING - free space: / 9649 MB (18.85% inode=98%);| /=41525MB;40940;46057;0;51175
[guaoran@localhost etc]$  /usr/local/nagios/libexec/check_nrpe -H 192.168.20.60 -c check-disk-mysql-data
connect to address 192.168.20.60 port 5666: No route to host
connect to host 192.168.20.60 port 5666: No route to host
```

***此时 本地主机是通的远程主机不通，看来是我理解错了，远程主机也应该需要 nrpe 的服务***

### 配置监控远程主机磁盘

在上面的配置中可以看到监控远程主机的话，需要在远程主机上 安装 `nrpe`
服务及 可以支持 `check_disk` 的 `nagios-plugins` 插件，其他的服务可以不用安装了。

- [安装环境包](#安装环境包)
- [创建管理用户](#创建管理用户)
- [创建安装目录，指定属主属组](#创建安装目录指定属主属组) 
- ~~[编译安装nagios](#null)~~ ***再次标注下，该服务不用安装***
- [安装nagios-plugins](#安装nagios-plugins)
- [安装 nrpe](#安装-nrpe)

此时远程主机的环境已经配置好，需要做的就是将  `/usr/local/nagios/etc/nrpe.cfg` 配置文件改动下

```properties
# allowed_hosts=127.0.0.1,::1
# 这里允许 20.2 服务来访问
allowed_hosts=127.0.0.1,192.168.20.2
# 将20.2 主机上的 改文件剪切或复制过来
command[check-disk-mysql-data]=/usr/local/nagios/libexec/check_disk -w 20% -c 10% -p /mnt
```

重启服务

```shell
systemctl restart nrpe.service
```

在本地主机测试下

```shell
[guaoran@localhost nagios]$ /usr/local/nagios/libexec/check_nrpe -H 192.168.20.60 -c check-disk-mysql-data
DISK OK - free space: /mnt 588446 MB (92.27% inode=100%);| /mnt=49261MB;510166;573937;0;637708
[guaoran@localhost nagios]$ 
```

最终的样子：

![1560334547875](assets/1560334547875.png)



## 配置监控CPU、内存

下载 `check_cpu.sh` 文件，并将该文件复制到  `/usr/local/nagios/libexec/` 中，[下载地址](https://exchange.nagios.org/directory/Plugins/System-Metrics/CPU-Usage-and-Load/check_cpu-2Esh/details)

下载 `check_cpu.sh` 文件，并将该文件复制到  `/usr/local/nagios/libexec/` 中，[下载地址](<https://exchange.nagios.org/directory/Plugins/System-Metrics/Memory/check_mem-2Esh/details>)

### 查看文件是否可执行

```shell
[guaoran@localhost nagios]$ sudo ./check_cpu.sh 
sudo: unable to execute ./check_cpu.sh: No such file or directory
[guaoran@localhost nagios]$ 
```

解决方案

> 尝试对文件用 vim 打开该sh文件，输入:set ff ，显示fileformat=unix，说明没有问题（如果显示fileformat=dos,则有问题，使用命令:set ff=unix 重新设置下文件格式再保存退出

再次校验

```shell
[guaoran@localhost libexec]$ ./check_cpu.sh
CPU OK : user=3% system=2% iowait=1% idle=95% | cpu_user=3%;70;90; cpu_sys=2%;70;90; cpu_iowait=1%;70;90; cpu_idle=95%;
```

### 修改配置文件

修改 nrpe 配置文件 ： `vim /usr/local/nagios/etc/nrpe.cfg`

```properties
command[check_swap]=/usr/local/nagios/libexec/check_swap -w 20% -c 10%
command[check_mem]=/usr/local/nagios/libexec/check_mem 
command[check_cpu]=/usr/local/nagios/libexec/check_cpu.sh   
```

修改服务配置文件 ： `vim /usr/local/nagios/etc/objects/services.cfg`

```properties
define service{
         use                    generic-service
         host_name              monitor_mysql_local
         service_description    check_swap
         check_command          check_nrpe!127.0.0.1!check_swap
}   
define service{
         use                    generic-service
         host_name              monitor_mysql_local
         service_description    check_mem
         check_command          check_nrpe!127.0.0.1!check_mem
}   
define service{
         use                    generic-service
         host_name              monitor_mysql_local
         service_description    check_cpu
         check_command          check_nrpe!127.0.0.1!check_cpu
}    
```

重启服务

```shell
systemctl restart nagios.service
systemctl restart nrpe.service
```



