# 目录

-   [MySQL](#mysql)
    -   [索引](#索引)
        -   [索引是什么](#索引是什么)
        -   [为什么选择的是B+](#为什么选择的是b)
            -   [二叉查找树](#二叉查找树)
            -   [平衡二叉查找树](#平衡二叉查找树)
            -   [多路平衡查找树](#多路平衡查找树)
            -   [加强版多路平衡查找树plus
                `B+`](#加强版多路平衡查找树plus-b)
        -   [B+TREE与B-TREE的区别](#btree与b-tree的区别)
            -   [MySQL中B+Tree索引体现形式](#mysql中btree索引体现形式)
            -   [Innodb VS Myisam](#innodb-vs-myisam)
            -   [myisam](#myisam)
            -   [innodb](#innodb)
        -   [myisam 和 innodb
            主键和非主键](#myisam-和-innodb-主键和非主键)
        -   [索引补充](#索引补充)
        -   [MySQL 存储引擎](#mysql-存储引擎)
            -   [CSV存储引擎](#csv存储引擎)
            -   [Archive 存储引擎](#archive-存储引擎)
            -   [Memory 存储引擎](#memory-存储引擎)
                -   [Myisam 存储引擎](#myisam-存储引擎)
                -   [Innodb 存储引擎](#innodb-存储引擎)
        -   [查询缓存](#查询缓存)
            -   [工作原理：](#工作原理)
            -   [判断标准：](#判断标准)
            -   [缓存配置参数：](#缓存配置参数)
            -   [命令可查看缓存情况](#命令可查看缓存情况)
            -   [不会缓存的情况](#不会缓存的情况)
            -   [为什么mysql默认关闭了缓存开启？？](#为什么mysql默认关闭了缓存开启)
            -   [使用业务场景](#使用业务场景)
        -   [查询优化器](#查询优化器)
            -   [最优执行计划](#最优执行计划)
            -   [执行计划](#执行计划)
                -   [执行计划-id](#执行计划-id)
                -   [执行计划-select\_type](#执行计划-select_type)
                -   [执行计划-table](#执行计划-table)
                -   [执行计划-type](#执行计划-type)
                -   [执行计划-possible\_keys/key/rows/filtered](#执行计划-possible_keyskeyrowsfiltered)
                -   [执行计划-Extra](#执行计划-extra)
    -   [慢查询](#慢查询)
    -   [事务](#事务)
    -   [锁](#锁)
        -   [MySQL Innodb 锁类型](#mysql-innodb-锁类型)
        -   [意向共享锁（IS) 和 意向排它锁（IX）
            `表锁`](#意向共享锁is-和-意向排它锁ix-表锁)
        -   [自增锁 AUTO-INC Locks](#自增锁-auto-inc-locks)
        -   [行锁的算法](#行锁的算法)
            -   [临键锁 （Next-key locks）](#临键锁-next-key-locks)
            -   [间隙锁（Gap locks）](#间隙锁gap-locks)
            -   [记录锁（Record locks）](#记录锁record-locks)
        -   [死锁](#死锁)
            -   [死锁的避免](#死锁的避免)
    -   [MVCC (多版本并发控制)](#mvcc-多版本并发控制)
    -   [Undo Log](#undo-log)
        -   [当前读和快照读](#当前读和快照读)
    -   [Redo Log](#redo-log)
        -   [Redo Log 补充知识点](#redo-log-补充知识点)
    -   [配置优化](#配置优化)
    -   [MySQL 性能优化](#mysql-性能优化)
-   [MySQL安装](#mysql安装)
    -   [安装MySQL](#安装mysql)
        -   [安装依赖](#安装依赖)
        -   [安装MySQL](#安装mysql-1)
        -   [修改密码](#修改密码)
        -   [问题](#问题)
            -   [远程连接出现Error
                1130问题](#远程连接出现error-1130问题)
    -   [安装MySQL 多实例](#安装mysql-多实例)
-   [mysql 存储表情](#mysql-存储表情)
    -   [修改 MySQL 服务器 字符编码
        utf8mb4](#修改-mysql-服务器-字符编码-utf8mb4)
    -   [修改已经存在库表编码](#修改已经存在库表编码)
-   [基于 binlog 的主从复制](#基于-binlog-的主从复制)
    -   [Master](#master)
    -   [Slave](#slave)
    -   [延迟](#延迟)
        -   [延迟是怎么产生的？](#延迟是怎么产生的)
        -   [判断延迟](#判断延迟)
        -   [怎么解决延迟问题](#怎么解决延迟问题)


# MySQL

存储引擎：MyIsam、Innodb

MyISAM是MySQL的默认数据库引擎（5.5版之前）。虽然性能极佳，但却有一个缺点：不支持事务处理（transaction）。不过，在这几年的发展下，MySQL也导入了InnoDB（另一种数据库引擎），以强化参考完整性与并发违规处理机制，后来就逐渐取代MyISAM。 

存储引擎是第三方提供的，可插拔式的插件。索引机制也便是有插拔式的存储引擎进行实现。

## 索引

### 索引是什么

> 索引是为了加速对表中数据行的检索而创建的一种分散存储的数据结构

![1553566746961](assets/1553566746961.png)

### 为什么选择的是B+

#### 二叉查找树

[Binary Search Tree](https://www.cs.usfca.edu/~galles/visualization/BST.html)

![1553566941783](assets/1553566941783.png)

#### 平衡二叉查找树

> 平衡二叉树又叫相对平衡树，高度差不会超过1

[AVL Trees](https://www.cs.usfca.edu/~galles/visualization/AVLtree.html)

![1553567372382](assets/1553567372382.png)



缺点：

*  太深：数据处的（高）深度决定着他的IO操作次数，IO操作耗时大
*  太小：每一个磁盘块（节点/页）保存的数据量太小了

#### 多路平衡查找树

[B-Tree](https://www.cs.usfca.edu/~galles/visualization/BTree.html)

![1553567527325](assets/1553567527325.png)

#### 加强版多路平衡查找树plus `B+`

[B+ Tree](https://www.cs.usfca.edu/~galles/visualization/BPlusTree.html)

![1553567574285](assets/1553567574285.png)

### B+TREE与B-TREE的区别

* B+节点关键字搜索采用闭合区间
* B+非叶节点不保存数据相关信息，只保存关键字和子节点的引用
* B+关键字对应的数据保存在叶子节点中
* B+叶子节点是顺序排列的，并且相邻节点具有顺序引用的关系

#### MySQL中B+Tree索引体现形式

#### Innodb VS Myisam

![1553564643471](assets/1553564643471.png)

- myisam:
  - .frm:表定义文件
  - .myd:数据 
  - .myi:索引 
- innodb:
  - .frm:表定义文件
  - .ibd:数据和索引

| Innodb                                                       | Myisam                                               |
| ------------------------------------------------------------ | ---------------------------------------------------- |
| .IBD文件                                                     | .MYI 和 .MYD 文件                                    |
| 辅助索引中存放的是主键索引，流程是根据辅助索引查询到主键索引，在主键索引中查询到对应的数据。 | 主键索引和辅助索引是通过在叶子节点存放数据的引用地址 |

#### myisam

![1553564577927](assets/1553564577927.png)

![1553564591372](assets/1553564591372.png)

#### innodb

![1553564615595](assets/1553564615595.png)

![1553564626507](assets/1553564626507.png)



### myisam 和 innodb 主键和非主键

myisam 的主键索引和辅助索引

> 是通过在叶子节点存放数据的引用地址

innodb 的主键索引和辅助索引，

> 辅助索引中，存放的是主键索引，流程是根据辅助索引查询到主键索引，在在主键索引中查询到对应的数据。
>
> 为什么要多次一举呢？因为在innodb中，认为主键索引是最经常、频繁用到的。而且当数据进行迁移的时候，只要保证主键不变，不需要在重新维护辅助索引。



### 索引补充

>离散性：越高，选择性越好，越适合建在第一位索引
>
>![1552298071343](assets/1552298071343.png)

> 联合索引列选择原则
>
> * 经常用的列优先 【最左匹配原则】
> * 选择性（离散度）高的列优先【离散度高原则】
> * 宽度小的列优先【最少空间原则】



### MySQL 存储引擎

#### CSV存储引擎

> 数据存储以CSV文件

特点：

* 不能定义索引，列定义必须为 NOT NULL、不能设置主键和自增	
  * 不适合大表或数据的在线处理
* CSV 数据的存储用`,`隔开，可直接编译CSV文件进行数据的修改
  * 数据安全性低
  * 编辑之后要生效需要执行  `flush table tablename;` 命令

应用场景：

* 数据的快速导入
* **表格直接转换成CSV**

#### Archive 存储引擎

> 压缩协议进行数据的存储,数据存储为ARZ文件格式

特点：

* 只支持 insert 和 select 两种操作
* 只允许自增ID列建立索引
* **行级锁**
* 不支持事务
* **数据占用磁盘少**（同innodb 比较)

应用场景：

* 日志系统
* 大量的设备数据采集

#### Memory 存储引擎

> 数据都是存储在内存中，IO效率要比其他引擎高很多服务重启数据丢失，内存数据表默认只有16M

特点：

* 支持hash 索引，B tree 索引，默认hash索引
* 字段长度是固定长度 varchar(32)=char(32)
* 不支持大数据存储类型字段 ，如：blog 、text
* 表级锁

应用场景：

* 等值查找热度较高数据
* 查询结果内存中的计算，大多数都是采用这种存储引擎作为临时表，用来存储计算的数据

##### Myisam 存储引擎

> MySQL 5.5版本之前的默认存储引擎，较多的系统表也还是使用这个存储引擎，系统临时表也会用到Myisam存储引擎,(如果数据较少的话，小于16M，默认先用Memory 引擎)

特点：

* `select count(*) from table` 无需进行数据的扫描
* 数据（MYD）和索引（MYI）分开存储
* 表级锁
* 不支持事务
* 通过先禁用索引、载入数据，重新启用索引，来实现高效的导入数据
* 

##### Innodb 存储引擎

> MySQL 5.5 及以后的默认存储引擎

特点：

* **支持事务**
* **行级锁**
* 使用主键索引（聚集索引）存储数据
* 支持外键关系保证数据完整性



### 查询缓存

#### 工作原理：

* 缓存SELECT操作的结果集和SQL语句；
* 新的SELECT语句，先去查询缓存，判断是否存在可用的记录集

#### 判断标准：

* 与缓存的SQL语句，是否完全一样，区分大小写
* 简单认为存储了一个key-value结构，key为sql，value为sql查询结果集

#### 缓存配置参数：

```mysql
show variables like 'query_cache%';
```

**query_cache_type:**

* 0 -– 不启用查询缓存，默认值；
* 1 -– 启用查询缓存，
  * 只要符合查询缓存的要求，客户端的查询语句和记录集都可以缓存起来，供其他客户端使用，
  * 加上 SQL_NO_CACHE将不缓存
* 2 -– 启用查询缓存，
  * 只要查询语句中添加了参数：SQL_CACHE，且符合查询缓存的要求，客户端的查询语句和记录集，则可以缓存起来，供其他客户端使用

**query_cache_size:**

* 允许设置query_cache_size的值最小为40K，默认1M，
* 推荐设置 为：64M/128M；

**query_cache_limit:**

* 限制查询缓存区最大能缓存的查询记录集，默认设置为1M

#### 命令可查看缓存情况

```mysql
show status like 'Qcache%';
```

#### 不会缓存的情况

* 当查询语句中有一些不确定的数据时，则不会被缓存。如包含函数NOW()，CURRENT_DATE()等类似的函数，或者用户自定义的函数，存储函数，用户变量等都不会被缓存
* 当查询的结果大于query_cache_limit设置的值时，结果不会被缓存
* 对**于InnoDB引擎来**说，当一个语句在事务中修改了某个表，那么在这个事务提交之前，所有与这个表相关的查询都无法被缓存。因此长时间执行事务，会大大降低缓存命中率
* 查询的表是系统表（select * from mysql.user;)
* 查询语句不涉及到表（select 1;）

#### 为什么mysql默认关闭了缓存开启？？

* 在查询之前必须先检查是否命中缓存,浪费计算资源
* 如果这个查询可以被缓存，那么执行完成后，MySQL发现查询缓存中没有这个查询，则会将结果存入查询缓存，这会带来额外的系统消耗
*  针对表进行写入或更新数据时，将对应表的所有缓存都设置失效。
*  如果查询缓存很大或者碎片很多时，这个操作可能带来很大的系统消耗

#### 使用业务场景

> 以读为主的业务，数据生成之后就不常改变的业务.
>
> 比如门户类、新闻类、报表类、论坛类等

### 查询优化器

#### 最优执行计划

> Mysql的查询优化器是基于成本计算的原则。他会尝试各种执行计划。数据抽样的方式进行试验

* 使用等价变化规则：基于联合索引，调整条件位置等
  * `5 = 5 and a > 5` 改写成 `a > 5`
  * `a < b and a = 5` 改写成 `b > 5 and a = 5`
* 优化count 、min、max等函数
  * min函数只需找索引最左边
  * max函数只需找索引最右边
  * myisam引擎count(*)
* 覆盖索引扫描
* 子查询优化
* 提前终止查询
  * 用了limit关键字或者使用不存在的条件
  * `select * from user where id=-1;`
* IN的优化
  * 先进性排序，再采用二分查找的方式
  * 这里表述的是 in (1,2,3,4)  ,而不是in (子查询)
  * in 中包含子查询的 写法，不建议使用，会导致驱动表全表扫描

#### 执行计划

```mysql
mysql> explain select * from person where id in(select id from person where name='guaoran') \G
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: person
   partitions: NULL
         type: ref
possible_keys: PRIMARY,idx_name
          key: idx_name
      key_len: 302
          ref: const
         rows: 1
     filtered: 100.00
        Extra: Using index
*************************** 2. row ***************************
           id: 1
  select_type: SIMPLE
        table: person
   partitions: NULL
         type: eq_ref
possible_keys: PRIMARY
          key: PRIMARY
      key_len: 4
          ref: my_demo.person.id
         rows: 1
     filtered: 100.00
        Extra: NULL
2 rows in set, 1 warning (0.00 sec)
```

```mysql
mysql> explain select * from my_innodb where id in(select phonenum from my_archive where age=20) \G
*************************** 1. row ***************************
           id: 1
  select_type: SIMPLE
        table: <subquery2>
   partitions: NULL
         type: ALL
possible_keys: NULL
          key: NULL
      key_len: NULL
          ref: NULL
         rows: NULL
     filtered: 100.00
        Extra: Using where
*************************** 2. row ***************************
           id: 1
  select_type: SIMPLE
        table: my_innodb
   partitions: NULL
         type: eq_ref
possible_keys: PRIMARY
          key: PRIMARY
      key_len: 8
          ref: <subquery2>.phonenum
         rows: 1
     filtered: 100.00
        Extra: Using where
*************************** 3. row ***************************
           id: 2
  select_type: MATERIALIZED
        table: my_archive
   partitions: NULL
         type: ALL
possible_keys: NULL
          key: NULL
      key_len: NULL
          ref: NULL
         rows: 999999
     filtered: 10.00
        Extra: Using where
3 rows in set, 1 warning (0.00 sec)
```

##### 执行计划-id

> select查询的序列号，标识执行的顺序

1. id相同，执行顺序由上至下
2. id不同，如果是子查询，id的序号会递增，id值越大优先级越高，越先被执行
3. id相同又不同即两种情况同时存在，id如果相同，可以认为是一组，从上往下顺序执行；在所有组中，id值越大，优先级越高，越先执行

##### 执行计划-select_type

> 查询的类型，主要是用于区分普通查询、联合查询、子查询等

1. `SIMPLE`：简单的select查询，查询中不包含子查询或者union
2. `PRIMARY`：查询中包含子部分，最外层查询则被标记为primary
3. SUBQUERY/MATERIALIZED：
   1. `SUBQUERY`表示在select 或 where列表中包含了子查询
   2. `MATERIALIZED`表示where 后面in条件的子查询
4. `UNION`：若第二个select出现在union之后，则被标记为union；
5. `UNION RESULT`：从union表获取结果的select

##### 执行计划-table

> 查询涉及到的表

1. 直接显示表名或者表的别名
   1. `<unionM,N>` 由ID为M,N 查询union产生的结果
   2. `<subqueryN>` 由ID为N查询生产的结果

##### 执行计划-type

> 访问类型，sql查询优化中一个很重要的指标，结果值从好到坏依次是：
>
> **system** > **const** > **eq_ref**  > **ref** > **range** > **index** > **ALL**

1. **system**：表只有一行记录（等于系统表），const类型的特例，基本不会出现，可以忽略不计

2. **const**：表示通过索引一次就找到了，const用于比较primary key 或者 unique索引

   ```mysql
      explain select * from my_innodb where id =1 \G
   ```

3. **eq_ref**：唯一索引扫描，对于每个索引键，表中只有一条记录与之匹配。常见于主键 或 唯一索引扫描
   
   ```mysql
       explain select * from users u,user_address a where a.userId = u.id\G
   ```
   
4. **ref**：非唯一性索引扫描，返回匹配某个单独值的所有行，本质是也是一种索引访问
   
   ```mysql
       explain select * from users where age=5\G
   ```
   
5. **range**：只检索给定范围的行，使用一个索引来选择行

6. **index**：Full Index Scan，索引全表扫描，把索引从头到尾扫一遍

   1. ```mysql
       explain select depart from person \G
       ```

7. **ALL**：Full Table Scan，遍历全表以找到匹配的行

##### 执行计划-possible_keys/key/rows/filtered

1. **`possible_keys`** : 查询过程中有可能用到的索引
2. **`key`** : 实际使用的索引，如果为NULL，则没有使用索引
3. **`rows`** : 根据表统计信息或者索引选用情况，大致估算出找到所需的记录所需要读取的行数
4. **`filtered`** : 它指返回结果的行占需要读到的行(rows列的值)的百分比。(有效率命中率)
   1. 表示返回结果的行数占需读取行数的百分比，filtered的值越大越好

##### 执行计划-Extra

> 十分重要的额外信息

1. Using filesort ：
   mysql对数据使用一个外部的文件内容进行了排序，而不是按照表内的索引进行排序读取

   ```mysql
   mysql> explain select * from cm_customer_person order by name desc \G
   *************************** 1. row ***************************
              id: 1
     select_type: SIMPLE
           table: cm_customer_person
      partitions: NULL
            type: ALL
   possible_keys: NULL
             key: NULL
         key_len: NULL
             ref: NULL
            rows: 37009
        filtered: 100.00
           Extra: Using filesort
   1 row in set, 1 warning (0.00 sec)
   ```

2. Using temporary：

   使用临时表保存中间结果，也就是说mysql在对查询结果排序时使用了临时表，常见于order by 或 group by

3. Using index：

   表示相应的select操作中使用了覆盖索引（Covering Index），避免了访问表的数据行，效率高

4. Using where ：

   表示使用了where过滤条件

5. select tables optimized away：

   基于索引优化MIN/MAX操作或者MyISAM存储引擎优化COUNT(*)操作，不必等到执行阶段在进行计算，查询执行计划生成的阶段即可完成优化

6.  Using index condition

    Using index condition 会先条件过滤索引，过滤完索引后找到所有符合索引条件的数据行，随后用 WHERE 子句中的其他条件去过滤这些数据行；

## 慢查询

```shell
## 查看慢查询的配置
show variables like 'slow_query_log';
## 开启慢查询日志
set global slow_query_log=on;
## 自定义设置慢查询日志文件
set global slow_query_log_file = '/guaoran/mysql/data/slow_query_log_demo.log';
## sql 查询要不要记录慢查询
set global log_queries_not_using_indexes=on;
## 单位 秒 ，当查询时间超过0.1s就记录慢查询日志
set global long_query_time = 0.1;
## 慢查询信息
show global status like '%slow%';
## 使用自带工具分析慢查询日志
mysqldumpslow -s c  /guaoran/mysql/data/slow_query_log_demo.log 
# perl mysqldumpslow -s c  /usr/local/mysql/data/localhost-slow.log 

## 查看所有操作的记录
show variables like '%general%';
## 打开、关闭记录所有的日志操作记录
set global general_log=on;
set global general_log=off;


## 查看慢查询日志输出的格式 该输出包含 慢日志查询和general_log 的输出
show variables like '%log_output%';
## 设置慢查询输出的 格式，table 的话，可以在mysql.slow_log 和 mysql.general_log 中查看
set global log_output='FILE,TABLE';

```



## 事务

事务的ACID特性

- 原子性
- 一致性
- 持久性
- 隔离性
  - 读未提交：脏读、不可重复读、幻读
  - 读已提交：不可重复读，幻读
  - 可重复读：幻读
  - 串行化：最高的隔离级别，解决所有事务问题



可重复读 REPEATABLE READ

> 当存在两个事务
>
> T1 ：对空表进行查询
>
> ```mysql
> BEGIN;
> SELECT * FROM test_tx;
> -- ROLLBACK;
> ```
>
> T2：对空表进行插入
>
> ```mysql
> BEGIN;
> INSERT INTO test_tx VALUES(1,"1");
> 
> -- ROLLBACK;
> -- COMMIT;
> ```
>
> 此时默认都未 COMMIT 或 ROLLBACK  。 当 T2 事务进行COMMIT 操作后， 在T1的事务中是**无法**获得最新的提交记录。之后再次开启一个新的事务才能获得最新的提交记录。

读已提交 READ COMMITTED

> 当存在上面可重复读相同操作的两个事务时，当 T2 事务进行COMMIT 操作后， 在T1的事务中是**可以**获得最新的提交记录。



![1551100192686](assets/1551100192686.png)

可重复读在MySQL中是默认的隔离级别，MySQL的InnoDB 在 可重复读的情况下是如何解决幻读的问题呢?

> 通过 锁 + MVCC 

## 锁

### MySQL Innodb 锁类型

- 共享锁（行锁），又称读锁，S 锁

  > 多个事务都可以访问到数据，但是只能读不能修改。

  加锁释放锁方式：

  ```mysql
  select * from users WHERE id=1 LOCK IN SHARE MODE; 
  commit ;
  -- rollback;
  ```

- 排他锁（行锁），又称写锁，X锁

  > 排他锁不能与其他锁并存，如果一个事务获得了排它锁，其他的事务就不能再获得其他锁。
  >
  > 只有该获取了排他锁的事务是可以对 数据行进行读取和修改，（其他事务要读取数据可来自于快照）

  加锁释锁方式： 

  ```mysql
  delete / update / insert 默认加上X锁 
  SELECT * FROM table_name WHERE ... FOR UPDATE ;
  commit;
  rollback;
  ```

innodb 的行锁到底锁了什么？

> InnoDB的行锁是通过给索引上的索引项加锁来实现的
>
> 只有通过索引条件进行数据检索，InnoDB才使用行级锁，否则，InnoDB 将使用表锁（锁住索引的所有记录）

### 意向共享锁（IS) 和 意向排它锁（IX） `表锁`

**意向共享锁(IS)** 表示事务准备给数据行加入共享锁，即一个数据行加共享锁前必须先取得该表的IS锁， 意向共享锁之间是可以相互兼容的

**意向排它锁(IX)** 表示事务准备给数据行加入排他锁，即一个数据行加排他锁前必须先取得该表的IX锁， 意向排它锁之间是可以相互兼容的

**意向锁(IS、IX)是InnoDB数据操作之前自动加的，不需要用户干预**

**意义**：
当事务想去进行锁表时，可以先判断意向锁是否存在，存在时则可快速返回该表不能 启用表锁

> 可以理解为是一个标志位， innodb 在去加共享锁或排它锁的时候，会先去判断，是否存在了某个锁，存在时则不需要再一次次的去判断，直接就可以返回。



### 自增锁 AUTO-INC Locks

> 针对自增列自增长的一个特殊的表级别锁
>
> ```mysql
> show variables like  'innodb_autoinc_lock_mode';
> ```
>
> 默认取值1，代表连续，事务未提交ID永久丢失

### 行锁的算法

#### 临键锁 （Next-key locks）

**Next-key locks**： 锁住记录+区间（左开右闭） 当sql执行按照索引进行数据的检索时,查询条件为范围查找（between and、<、>等）并有数 据命中则此时SQL语句加上的锁为Next-key locks，锁住索引的记录+区间（左开右闭）

![1551102500869](assets/1551102500869.png)

**InnoDB 选择 临键锁作为行锁的默认算法，就是为了在可重复读模式下，解决幻读的问题**

#### 间隙锁（Gap locks）

**Gap locks**： 锁住数据不存在的区间（左开右开） 当sql执行按照索引进行数据的检索时，查询条件的数据不存在，这时SQL语句加上的锁即为 Gap locks，锁住索引不存在的区间（左开右开）

![1551103786631](assets/1551103786631.png)

#### 记录锁（Record locks）

**Record locks**： 锁住具体的索引项 当sql执行按照唯一性（Primary key、Unique key）索引进行数据的检索时，查询条件等值匹 配且查询的数据是存在，这时SQL语句加上的锁即为记录锁Record locks，锁住具体的索引项

![1551103853462](assets/1551103853462.png)

### 死锁

多个并发事务（2个或者以上）；
每个事务都持有锁（或者是已经在等待锁）;
每个事务都需要再继续持有锁；
事务之间产生加锁的循环等待，形成死锁

> 例如：
>
> ```msyql
> -- T1
> begin;
> update user set age = 20 where id=1;
> update user_address set address='xx' where id=25;
> 
> -- T2 
> begin
> update user_address set address='xxxxx' where id=25;
> update user set age = 22 where id=1;
> ```
>
> 在事务T1中执行完user 表的操作时，而事务T2 也正好执行完user_address 的操作，此时T1 在操作时需要等待T2进行释放 user_address  的排他锁，而T2 也需要等待 T1 进行释放user 的排他锁，由于没有进行commit 或者rollback，则T1 和 T2 都需要等待对方进行释放锁，便形成了死锁。

#### 死锁的避免

1. 类似的业务逻辑以固定的顺序访问表和行。
2. 大事务拆小。大事务更倾向于死锁，如果业务允许，将大事务拆小。
3. 在同一个事务中，尽可能做到一次锁定所需要的所有资源，减少死锁概 率。 
4. 降低隔离级别，如果业务允许，将隔离级别调低也是较好的选择 
5. 为表添加合理的索引。可以看到如果不走索引将会为表的每一行记录添 加上锁（或者说是表锁）



## MVCC (多版本并发控制)

> 并发访问(读或写)数据库时，对正在事务内处理的数据做 多版本的管理。以达到用来避免写操作的堵塞，从而引发读操 作的并发问题。

mysql 中MVCC 逻辑流程-查询

![1551108209890](assets/1551108209890.png)





查询规则：

1. 查找数据行版本早于当前事务版本的数据行
   1. 就是数据行的系统版本号小于或等于当前的事务版本号，这样可以确保事务读取的行，要么是在事务开始之前已经存在的，要么是该事务自身插入或者修改过的
2. 查找删除版本号要么为null，要么大于当前事务版本号的记录
   1. 确保取出来的行记录是在事务开启之前没有被删除的。

案例：
![1551108491384](assets/1551108491384.png)

案例1：

![1551108516881](assets/1551108516881.png)

案例2：

![1551108542231](assets/1551108542231.png)

> 关于案例一好像符合MVCC的规则流程，但是案例二好像也符合MVCC的规则，不过这样的话就出现脏读的问题了， 具体是因为什么呢？ 因为这跟Undo log 有关系。关于案例二的实际结果并不是按照MVCC的规则进行的，而是读了快照。

## Undo Log

Undo Log 是什么： 

> undo意为取消，以撤销操作为目的，返回指定某个状态的操作 undo log指事务开始之前，**在操作任何数据之前,首先将需操作的数据备份到一个地方 (Undo Log)**

***UndoLog是为了实现事务的原子性而出现的产物*** 
Undo Log实现事务原子性： 

> 事务处理过程中如果出现了错误或者用户执行了 ROLLBACK语句,Mysql可以利用Undo Log中的备份 将数据恢复到事务开始之前的状态

***UndoLog在Mysql innodb存储引擎中用来实现多版本并发控制*** 
Undo log实现多版本并发控制： 

> 事务未提交之前，Undo保存了未提交之前的版本数据，Undo 中的数据可作为数据旧版本快照供 其他并发事务进行快照读

![1551108835203](assets/1551108835203.png)

### 当前读和快照读

快照读：

> SQL读取的数据是快照版本，也就是历史版本，普通的SELECT就是快照读 
>
> innodb快照读，数据的读取将由 cache(原本数据) + undo(事务修改过的数据) 两部分组成

当前读：

> SQL读取的数据是最新版本。通过锁机制来保证读取的数据无法通过其他事务进行修改
>
> UPDATE、DELETE、INSERT、SELECT …  LOCK IN SHARE MODE、SELECT … FOR UPDATE都是当前读

## Redo Log

Redo Log 是什么：

> Redo，顾名思义就是重做。以恢复操作为目的，重现操作； Redo log指事务中操作的任何数据,将最新的数据备份到一个地方 (Redo Log)

Redo log的持久： 

> 不是随着事务的提交才写入的，而是在事务的执行过程中，便开始写入redo 中。具体 的落盘策略可以进行配置 。**RedoLog是为了实现事务的持久性而出现的产物**

Redo Log实现事务持久性： 

> 防止在发生故障的时间点，尚有脏页未写入磁盘，在重启mysql服务的时候，根据redo log进行重做，从而达到事务的未入磁盘数据进行持久化这一特性。

![1551109029776](assets/1551109029776.png)

### Redo Log 补充知识点

> 指定Redo log 记录在{datadir}/ib_logfile1&ib_logfile2   可通过innodb_log_group_home_dir 配置指定 目录存储
>
> 一旦事务成功提交且数据持久化落盘之后，此时Redo log中的对应事务数据记录就失去了意义，所 以Redo log的写入是日志文件循环写入的 
>
> 指定Redo log日志文件组中的数量 `innodb_log_files_in_group` 默认为2 
>
> 指定Redo log每一个日志文件最大存储量`innodb_log_file_size`  默认48M 
>
> 指定Redo log在cache/buffer中的buffer池大小`innodb_log_buffer_size` 默认16M

Redo buffer 持久化Redo log的策略(`Innodb_flush_log_at_trx_commit`)： 

- 取值 0  每秒提交 Redo buffer  -->  Redo log  OS cache -->flush cache to disk[可能丢失一秒内 的事务数据]
- 取值 1  默认值，每次事务提交执行Redo buffer  -->  Redo log  OS cache -->flush cache to disk [最安全，性能最差的方式] 
- 取值 2  每次事务提交执行Redo buffer  -->  Redo log  OS cache  再每一秒执行 ->flush cache to disk操作



## 配置优化

1. 寻找配置文件的位置和加载顺序 `mysql --help  `

   ```shell
   Default options are read from the following files in the given order:
   	/etc/my.cnf /etc/mysql/my.cnf /usr/etc/my.cnf ~/.my.cnf
   mysql --help | grep -A 1 'Default options are read from the following files in the given order'
   ```

2. 全局配置文件

   1. max_connections   ：最大连接数

   2. ulimit -a  ： 系统句柄数

      `/etc/security/limits.conf` 

   3. mysql句柄数配置 

      `/usr/lib/systemd/system/mysqld.service` 

3. 内存参数配置

   > 每一个**connection**内存参数配置：
   >        **sort_buffer_size   connection**排序缓冲区大小
   > 		建议**256K(默认值)-> 2M**之内
   > 		当查询语句中有需要文件排序功能时，马上为connection分配配置的内存大小
   >
   > ​	**join_buffer_size   connection**关联查询缓冲区大小
   > ​		建议**256K(默认值)-> 1M**之内
   > ​		当查询语句中有关联查询时，马上分配配置大小的内存用这个关联查询，所以有可能在一个查询语句中会分配很多个关联查询缓冲区
   >
   > 
   >
   > 上述配置4000连接占用内存： 4000*(0.256M+0.256M) = 2G

   >**`wait_timeout`** : 服务器关闭非交互连接之前等待活动的秒数
   >
   >**`innodb_open_files`** : 限制Innodb能打开的表的个数
   >
   >**`innodb_write_io_threads/innodb_read_io_threads`**  :  innodb 使用后台线程处理innodb缓冲区数据页上的读写 I/O(输入输出)请求
   >
   >**`innodb_lock_wait_timeout`** :  InnoDB事务在被回滚之前可以等待一个锁定的超时秒数

   

## MySQL 性能优化

1.尽量采用innodb的存储引擎，它包含事务，热备份，行锁，崩溃恢复速度快，而且损耗低；而myisam不包含事务，不支持外键，锁级别是表，支持全文索引，压缩。

2.建立主键时，尽量采用数字型为主键，因为字符型的标识会比数字型的标识慢，且在myisam中会比innodb中慢很多。随机、MD5，hash，uuid时会在插入和查询时会更加慢。如果采用UUID的话，应该去除“-”符号，或者用unhex()函数转换成UUID值为16字节的数字。并且存储到binary（16）列中，检索时可以通过hex（）函数来格式化为16进制格式。

3.字段存在NULL，因为可为NULL的列使的索引、索引统计和值都比较复杂，为NULL时的列会使用更多的存储空间，当可为NULL的列被索引时，每个索引记录需要一个额外的字节，在myisam里甚至还可能导致固定大小的索引变成可变大小的索引。

4.索引：

```
（InnoDb ：B+Tree）创建一个多列索引idx_many(firstname,lastname,idcard)
    全值匹配：查询firstname='xx' and lastname='xxxxx' and idcard='xxx'
    匹配最左前缀：查询firstname='xxx'
    匹配列前缀：查询firstname='x%'
    匹配范围值：查询 BETWEEN firstname='xxxx' and firstname='yyy'
    精确匹配某一列，并范围匹配另外一列：查询firstname='xxx' and BETWEEN idcard ='xxx' and idcard ='xxx'
    如果直接查询lastname或者IDcard将不会使用索引。
（Memory：hash）精确匹配索引
    哈希索引只包含哈希值和行指针，而不存储字段值，所以不能使用索引中的值来避免读取行
    哈希索引数据并不是按照索引值顺序存储的，所以，无法用于排序
    哈希索引不支持部分索引列匹配查找。因为是哈希索引时通过索引列的全部内容来计算hash值的
    只支持等值比较：包括=，IN（）,<=>
    会存在哈希冲突
```

锁：

```
InnoDb 使用行锁， 当使用非索引的字段时是表锁，
myisam 使用表锁，
行锁：粒度小，容易发生死锁（当多个事务之间存在互相等待对方释放锁的时候）
表锁：实现逻辑简单，获取、释放快，避免死锁， 粒度太大，并发不够高
```



# MySQL安装

[安装包地址](https://dev.mysql.com/downloads/mysql/)

[安装包历史地址](https://downloads.mysql.com/archives/community/)

## 安装MySQL

### 安装依赖

```shell
 yum install -y cmake make gcc gcc-c++ libaio ncurses ncurses-devel
```

### 安装MySQL

**下载解压**

```shell
cd /guaoran/installPack
wget https://cdn.mysql.com/archives/mysql-5.7/mysql-5.7.24-linux-glibc2.12-x86_64.tar.gz
tar -zxvf mysql-5.7.24-linux-glibc2.12-x86_64.tar.gz 
```

**将解压的复制到   /usr/local/mysql 目录** 

```shell
mkdir -pv /usr/local/mysql
cp -r mysql-5.7.24-linux-glibc2.12-x86_64 /usr/local/mysql/
```

**添加系统mysql组和mysql用户**  

```shell
groupadd mysql
useradd -r -g mysql mysql
```

**安装数据库** 

切换到 mysql 目录，并修改该目录拥有者为MySQL用户

```shell
cd /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/
chown -R mysql:mysql ./
mkdir -pv /guaoran/mysql/data
bin/mysqld --initialize --user=mysql --basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64 --datadir=/guaoran/mysql/data
mkdir -pv /guaoran/mysql/data/log
```

安装后会生成临时密码，修改密码时会用到,最后面是密码

`[Note] A temporary password is generated for root@localhost: ejStoWO/i7Fs`

**配置my.cnf**  

`vi /etc/my.cnf` 设置数据库，表，字段默认是 utf8mb4 ，支持存储表情

```properties
[client]
default-character-set=utf8mb4

[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
init_connect='SET NAMES utf8mb4'

basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64
datadir=/guaoran/mysql/data
socket=/tmp/mysql.sock
#不区分大小写 
lower_case_table_names = 1

sql_mode = "STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"

###慢查询
#slow_query_log=on
#long_query_time=1
#log_output='TABLE'

## 查询缓存开启：
### 0：不启用查询缓存，默认值
### 1：启用查询缓存，加上 SQL_NO_CACHE将不缓存
### 2：启用查询缓存，加上 SQL_CACHE 后符合条件将缓存
#query_cache_type=1

## 最大连接数
max_connections=2000

## 默认的隔离级别
#transaction_isolation = REPEATABLE-READ

max_allowed_packet=128M

[mysqld_safe]
log-error=/var/log/mariadb/mariadb.log
pid-file=/var/run/mariadb/mariadb.pid

[mysql]
default-character-set=utf8mb4
```

**添加开机启动** 

```shell
cp /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/support-files/mysql.server  /etc/init.d/mysqld
vi /etc/init.d/mysqld
```

查看 序号 `:set number` 

在46行-47行 修改

```shell
basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64
datadir=/guaoran/mysql/data
```

**启动MySQL ，加入开机启动**

```shell
service mysqld start 
chkconfig --add mysqld 
```

**创建引用链接**

```shell
ln -s /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/bin/mysql /usr/bin/
ln -s /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/bin/mysqldump /usr/bin/
ln -s /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/bin/mysqldumpslow /usr/bin/
```

查看引用

```shell
cd /usr/bin
find mysql*  -exec ls -l {} \;
```

### 修改密码

临时密码是在安装数据库时生成的密码

使用临时密码进行登录：

```shell
mysql -u root -p
```

登录成功修改新密码为 *root* ：

```mysql
ALTER USER 'root'@'localhost' IDENTIFIED BY 'root';
```

### 问题

#### 远程连接出现Error 1130问题

本地登录MySQL `mysql -uroot -proot`

```mysql
use mysql;
select host, user from user where user = 'root';
```

发现 `host = localhost` ,所以要改成  `%` 

```mysql
use mysql;
update user set host = '%' where user = 'root';
flush privileges;
```


## 安装MySQL 多实例

[centos7 安装mysql 5.7多实例](https://blog.csdn.net/keketrtr/article/details/80535961)
[单台linux部署多个mysql实例实战](https://www.jianshu.com/p/c994d13753ab)

```shell
mkdir /guaoran/mysql/data/mysql1
mkdir /guaoran/mysql/data/mysql2
mkdir -pv /guaoran/mysql/data/log/mysql1
mkdir -pv /guaoran/mysql/data/log/mysql2

echo 'export PATH=$PATH:/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/bin' >> /etc/profile
source /etc/profile

```

`vi /etc/my.cnf`

```properties
[client]
default-character-set=utf8mb4

#######

[mysqld_multi]
mysqld = /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/bin/mysqld_safe
mysqladmin = /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/bin/mysqladmin
log = /guaoran/mysql/data/log/mysqld_multi.log

########

[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
init_connect='SET NAMES utf8mb4'

basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64
datadir=/guaoran/mysql/data/mysql1
socket=/tmp/mysql.sock
#不区分大小写 
lower_case_table_names = 1

sql_mode = "STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION"

###慢查询
#slow_query_log=on
#long_query_time=1
#log_output='TABLE'

## 查询缓存开启：
### 0：不启用查询缓存，默认值
### 1：启用查询缓存，加上 SQL_NO_CACHE将不缓存
### 2：启用查询缓存，加上 SQL_CACHE 后符合条件将缓存
#query_cache_type=1

## 最大连接数
max_connections=2000

## 默认的隔离级别
#transaction_isolation = REPEATABLE-READ

max_allowed_packet=128M

=======================
[mysqld1]
mysqld=mysqld
mysqladmin=mysqladmin
datadir=/guaoran/mysql/data/mysql1
port=3306
server_id=mysql1
socket=/tmp/mysql.sock
log-error = /guaoran/mysql/data/log/mysql1/error.log

[mysqld2]
mysqld=mysqld
mysqladmin=mysqladmin
datadir=/guaoran/mysql/data/mysql2
port=3307
server_id=mysql2
socket=/tmp/mysql2.sock
log-error = /guaoran/mysql/data/log/mysql2/error.log

=====================


[mysql]
default-character-set=utf8mb4
```





```shell
cd /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64

bin/mysqld --initialize --user=mysql --basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64 --datadir=/guaoran/mysql/data
{g?)wSiOAu7el}

bin/mysqld --initialize --user=mysql --basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64 --datadir=/guaoran/mysql/data2
{HXwfq)vka7%w}

bin/mysqld --initialize --user=mysql --basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64 --datadir=/guaoran/mysql/data3
{lPe.cstql3Ax}

bin/mysql_ssl_rsa_setup  --basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/  --datadir=/guaoran/mysql/data  --defaults-file=/etc/my.cnf

bin/mysql_ssl_rsa_setup  --basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/  --datadir=/guaoran/mysql/data2  --defaults-file=/etc/my.cnf

bin/mysql_ssl_rsa_setup  --basedir=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/  --datadir=/guaoran/mysql/data3  --defaults-file=/etc/my.cnf


cp /usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64/support-files/mysqld_multi.server /etc/init.d/mysqld_multi

chkconfig --add mysqld_multi



```

在文件最后面添加内容：sudo vim /etc/profile

```properties
## SETUP MYSQL’S PATH
export MYSQL_HOME=/usr/local/mysql/mysql-5.7.24-linux-glibc2.12-x86_64
export PATH=${MYSQL_HOME}/bin:$PATH
```

使修改的配置文件生效：`source /etc/profile`

查看多个实例状态：`mysqld_multi report`

启动各个实例 ： `sudo mysqld_multi start`

查看实例的监听端口：`sudo ss -tulpn|grep mysqld`

连接实例: `sudo mysql -S /tmp/mysql2.sock -p`



# mysql  存储表情

## 修改 MySQL 服务器 字符编码 utf8mb4

查看字符集情况

```mysql
SHOW VARIABLES WHERE Variable_name LIKE 'character\_set\_%' OR Variable_name LIKE 'collation%';
```

修改字符集 `vim /etc/my.cnf `

``` properties
[client]
default-character-set=utf8mb4

[mysqld]
character-set-client-handshake = FALSE
character-set-server = utf8mb4
collation-server = utf8mb4_unicode_ci
init_connect='SET NAMES utf8mb4'

[mysql]
default-character-set=utf8mb4
```

重启MySQL 

```shell
service mysqld restart
```

再次查看字符集，已经改变

![1547778241574](assets/1547778241574.png)

## 修改已经存在库表编码

1. 修改库的编码为 `utf8mb4`

2. 修改表的编码为 `utf8mb4` 

   ```mysql
   ALTER TABLE table_name CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci; 
   ```

3. 修改表字段编码为 `utf8mb4`

   ```mysql
   ALTER TABLE 表名 CHANGE 字段名 字段名 该字段原来的数据类型 CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci; 
   ```



# 基于 binlog 的主从复制

## Master

1. 连接MySQL 并创建主从复制的用户

   ```mysql
   create user masterSync;
   ```

2. 给新建用户赋权

   ```mysql
   GRANT REPLICATION SLAVE,FILE ON *.* TO 'masterSync'@'%' IDENTIFIED BY 'masterSync';
   ```

3. 指定服务ID，开启binlog日志记录，在 /etc/my.cnf 中加入如下配置

   ```properties
   [mysqld]
   # 通常设置为ip
   server-id=135
   # 记录到 mysql-bin 文件中
   log-bin=mysql-bin
   # 同步的指定db 与binlog_ignore_db（指定不同步的db） 互斥，
   binlog-do-db=demo
   binlog-do-db=db_store
   ```

    重启MySQL

   ```shell
   service mysqld restart
   ```

4. 通过命令查看Master db状态.

    ```mysql
    SHOW MASTER STATUS;
    ```

## Slave

1. 指定服务器ID，指定同步的binlog存储位置，在 my.cnf中加入如下配置

   ```properties
   server-id=134
   relay-log=slave-relay-bin
   relay-log-index=slave-relay-bin.index
   read_only=1
   # 复制的db
   replicate_do_db=demo
   replicate_do_db=db_store
   ```

2.  重启MySQL

   ```shell
   service mysqld restart
   ```

3. 接入slave的mysql服务，开始复制

   ```mysql
   change master to master_host='192.168.45.135',master_port=3306,master_user='masterSync',master_password='masterSync',master_log_file='mysql-bin.000006',master_log_pos=462; 
   start slave;
   ```

4. 查看slave服务器状态

   ```mysql 
   show slave status\G 
   ```

## 延迟

### 延迟是怎么产生的？

* 当master  tps高于slave的sql线程所能承受的范围
* 网络原因
* 磁盘读写耗时

### 判断延迟

* show  slave status \G 
  * sends_behind_master  0 为0时代表没有延迟
* mk-heartbeat  timestamp 进行时间戳的判断

### 怎么解决延迟问题

* 配置更高的硬件资源
* 把 IO thread  改变成 多线程的方式
  * mysql5.6  库进行多线程的方式
  * mysql5.7  GTID进行多线程的方式
*  应用程序自己去判断（mycat有这么方案）

