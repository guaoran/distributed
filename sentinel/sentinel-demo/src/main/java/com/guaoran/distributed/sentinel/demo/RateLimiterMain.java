package com.guaoran.distributed.sentinel.demo;

import com.google.common.util.concurrent.RateLimiter;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @author : 孤傲然
 * @description :  使用 guava 令牌桶实现限流，并不是分布式的限流
 * @date :2019/11/2 13:24
 */
public class RateLimiterMain {
    //令牌桶的实现
    RateLimiter rateLimiter = RateLimiter.create(10); //qps
//    RateLimiter rateLimiter = RateLimiter.create(10,0, TimeUnit.MILLISECONDS); //qps

    public void doTest(){
        if(rateLimiter.tryAcquire()){ //这里就是获得一个令牌，成功获得了一个令牌
            System.out.println("允许通过进行访问");
        }else{
            System.out.println("被限流了");
        }
    }

    public static void main(String[] args) throws IOException {
        RateLimiterMain rateLimiterMain=new RateLimiterMain();
        CountDownLatch countDownLatch=new CountDownLatch(1);
        Random random=new Random();
        for(int i=0;i<20;i++){
            new Thread(()->{
                try {
                    countDownLatch.await();
                    Thread.sleep(random.nextInt(1000));
                    rateLimiterMain.doTest();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
        countDownLatch.countDown();
        System.in.read();
    }
}
