package com.guaoran.distributed.sentinel.provider;

import com.alibaba.csp.sentinel.cluster.ClusterStateManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * @author : 孤傲然
 * @description : 服务提供
 * @date :2020/7/4 10:40
 */
@SpringBootApplication
public class SentinelProviderApplication {
    // -Dproject.name=App-demo -Dcsp.sentinel.dashboard.server=localhost:8888 -Dcsp.sentinel.log.use.pid=true
    public static void main(String[] args) throws IOException {
        //表示当前的节点是集群客户端
        ClusterStateManager.applyState(ClusterStateManager.CLUSTER_CLIENT);
        SpringApplication.run(SentinelProviderApplication.class,args);
        System.in.read();
    }
}
