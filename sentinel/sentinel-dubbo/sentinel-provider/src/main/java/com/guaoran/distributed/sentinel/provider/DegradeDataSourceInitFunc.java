package com.guaoran.distributed.sentinel.provider;

import com.alibaba.csp.sentinel.init.InitFunc;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRule;
import com.alibaba.csp.sentinel.slots.block.degrade.DegradeRuleManager;

import java.util.Collections;

/**
 * @author : 孤傲然
 * @description :  加载熔断的规则
 * @date :2020/7/4 10:40
 */
public class DegradeDataSourceInitFunc implements InitFunc {

    @Override
    public void init() throws Exception {
        DegradeRule rule = new DegradeRule();
        rule.setResource("com.guaoran.distributed.sentinel.SentinelService");
        // 根据平均响应时间 ： 默认1 秒内5个请求
        rule.setGrade(RuleConstant.DEGRADE_GRADE_RT);
        // 请求的平均响应时间 单位毫秒
        rule.setCount(10);
        // 如果熔断，多久进行恢复 单位秒
        rule.setTimeWindow(5);
        DegradeRuleManager.loadRules(Collections.singletonList(rule));
    }



}
