package com.guaoran.distributed.sentinel.provider;

import com.guaoran.distributed.sentinel.SentinelService;
import org.apache.dubbo.config.annotation.Service;

import java.time.LocalDateTime;

/**
 * @author : 孤傲然
 * @description : Sentinel
 * @date :2020/7/4 10:31
 */
@Service
public class SentinelServiceImpl implements SentinelService {
    @Override
    public String say(String message) {
//        try {
//            Thread.sleep(500);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return "say : " + message + ": " + LocalDateTime.now() + " end!";
    }
}
