package com.guaoran.distributed.sentinel.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : 孤傲然
 * @description : web
 * @date :2020/7/4 11:16
 */
@SpringBootApplication
public class SentinelWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(SentinelWebApplication.class,args);
    }
}
