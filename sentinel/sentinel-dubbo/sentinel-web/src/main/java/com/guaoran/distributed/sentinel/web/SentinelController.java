package com.guaoran.distributed.sentinel.web;

import com.guaoran.distributed.sentinel.SentinelService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author : 孤傲然
 * @description : web
 * @date :2020/7/4 11:16
 */
@RestController
public class SentinelController {

    @Reference(timeout = 3000)
    SentinelService sentinelService;

    @GetMapping("/say")
    public String sayHello(){
        return sentinelService.say("test");
    }
}
