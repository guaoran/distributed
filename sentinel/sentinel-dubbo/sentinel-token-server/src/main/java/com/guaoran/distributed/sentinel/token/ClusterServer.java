package com.guaoran.distributed.sentinel.token;

import com.alibaba.csp.sentinel.cluster.server.ClusterTokenServer;
import com.alibaba.csp.sentinel.cluster.server.SentinelDefaultTokenServer;
import com.alibaba.csp.sentinel.cluster.server.config.ClusterServerConfigManager;
import com.alibaba.csp.sentinel.cluster.server.config.ServerTransportConfig;

import java.util.Collections;

/**
 * @author : 孤傲然
 * @description : ClusterServer
 * @date :2020/7/4 16:40
 */
public class ClusterServer {

    // -Dproject.name=App-demo -Dcsp.sentinel.dashboard.server=localhost:8888 -Dcsp.sentinel.log.use.pid=true
    public static void main(String[] args) throws Exception {
        ClusterTokenServer tokenServer=new SentinelDefaultTokenServer();
        ClusterServerConfigManager.loadGlobalTransportConfig(
                new ServerTransportConfig().setIdleSeconds(600).setPort(9999));
        ClusterServerConfigManager.loadServerNamespaceSet(Collections.singleton("App-demo")); //设置成动态
        tokenServer.start();
    }
}
