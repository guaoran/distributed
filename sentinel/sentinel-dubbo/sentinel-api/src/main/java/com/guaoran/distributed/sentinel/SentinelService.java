package com.guaoran.distributed.sentinel;

/**
 * @author : 孤傲然
 * @description : sentinel
 * @date :2020/7/4 10:30
 */
public interface SentinelService {
    String say(String message);
}
