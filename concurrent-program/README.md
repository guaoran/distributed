# 目录

-   [并发编程的基础](#并发编程的基础)
-   [线程的安全性问题](#线程的安全性问题)
    -   [缓存一致性问题](#缓存一致性问题)
        -   [总线锁](#总线锁)
        -   [缓存锁](#缓存锁)
        -   [缓存一致性问题](#缓存一致性问题-1)
        -   [CPU的优化执行](#cpu的优化执行)
        -   [并发编程的问题](#并发编程的问题)
        -   [JMM 内存模型](#jmm-内存模型)
-   [JMM
    怎么解决原子性、可见性、有序性的问题？](#jmm-怎么解决原子性可见性有序性的问题)
    -   [原子性保障](#原子性保障)
    -   [可见性](#可见性)
    -   [有序性](#有序性)
    -   [volatile如何保证可见性](#volatile如何保证可见性)
    -   [volatile防止指令重排序](#volatile防止指令重排序)
    -   [内存屏障](#内存屏障)
        -   [store barrier 写屏障](#store-barrier-写屏障)
        -   [load barrier 读屏障](#load-barrier-读屏障)
        -   [Full barrier](#full-barrier)
    -   [Synchronized](#synchronized)
        -   [synchronized有三种方式来加锁](#synchronized有三种方式来加锁)
            -   [synchronized括号后面的对象](#synchronized括号后面的对象)
        -   [Synchronized 锁的原理](#synchronized-锁的原理)
            -   [Java对象头](#java对象头)
                -   [Mawrk Word](#mawrk-word)
            -   [Monitor](#monitor)
        -   [synchronized的锁升级和获取过程](#synchronized的锁升级和获取过程)
            -   [自旋锁（CAS）](#自旋锁cas)
            -   [偏向锁](#偏向锁)
            -   [轻量级锁](#轻量级锁)
            -   [重量级锁](#重量级锁)
        -   [wait 和 notify](#wait-和-notify)
            -   [wait和notify的原理](#wait和notify的原理)
            -   [wait和notify为什么需要在synchronized里面](#wait和notify为什么需要在synchronized里面)
-   [JUC](#juc)
    -   [锁](#锁)
        -   [ReentrantLock](#reentrantlock)
        -   [ReentrantReadWriteLock](#reentrantreadwritelock)
        -   [公平锁和非公平锁](#公平锁和非公平锁)
    -   [Condition](#condition)
    -   [CountDownLatch](#countdownlatch)
        -   [使用场景](#使用场景)
    -   [Semaphore](#semaphore)
        -   [使用场景](#使用场景-1)
    -   [原子操作](#原子操作)
    -   [线程池](#线程池)
        -   [线程池使用](#线程池使用)
        -   [submit和execute的区别](#submit和execute的区别)
        -   [ThreadpoolExecutor](#threadpoolexecutor)
        -   [newFixedThreadPool](#newfixedthreadpool)
        -   [newCachedThreadPool](#newcachedthreadpool)
        -   [newSingleThreadScheduledExecutor](#newsinglethreadscheduledexecutor)
        -   [newScheduledThreadPool](#newscheduledthreadpool)


# 并发编程的基础

多线程使用的场景

* 通过并行计算提高程序执行性能
* 需要等待网络、I/O响应导致耗费大量的执行时间，可以采用异步线程的方式来减少阻塞

线程实现的方式：

* Thread

* Runnable

* Callable，future

  ```java
  public class CallableDemo implements Callable<String> {
      public static void main(String[] args) 
          throws ExecutionException, InterruptedException {
          ExecutorService executorService = Executors.newCachedThreadPool();
          CallableDemo callableDemo = new CallableDemo();
          Future<String> f = executorService.submit(callableDemo);
          System.out.println(f.get());
          executorService.shutdown();
      }
      @Override
      public String call() throws Exception {
          return "test";
      }
  }
  ```

线程的状态

* NEW 初始，没有调用start()
* RUNNABLE 运行
* BLOCKED 阻塞
* WAITING  等待
* TIMED_WAITING 时间等待
* TERMINATED 中止

![1551150963226](assets/1551150963226.png)

通过相应命令显示线程状态

> 打开终端或者命令提示符，键入“jps”，（JDK1.5提供的一个显示当前所有java进程pid的命令），可以获得相应进程的pid
>
> 根据上一步骤获得的pid，继续输入jstack pid（jstack是java虚拟机自带的一种堆栈跟踪工具。jstack用于打印出给定的java进程ID或core file或远程调试服务的Java堆栈信息）

线程启动

```java
public class ThreadStart {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new Thread(()->{
            System.out.println(Thread.currentThread().getName());
        },"thread").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        },"runnable").start();

        //继承Thread 类
        ThreadDemo threadDemo = new ThreadDemo();
        threadDemo.start();
        //实现Runnable 接口
        RunableDemo runableDemo = new RunableDemo();
        Thread thread = new Thread(runableDemo);
        thread.start();
        //实现Callable 接口
        ExecutorService executorService = Executors.newCachedThreadPool();
        CallableDemo callableDemo = new CallableDemo();
        Future<String> f = executorService.submit(callableDemo);
        System.out.println(f.get());
        executorService.shutdown();
    }

    static class ThreadDemo extends Thread{
        @Override
        public void run() {
            System.out.println("ThreadDemo");
        }
    }
    static class RunableDemo implements Runnable{
        @Override
        public void run() {
            System.out.println("Runnable");
        }
    }
}
```

线程停止

```java
public class ThreadStop {
    private static int i=0;
    private static int j=0;
    private static volatile boolean isStop = false;
    public static void main(String[] args) throws InterruptedException {
        //通过 interrupt 中断线程
        Thread thread = new Thread(()->{
            while(!Thread.currentThread().isInterrupted()){
                i++;
            }
            System.out.println(Thread.currentThread().getName()+i);
        },"通过 interrupt 中断线程");
        thread.start();
        TimeUnit.SECONDS.sleep(1);
        thread.interrupt();

        //通过标志位
        Thread threadFlag = new Thread(()->{
            while(!isStop){
                j++;
            }
            System.out.println(Thread.currentThread().getName()+j);
        },"通过标志位");
        threadFlag.start();
        TimeUnit.SECONDS.sleep(1);
        isStop = true;
    }
}
```

# 线程的安全性问题

线程安全问题可以总结为: 可见性、原子性、有序性这几个问题

## 缓存一致性问题

![1551163332204](assets/1551163332204.png)

CPU-0读取主内存的数据，缓存到CPU-0的高速缓存中，CPU-1也做了同样的事情，而CPU-1把count的值修改成了2，并且同步到CPU-1的高速缓存，但是这个修改以后的值并没有写入到主存中，CPU-0访问该字节，由于缓存没有更新，所以仍然是之前的值，就会导致数据不一致的问题

引发这个问题的原因是因为多核心CPU情况下存在指令并行执行，而各个CPU核心之间的数据不共享从而导致缓存一致性问题，为了解决这个问题，CPU生产厂商提供了相应的解决方案

### 总线锁

当一个CPU对其缓存中的数据进行操作的时候，往总线中发送一个Lock信号。其他处理器的请求将会被阻塞，那么该处理器可以独占共享内存。总线锁相当于把CPU和内存之间的通信锁住了，所以这种方式会导致CPU的性能下降，所以P6系列以后的处理器，出现了另外一种方式，就是缓存锁。

### 缓存锁

如果缓存在处理器缓存行中的内存区域在LOCK操作期间被锁定，当它执行锁操作回写内存时，处理器不在总线上声明LOCK信号，而是修改内部的缓存地址，然后通过缓存一致性机制来保证操作的原子性，因为缓存一致性机制会阻止同时修改被两个以上处理器缓存的内存区域的数据，当其他处理器回写已经被锁定的缓存行的数据时会导致该缓存行无效。
所以如果声明了CPU的锁机制，会生成一个LOCK指令，会产生两个作用

1. Lock前缀指令会引起处理器缓存回写到内存，在P6以后的处理器中，LOCK信号一般不锁总线，而是锁缓存
2. 一个处理器的缓存回写到内存会导致其他处理器的缓存无效

![1551163627077](assets/1551163627077.png)

### 缓存一致性问题

处理器上有一套完整的协议，来保证Cache的一致性，比较经典的应该就是MESI协议了，它的方法是在CPU缓存中保存一个标记位，这个标记为有四种状态

1.  M(Modified) 修改缓存，当前CPU缓存已经被修改，表示已经和内存中的数据不一致了
2.  I(Invalid) 失效缓存，说明CPU的缓存已经不能使用了
3.  E(Exclusive) 独占缓存，当前cpu的缓存和内存中数据保持一直，而且其他处理器没有缓存该数据
4.  S(Shared) 共享缓存，数据和内存中数据一致，并且该数据存在多个cpu缓存中

每个Core的Cache控制器不仅知道自己的读写操作，也监听其它Cache的读写操作，嗅探（snooping）"协议
CPU的读取会遵循几个原则

1. 如果缓存的状态是I，那么就从内存中读取，否则直接从缓存读取
2. 如果缓存处于M或者E的CPU 嗅探到其他CPU有读的操作，就把自己的缓存写入到内存，并把自己的状态设置为S
3. 只有缓存状态是M或E的时候，CPU才可以修改缓存中的数据，修改后，缓存状态变为M

### CPU的优化执行

除了增加高速缓存以外，为了更充分利用处理器内部的运算单元，处理器可能会对输入的代码进行乱序执行优化，处理器会在计算之后将乱序执行的结果重组，保证该结果与顺序执行的结果一致，但并不保证程序中各个语句计算的先后顺序与输入代码中的顺序一致，这个是处理器的优化执行；还有一个就是编程语言的编译器也会有类似的优化，比如做指令重排来提升性能。

### 并发编程的问题

原子性、可见性、有序性问题，是我们抽象出来的概念，他们的核心本质就是缓存一致性问题、处理器优化问题导致的指令重排序问题。

比如缓存一致性就导致可见性问题、处理器的乱序执行会导致原子性问题、指令重排会导致有序性问题。为了解决这些问题，所以在JVM中引入了JMM的概念。

### JMM 内存模型

Java内存模型定义了线程和内存的交互方式，在JMM抽象模型中，分为主内存、工作内存。主内存是所有线程共享的，工作内存是每个线程独有的。线程对变量的所有操作（读取、赋值）都必须在工作内存中进行，不能直接读写主内存中的变量。并且不同的线程之间无法访问对方工作内存中的变量，线程间的变量值的传递都需要通过主内存来完成，他们三者的交互关系如下

![1551164059885](assets/1551164059885.png)

所以，总的来说，JMM是一种规范，目的是解决由于多线程通过共享内存进行通信时，存在**本地内存数据不一致、编译器会对代码指令重排序、处理器会对代码乱序执行**等带来的问题。目的是保证并发编程场景中的原子性、可见性和有序性。



# JMM 怎么解决原子性、可见性、有序性的问题？

## 原子性保障

在java中提供了两个高级的字节码指令monitorenter和monitorexit，在Java中对应的**Synchronized**来保证代码块内的操作是原子的

## 可见性

Java中的volatile关键字提供了一个功能，那就是被其修饰的变量在被修改后可以立即同步到主内存，被其修饰的变量在每次使用之前都从主内存刷新。因此，可以使用volatile来保证多线程操作时变量的可见性。
除了**volatile**，Java中的**synchronized**和**final**两个关键字也可以实现可见性

## 有序性

在Java中，可以使用**synchronized**和**volatile**来保证多线程之间操作的有序性。实现方式有所区别：

***volatile关键字会禁止指令重排。synchronized关键字保证同一时刻只允许一条线程操作***。

## volatile如何保证可见性

volatile 变量修饰的共享变量，在进行写操作的时候会多出一个lock前缀的汇编指令，这个指令会触发缓存锁，通过缓存一致性协议（MESI）来解决可见性问题。

对于声明了volatile 的变量进行写操作，JVM 就会向处理器发送一条Lock前缀的指令，把这个变量所在的缓存行的数据写会到系统内存，再根据缓存一致性协议（MESI）来保证多CPU下的各个高速缓存中的数据的一致性。

## volatile防止指令重排序

指令重排的目的是为了最大化的提高CPU利用率以及性能，CPU的乱序执行优化在单核时代并不影响正确性，但是在多核时代的多线程能够在不同的核心上实现真正的并行，一旦线程之间共享数据，就可能会出现一些不可预料的问题

指令重排序必须要遵循的原则是，不影响代码执行的最终结果，编译器和处理器不会改变存在数据依赖关系的两个操作的执行顺序，(这里所说的数据依赖性仅仅是针对单个处理器中执行的指令和单个线程中执行的操作.)

这个语义，实际上就是as-if-serial语义，不管怎么重排序，单线程程序的执行结果不会改变，编译器、处理器都必须遵守as-if-serial语义

## 内存屏障

内存屏障需要解决两个问题，一个是编译器的优化乱序和CPU的执行乱序，我们可以分别使用优化屏障和内存屏障这两个机制来解决

写屏障(store barrier)、读屏障(load barrier)和全屏障(Full Barrier)，主要的作用是

*  防止指令之间的重排序
* 保证数据的可见性

### store barrier 写屏障

store barrier称为写屏障，相当于storestore barrier, 强制所有在storestore内存屏障之前的所有执行，都要在该内存屏障之前执行，并发送缓存失效的信号。所有在storestore barrier指令之后的store指令，都必须在storestore barrier屏障之前的指令执行完后再被执行。也就是禁止了写屏障前后的指令进行重排序，使得所有store barrier之前发生的内存更新都是可见的（这里的可见指的是修改值可见以及操作结果可见）

### load barrier 读屏障

load barrier称为读屏障，相当于loadload barrier，强制所有在load barrier读屏障之后的load指令，都在load barrier屏障之后执行。也就是进制对load barrier读屏障前后的load指令进行重排序， 配合store barrier，使得所有store barrier之前发生的内存更新，对load barrier之后的load操作是可见的

### Full barrier

full barrier成为全屏障，相当于storeload，是一个全能型的屏障，因为它同时具备前面两种屏障的效果。强制了
所有在storeload barrier之前的store/load指令，都在该屏障之前被执行，所有在该屏障之后的的store/load指
令，都在该屏障之后被执行。禁止对storeload屏障前后的指令进行重排序。

总结：内存屏障只是解决顺序一致性问题，不解决缓存一致性问题，缓存一致性是由cpu的缓存锁以及MESI协议来
完成的。而缓存一致性协议只关心缓存一致性，不关心顺序一致性。所以这是两个问题

## Synchronized

解决原子性、一致性、有序性

```java
public class SynchronizedDemo {
    public static Object objectLock  = new Object();
    public Object lock = new Object();
    public void locks(){
        //对象级别全局锁
        synchronized (objectLock){}
        //实例级别
        synchronized (lock){}
    }
    public static void test(){
        //全局锁，针对 SynchronizedDemo 对象而言是无论多少个对象，调用该方法都会进行竞争锁
        synchronized (SynchronizedDemo.class){}
    }
    //全局锁，针对 SynchronizedDemo 对象而言是无论多少个对象，调用该方法都会进行竞争锁
    public synchronized static void test2(){}

    public void demo(){
        synchronized (this){
            //针对对象的锁，如果是不同的实例对象，是不存在锁的竞争的
        }
    }
    public static void main(String[] args) {
        SynchronizedDemo.test();
        SynchronizedDemo.test2();
        SynchronizedDemo demo = new SynchronizedDemo();
        SynchronizedDemo demo2 = new SynchronizedDemo();
        demo.demo();
        demo2.demo();
    }
}
```

### synchronized有三种方式来加锁

1. 修饰实例方法，作用于当前实例加锁，进入同步代码前要获得当前实例的锁
2. 静态方法，作用于当前类对象加锁，进入同步代码前要获得当前类对象的锁
3. 修饰代码块，指定加锁对象，对给定对象加锁，进入同步代码库前要获得给定对象的锁。

#### synchronized括号后面的对象

synchronized扩后后面的对象是一把锁，在java中任意一个对象都可以成为锁，简单来说，我们把object比喻是一
个key，拥有这个key的线程才能执行这个方法，拿到这个key以后在执行方法过程中，这个key是随身携带的，并且只有一把。如果后续的线程想访问当前方法，因为没有key所以不能访问只能在门口等着，等之前的线程把key放回去。所以，synchronized锁定的对象必须是同一个，如果是不同对象，就意味着是不同的房间的钥匙，对于访问者来说是没有任何影响的

####synchronized的字节码指令

通过javap -v 来查看对应代码的字节码指令，对于同步块的实现使用了monitorenter和monitorexit指令，前面我们在讲JMM的时候，提到过这两个指令，他们隐式的执行了Lock和UnLock操作，用于提供原子性保证。

monitorenter指令插入到同步代码块开始的位置、monitorexit指令插入到同步代码块结束位置，jvm需要保证每个monitorenter都有一个monitorexit对应。

这两个指令，本质上都是对一个对象的监视器(monitor)进行获取，这个过程是排他的，也就是说同一时刻只能有一个线程获取到由synchronized所保护对象的监视器

线程执行到monitorenter指令时，会尝试获取对象所对应的monitor所有权，也就是尝试获取对象的锁；而执行monitorexit，就是释放monitor的所有权

### Synchronized 锁的原理

jdk1.6以后对synchronized锁进行了优化，包含偏向锁、轻量级锁、重量级锁; 在了解synchronized锁之前，我们需要了解两个重要的概念，一个是对象头、另一个是monitor

#### Java对象头

在Hotspot虚拟机中，对象在内存中的布局分为三块区域：对象头、实例数据和对齐填充；Java对象头是实现synchronized的锁对象的基础，一般而言，synchronized使用的锁对象是存储在Java对象头里。它是轻量级锁和偏向锁的关键

##### Mawrk Word

Mark Word用于存储对象自身的运行时数据，如哈希码（HashCode）、GC分代年龄、锁状态标志、线程持有的锁、偏向线程 ID、偏向时间戳等等。Java对象头一般占有两个机器码（在32位虚拟机中，1个机器码等于4字节，也就是32bit）

![1551194081629](assets/1551194081629.png)

#### Monitor

什么是Monitor？我们可以把它理解为一个同步工具，也可以描述为一种同步机制。所有的Java对象是天生的Monitor，每个object的对象里 markOop->monitor() 里可以保存ObjectMonitor的对象。从源码层面分析一下monitor对象

1. oop.hpp下的oopDesc类是JVM对象的顶级基类，所以每个object对象都包含markOop

2. markOop.hpp**中** markOopDesc继承自oopDesc，并扩展了自己的monitor方法，这个方法返回一个ObjectMonitor指针对象

3. objectMonitor.hpp,在hotspot虚拟机中，采用ObjectMonitor类来实现monitor，

### synchronized的锁升级和获取过程

synchronized的锁是进行过优化的，引入了偏向锁、轻量级锁；锁的级别从低到高逐步升级， 无锁->偏向锁->轻量级锁->重量级锁.

#### 自旋锁（CAS）

自旋锁就是让不满足条件的线程等待一段时间，而不是立即挂起。看持有锁的线程是否能够很快释放锁。怎么自旋呢？其实就是一段没有任何意义的循环。
虽然它通过占用处理器的时间来避免线程切换带来的开销，但是如果持有锁的线程不能在很快释放锁，那么自旋的线程就会浪费处理器的资源，因为它不会做任何有意义的工作。所以，自旋等待的时间或者次数是有一个限度的，如果自旋超过了定义的时间仍然没有获取到锁，则该线程应该被挂起

#### 偏向锁

大多数情况下，锁不仅不存在多线程竞争，而且总是由同一线程多次获得，为了让线程获得锁的代价更低而引入了偏向锁。当一个线程访问同步块并获取锁时，会在对象头和栈帧中的锁记录里存储锁偏向的线程ID，以后该线程在进入和退出同步块时不需要进行CAS操作来加锁和解锁，只需简单地测试一下对象头的Mark Word里是否存储着指向当前线程的偏向锁。如果测试成功，表示线程已经获得了锁。如果测试失败，则需要再测试一下Mark Word中偏向锁的标识是否设置成1（表示当前是偏向锁）：如果没有设置，则使用CAS竞争锁；如果设置了，则尝试使用CAS将对象头的偏向锁指向当前线程

#### 轻量级锁

引入轻量级锁的主要目的是在没有多线程竞争的前提下，减少传统的重量级锁使用操作系统互斥量产生的性能消耗。当关闭偏向锁功能或者多个线程竞争偏向锁导致偏向锁升级为轻量级锁，则会尝试获取轻量级锁

#### 重量级锁

重量级锁通过对象内部的监视器（monitor）实现，其中monitor的本质是依赖于底层操作系统的Mutex Lock实现，操作系统实现线程之间的切换需要从用户态到内核态的切换，切换成本非常高。
前面我们在讲Java对象头的时候，讲到了monitor这个对象，在hotspot虚拟机中，通过ObjectMonitor类来实现monitor。他的锁的获取过程的体现会简单很多

![1551194554464](assets/1551194554464.png)



### wait 和 notify 

wait和notify是用来让线程进入等待状态以及使得线程唤醒的两个操作

```java
public class ThreadWait extends Thread {
    private Object lock;
    public ThreadWait(Object lock) {
        this.lock = lock;
    }
    @Override
    public void run() {
        synchronized (lock) {
            System.out.println("开始执行 thread wait");
            try {
                lock.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("执行结束 thread wait");
        }
    }
}

public class ThreadNotify extends Thread {
    private Object lock;
    public ThreadNotify(Object lock) {
        this.lock = lock;
    }
    @Override
    public void run() {
        synchronized (lock) {
            System.out.println("开始执行 thread notify");
            lock.notify();
            System.out.println("执行结束 thread notify");
        }
    }
    public static void main(String[] args) {
        Object lock = new Object();
        ThreadWait wait = new ThreadWait(lock);
        wait.start();
        ThreadNotify notify = new ThreadNotify(lock);
        notify.start();
    }
}
```

#### wait和notify的原理

调用wait方法，**首先会获取监视器锁，获得成功以后，会让当前线程进入等待状态进入等待队列并且释放锁**；然后当其他线程调用notify或者notifyall以后，**会选择从等待队列中唤醒任意一个线程，而执行完notify方法以后，并不会立马唤醒线程，原因是当前的线程仍然持有这把锁，处于等待状态的线程无法获得锁**。必须要等到当前的线程执行完按monitorexit指令以后，也就是锁被释放以后，处于等待队列中的线程就可以开始竞争锁了

![1551195191773](assets/1551195191773.png)

#### wait和notify为什么需要在synchronized里面

wait方法的语义有两个，一个是释放当前的对象锁、另一个是使得当前线程进入阻塞队列， 而这些操作都和监视器是相关的，所以wait必须要获得一个监视器锁
而对于notify来说也是一样，它是唤醒一个线程，既然要去唤醒，首先得知道它在哪里？所以就必须要找到这个对象获取到这个对象的锁，然后到这个对象的等待队列中去唤醒一个线程。



# JUC

## 锁

### ReentrantLock

重入锁，表示支持重新进入的锁，也就是说，如果当前线程t1通过调用lock方法获取了锁之后，再次调用lock，是不会再阻塞去获取锁的，直接增加重试次数就行了。

```java
public class LockDemo {
    static Lock lock = new ReentrantLock();//重入锁，包含公平锁和非公平锁
    private static int count = 0;
    /*
    synchronized和lock的区别
    1. lock是类上的实现，synchronized是jvm的关键字
    2. lock比synchronized更灵活 ，lock可以选择在什么时候进行加锁和释放锁
       1. synchronized释放锁：异常或执行完同步代码块
    3. lock可以判断锁的状态，而synchronized是一个关键字，无法灵活判断锁的状态
    4. lock 的实现可以是 ReentrantLock是一个重入锁，可以是公平或非公平锁，synchronized是一个非公平锁
     */
    public static void incr(){
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lock.lock();
        count ++;
        lock.unlock();
    }
}
```

### ReentrantReadWriteLock

可重入读写锁，适合读多写少的场景。 读锁：共享锁，写锁: 排它锁

```java
public class RwLockDemo {
    static ReentrantReadWriteLock reentrantReadWriteLock = new ReentrantReadWriteLock();
    static Lock readLock = reentrantReadWriteLock.readLock();
    static Lock writeLock = reentrantReadWriteLock.writeLock();
    /**
     * 读写锁：适合读多写少
     *  读锁：共享锁
     *  写锁: 排它锁
     */
    //缓存
    static Map<String,Object> cacheMap = new HashMap<>();

    public Object get(String key){
        readLock.lock();
        try {
            return cacheMap.get(key);
        }finally {
            readLock.unlock();
        }
    }
    public Object set(String key,Object v){
        writeLock.lock();
        try {
            return cacheMap.put(key,v);
        }finally {
            writeLock.unlock();
        }
    }
}

```



### 公平锁和非公平锁

```java
//非公平锁
static final class NonfairSync extends Sync {
    //非公平锁中，会首先进行竞争拿到锁，拿不到锁之后再进入accquire()方法中
    final void lock() {
        //这个方法的话，会对已经等待锁的线程来说是不公平的
        if (compareAndSetState(0, 1))
            setExclusiveOwnerThread(Thread.currentThread());
        else
            acquire(1);
    }

    protected final boolean tryAcquire(int acquires) {
        return nonfairTryAcquire(acquires);
    }
}
//公平锁
static final class FairSync extends Sync {
	//公平锁中，不会进行竞争锁，而是直接进入到accquire()方法中
    final void lock() {
        acquire(1);
    }
}
```



## Condition

通过await 和 signal  实现wait 和 notify

```java
public class ConditionAwait extends Thread {
    private Lock lock;
    private Condition condition;

    public ConditionAwait(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }
    @Override
    public void run() {
        try {
            lock.lock();
            System.out.println("开始执行 ConditionAwait");
            try {
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("执行结束 ConditionAwait");
        }finally {
            lock.unlock();
        }
    }
}

public class ConditionSignal extends Thread{
    private Lock lock;
    private Condition condition;
    public ConditionSignal(Lock lock, Condition condition) {
        this.lock = lock;
        this.condition = condition;
    }
    @Override
    public void run() {
        try {
            lock.lock();
            System.out.println("开始执行 ConditionSignal");
            condition.signal();
            System.out.println("执行结束 ConditionSignal");
        }finally {
            lock.unlock();
        }
    }
    public static void main(String[] args) throws InterruptedException {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        ConditionAwait await = new ConditionAwait(lock,condition);
        await.start();
        await.join(10);
        ConditionSignal signal = new ConditionSignal(lock,condition);
        signal.start();
    }
}
```

## CountDownLatch

countdownlatch是一个同步工具类，它允许一个或多个线程一直等待，直到其他线程的操作执行完毕再执行

countdownlatch提供了两个方法，一个是countDown，一个是await， countdownlatch初始化的时候需要传入一个整数，在这个整数倒数到0之前，调用了await方法的程序都必须要等待，然后通过countDown来倒数。

```java
public class CountDownLatchDemo {
    public static void main(String[] args) throws InterruptedException {
        Long time = System.currentTimeMillis();
        CountDownLatch countDownLatch = new CountDownLatch(3);
        new Thread(()->{
            countDownLatch.countDown();
        }).start();
        new Thread(()->{
            countDownLatch.countDown();
        }).start();
        new Thread(()->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            countDownLatch.countDown();
        }).start();
        countDownLatch.await();
        System.out.println("用时"+(System.currentTimeMillis()-time)/1000+"s");
    }
}
```

从代码的实现来看，有点类似join的功能，但是比join更加灵活。CountDownLatch构造函数会接收一个int类型的参数作为计数器的初始值，当调用CountDownLatch的countDown方法时，这个计数器就会减一。通过await方法去阻塞去阻塞主流程

### 使用场景

通过countdownlatch实现最大的并行请求，也就是可以让N个线程同时执行

## Semaphore

semaphore也就是我们常说的信号灯，semaphore可以控制同时访问的线程个数，通过acquire获取一个许可，如果没有就等待，通过release释放一个许可。有点类似限流的作用。叫信号灯的原因也和他的用处有关，比如某商场就5个停车位，每个停车位只能停一辆车，如果这个时候来了10辆车，必须要等前面有空的车位才能进入。

```java
public class SemaphoreDemo {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(5);
        for (int i = 0; i < 10; i++) {
            new DoAnything(i,semaphore).start();
        }
    }
    static class DoAnything extends Thread{
        private int num;
        private Semaphore semaphore;
        public DoAnything(int num, Semaphore semaphore) {
            this.num = num;
            this.semaphore = semaphore;
        }
        @Override
        public void run() {
            try {
                semaphore.acquire();
                System.out.println("第"+num+"个线程进入");
                Thread.sleep(2000);
                semaphore.release();
                System.out.println("第"+num+"个线程释放");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
```

### 使用场景

可以实现对某些接口访问的限流

## 原子操作

```java
public class AtomicDemo {
    private static AtomicInteger count = new AtomicInteger(0);

    public static synchronized void inc() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count.getAndIncrement();
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                AtomicDemo.inc();
            }).start();
        }
        Thread.sleep(4000);
        System.out.println(count.get());
    }
}
```



## 线程池

Java中的线程池是运用场景最多的并发框架，几乎所有需要异步或并发执行任务的程序都可以使用线程池。线程池就像数据库连接池的作用类似，只是线程池是用来重复管理线程避免创建大量线程增加开销。

所以合理的使用线程池可以

1. 降低创建线程和销毁线程的性能开销
2. 合理的设置线程池大小可以避免因为线程数超出硬件资源瓶颈带来的问题，类似起到了限流的作用；线程是稀缺资源，如果无线创建，会造成系统稳定性问题

创建线程池
1. newFixedThreadPool 固定线程池 LinkedBlockingQueue
2. newSingleThreadExecutor 单个线程池 LinkedBlockingQueue
3. newCachedThreadPool 缓存线程池 SynchronousQueue

线程池的工作原理：
1. 首先判断核心线程池里的线程是否都在执行任务，如果不是则创建一个线程进行处理任务。
2. 如果都在执行任务，则判断工作队列是否满了，如果未满，则添加到工作队列中， 
3. 如果工作队列满了，则判断线程池的线程是否都在执行任务，如果不是，则创建一个线程进行处理任务。
4. 如果线程池的线程都在执行任务，则交给饱和策略来处理。


饱和策略
1. 直接抛异常
2. 丢弃队列中最近的一个任务
3. 不处理，丢弃掉

###  线程池使用

```java
public class ExecutorsDemo implements Runnable{
    public static void main(String[] args) {
//        ExecutorService executorService = null;
//        //创建一个固定的线程池,负载比较大的
//        executorService = Executors.newFixedThreadPool(1);
//        //创建只有一个线程的线程池
//        executorService = Executors.newSingleThreadExecutor();
//        //创建一个不限制最大线程数
//        executorService = Executors.newCachedThreadPool();
//        //定时器，延时执行的线程池
//        executorService = Executors.newScheduledThreadPool(1);

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 100; i++) {
            //有返回值，且可以提交Runnable和 Callable 的线程
            executorService.submit(new ExecutorsDemo());
            //只能提交Runnable的线程
            //executorService.execute();
        }
        executorService.shutdown();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
    }
}
```

### submit和execute的区别

执行一个任务，可以使用submit和execute，这两者有什么区别呢？

1. execute只能接受Runnable类型的任务
2. submit不管是Runnable还是Callable类型的任务都可以接受，但是Runnable返回值均为void，所以使用Future的get()获得的还是null

### ThreadpoolExecutor

四种线程池构建工具，都是基于ThreadPoolExecutor 类，它的构造函数参数

```java
public ThreadPoolExecutor(
    			int corePoolSize, //核心线程数量
				int maximumPoolSize, //最大线程数
				long keepAliveTime, //超时时间,超出核心线程数量以外的线程空余存活时间
				TimeUnit unit, //存活时间单位
				BlockingQueue<Runnable> workQueue, //保存执行任务的队列
				ThreadFactory threadFactory,//创建新线程使用的工厂
				RejectedExecutionHandler handler //当任务无法执行的时候的处理方式
) { }
```

### newFixedThreadPool

> 固定大小数量的线程池，用LinkedBlockingQueue 来存储消息，适合负载大的服务器

```java
public static ExecutorService newFixedThreadPool(int nThreads) {
    return new ThreadPoolExecutor(nThreads, nThreads,
                                  0L, TimeUnit.MILLISECONDS,
                                  new LinkedBlockingQueue<Runnable>());
}
```

newFixedThreadPool 的核心线程数和最大线程数都是指定值，也就是说当线程池中的线程数超过核心线程数后，任务都会被放到阻塞队列中。另外 keepAliveTime 为 0，也就是超出核心线程数量以外的线程没有存活时间。而这里选用的阻塞队列是 LinkedBlockingQueue，使用的是默认容量 Integer.MAX_VALUE，相当于没有上限

这个线程池执行任务的流程如下：

1. 线程数少于核心线程数，也就是设置的线程数时，新建线程执行任务
2. 线程数等于核心线程数后，将任务加入阻塞队列
3. 由于队列容量非常大，可以一直添加
4. 执行完任务的线程反复去队列中取任务执行

用途：FixedThreadPool 用于负载比较大的服务器，为了资源的合理利用，需要限制当前线程数量

### newCachedThreadPool

> 创建一个不限制最大线程数（Integer.MAX_VALUE) 的线程池

```java
public static ExecutorService newCachedThreadPool() {
    return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                  60L, TimeUnit.SECONDS,
                                  new SynchronousQueue<Runnable>());
}
```

newCachedThreadPool 创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程; 并且没有核心线程，非核心线程数无上限，但是每个空闲的时间只有 60 秒，超过后就会被回收。

它的执行流程如下：

1. 没有核心线程，直接向 SynchronousQueue 中提交任务
2. 如果有空闲线程，就去取出任务执行；如果没有空闲线程，就新建一个
3. 执行完任务的线程有 60 秒生存时间，如果在这个时间内可以接到新任务，就可以继续活下去，否则就被回收

### newSingleThreadScheduledExecutor

> 创建只有一个线程的线程池,它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序(FIFO, LIFO, 优先级)执行

```java
public static ScheduledExecutorService newSingleThreadScheduledExecutor() {
    return new DelegatedScheduledExecutorService
        (new ScheduledThreadPoolExecutor(1));
}
```

### newScheduledThreadPool

> 定时器，延时执行的线程池

```java
public static ScheduledExecutorService newScheduledThreadPool(int corePoolSize) {
    return new ScheduledThreadPoolExecutor(corePoolSize);
}
```

![1551251340944](assets/1551251340944.png)





问题：

volatile 是干嘛的？

1. 可以保证可见性和防止内存重排序
2. lock 使得缓存锁失效
3. 内存屏障
   1. store store
   2. store load
   3. load store
   4. load load

wait 和 sleep 的区别

1. wait 会释放锁，sleep 不会释放锁
2. notify 释放锁的时候，需要拿到锁才能进行释放

synchronized和lock的区别

1. lock是类上的实现，synchronized是jvm的关键字
2. lock比synchronized更灵活 ，lock可以选择在什么时候进行加锁和释放锁
   1. synchronized释放锁：异常或执行完同步代码块
3. lock可以判断锁的状态，而synchronized是一个关键字，无法灵活判断锁的状态
4. lock 的实现可以是 `ReentrantLock`是一个重入锁，可以是公平或非公平锁，synchronized是一个非公平锁




