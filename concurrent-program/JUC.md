# 目录

-   [JUC](#juc)
    -   [ReentrantLock](#reentrantlock)
        -   [应用程序实例](#应用程序实例)
        -   [原理分析](#原理分析)
            -   [类图关系](#类图关系)
            -   [`lock unLock`](#lock-unlock)
                -   [公平锁和非公平锁的体现](#公平锁和非公平锁的体现)
                -   [竞争锁的体现（非公平锁）](#竞争锁的体现非公平锁)
                    -   [ThreadA 获得锁](#threada-获得锁)
                    -   [ThreadB、ThreadC、ThreadD
                        排队获得锁](#threadbthreadcthreadd-排队获得锁)
                    -   [锁的释放](#锁的释放)
            -   [`await signal`](#await-signal)
                -   [线程的等待与唤醒的体现](#线程的等待与唤醒的体现)
                    -   [LockAwaitDemo.await](#lockawaitdemo.await)
                    -   [LockSignalDemo.signal](#locksignaldemo.signal)
                    -   [await 被唤醒后的逻辑](#await-被唤醒后的逻辑)
            -   [疑问](#疑问)
    -   [CountDownLatch](#countdownlatch)
        -   [应用程序实例](#应用程序实例-1)
        -   [原理分析](#原理分析-1)
            -   [类图关系](#类图关系-1)
            -   [`await`](#await)
            -   [`countDown`](#countdown)
            -   [`await` 被唤醒后的逻辑](#await-被唤醒后的逻辑-1)
            -   [疑问](#疑问-1)


# JUC

## ReentrantLock

### 应用程序实例

```java
/**
 * @Author guaoran
 * @Description ReentrantLock 加锁、await、signal
 * 2019-05-22 13:36
 */
public class ReentrantLockDemo {
    public static void main(String[] args) {
        // 实例化一把重入锁
        Lock lock = new ReentrantLock();
        // 获得 condition
        Condition condition = lock.newCondition();
        // 启动两个线程，维护同一把锁和condition
        new LockAwaitDemo(lock,condition).start();
        new LockSignalDemo(lock,condition).start();
    }
    static class LockAwaitDemo extends Thread {
        private Lock lock;
        private Condition condition;

        public LockAwaitDemo(Lock lock, Condition condition) {
            this.lock = lock;
            this.condition = condition;
        }

        @Override
        public void run() {
            lock.lock();
            try {
                System.out.println("await...start");
                condition.await();
                System.out.println("await...end");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }
    static class LockSignalDemo extends Thread {
        private Lock lock;
        private Condition condition;

        public LockSignalDemo(Lock lock, Condition condition) {
            this.lock = lock;
            this.condition = condition;
        }

        @Override
        public void run() {
            lock.lock();
            try {
                System.out.println("signal...start");
                condition.signal();
                System.out.println("signal...end");
            } finally {
                lock.unlock();
            }
        }
    }
}
```

输出结果

> await...start
>
> signal...start
>
> signal...end
>
> await...end
>

### 原理分析

#### 类图关系

![1559199606244](assets/1559199606244.png)

 ![1559030492912](assets/1559030492912.png)

由上图可看出锁的机制主要是通过 AQS 来实现的，通过了解 `ReentrantLock` 类的关系图，下面的代码也会比较好理解一些。

> 根据实例代码可知 程序创建一把重入锁，并获得一个 Condition ，启动两个线程并将同一把重入锁传入线程中，此时在 `main` 线程中(不考虑 signal 线程先执行)，首先启动 `LockAwaitDemo` 线程，然后 `LockSignalDemo`  线程执行。因为是同一把锁，当  `LockAwaitDemo`  拿到锁之后，`LockSignalDemo`  将去竞争锁失败而阻塞。
>
> 当  `LockAwaitDemo`  获得锁执行 `await()` 方法,必将该线程阻塞并释放锁，此时 `LockSignalDemo`  线程将重新获得锁并执行 `signal()` 方法来唤醒 `await()` 阻塞的线程，当 `LockSignalDemo`  执行完 `unlock()` 释放锁后，  `LockAwaitDemo`  继续执行。

疑问：

1. **公平锁和非公平锁的体现：** `ReentrantLock` 是一个重入锁，包含公平和非公平锁，在代码那种是怎么体现公平和非公平的？
2. **竞争锁的体现：** 当  `LockAwaitDemo` 线程拿到锁之后 ，`LockSignalDemo` 是怎么竞争锁的？如果竞争失败是怎么阻塞的？
3. **线程的等待与唤醒的体现：** 当 `LockAwaitDemo` 线程执行 `await()` 时， 是怎么阻塞当前线程的？ 当 `LockAwaitDemo` 线程阻塞后， `LockSignalDemo` 是怎么通过阻塞进行获得锁的？ 当 `LockSignalDemo` 线程拿到锁之后，是怎么唤醒 `LockAwaitDemo` 阻塞线程的？



#### `lock unLock`

默认非公平锁

```java
public ReentrantLock() {
    // 默认是非公平锁
    sync = new NonfairSync();
}
```

##### 公平锁和非公平锁的体现

在 `ReentrantLock` 中，非公平锁是在进入 `lock` 方法后，线程会直接通过 `CAS` 操作竞争锁，如果正好有其他线程释放锁则该线程便会抢到锁，这种方式对之前未获得到锁后处于排队等待的线程来说是不公平的。

`FairSync`

```java
final void lock() {
    // 排队获取锁
    acquire(1);
}
// 公平锁的公平再次被体现
protected final boolean tryAcquire(int acquires) {
    // 获得当前线程
    final Thread current = Thread.currentThread();
    // 获得锁的状态：大于0 表示持有锁，大于1表示持有锁重入的次数
    int c = getState();
    // 为 0 表示并没有线程获得锁，有可能是拥有锁的线程正好释放锁
    if (c == 0) {
        // 如果 该线程前面没有排队的线程，
        // 则 通过 CAS 获得锁
        // 该处是公平锁的又一个公平之处， 保证所有的线程都能按顺序排队去获得锁
        if (!hasQueuedPredecessors() &&
            compareAndSetState(0, acquires)) {
            // 为当前线程设置独占
            setExclusiveOwnerThread(current);
            return true;
        }
    }
    else if (current == getExclusiveOwnerThread()) {
        int nextc = c + acquires;
        if (nextc < 0)
            throw new Error("Maximum lock count exceeded");
        setState(nextc);
        return true;
    }
    return false;
}
```

`NonfairSync`

```java
 final void lock() {
     // 默认先去争夺锁，如果争夺成功，则设置该线程为独占锁
     // state 参数：0 代表没有锁，大于等于1 代表已经有锁了和重入的次数
     // 公平锁和非公平锁的差异就是在这里，
     // 非公平锁是线程一进来就进行去争夺锁，这对其他已经过来的线程来说是不公平的，
     // 而公平锁是线程进来之后就按照排队顺序去获得锁，
     if (compareAndSetState(0, 1))
         setExclusiveOwnerThread(Thread.currentThread());
     else
         // 没有拿到锁，去排队获得锁
         acquire(1);
 }
// 非公平尝试获得锁
protected final boolean tryAcquire(int acquires) {
    return nonfairTryAcquire(acquires);
}
// 非公平尝试获得锁
// 非公平锁的不公平之处再次被体现
final boolean nonfairTryAcquire(int acquires) {
    // 拿到当前线程
    final Thread current = Thread.currentThread();
    // 获得当前锁的状态
    int c = getState();
    if (c == 0) {
        // 代表当前没有锁，去尝试竞争锁，如果竞争成功了，则修改当前线程为独占锁
        // 如果此时存在其他线程在排队获得锁，而此时有线程释放锁，
        // 则 当前线程便会获得锁，那么 对其他排队等待锁的线程来说是不公平的
        if (compareAndSetState(0, acquires)) {
            setExclusiveOwnerThread(current);
            return true;
        }
    }
    // 如果已经存在锁了，并且 当前线程和独占锁的线程是同一个，代表是重入了，
    else if (current == getExclusiveOwnerThread()) {
        // 增加重入次数
        int nextc = c + acquires;
        if (nextc < 0) // overflow
            throw new Error("Maximum lock count exceeded");
        // 设置锁的状态
        setState(nextc);
        return true;
    }
    return false;
}
```

##### 竞争锁的体现（非公平锁）

> **场景**：如果此时有 A、B、C 、D 四个线程，第一次进来时默认 线程 A 获得锁，线程A 释放锁的时候，线程D正好进来竞争锁，此时线程D获得锁，线程B、C 继续排队竞争锁（阻塞）

###### ThreadA 获得锁

```java
final void lock() {
    // 默认先去争夺锁，如果争夺成功，则设置该线程为独占锁
    // state 参数：0 代表没有锁，大于等于1 代表已经有锁了和重入的次数
    // 公平锁和非公平锁的差异就是在这里，
    // 非公平锁是线程一进来就进行去争夺锁，这对其他已经过来的线程来说是不公平的，
    // 而公平锁是线程进来之后就按照排队顺序去获得锁，
	// ThreadA 获得锁，state=1，exclusiveOwnerThread=ThreadA
    if (compareAndSetState(0, 1))
        setExclusiveOwnerThread(Thread.currentThread());
    else
        // 没有拿到锁，去排队获得锁
        // ThreadB、ThreadC 去排队获得锁
        acquire(1);
}
```

当ThreadA 进来的时候，由于没有线程获得锁，线程A 通过 CAS 获得锁，并设置独占  `state=1` 和 `exclusiveOwnerThread=ThreadA` ，拿到线程后，ThreadA 便会去执行自己的代码块

###### ThreadB、ThreadC、ThreadD 排队获得锁

```java
// 公平锁和非公平锁 都会执行的方法
// 在这里是进行排队去获得锁，
public final void acquire(int arg) {
    // 尝试去获得锁，获得锁失败 后会将当前线程设置为独占锁并添加到队列中，此时并没有拿到锁
    // 重写父类方法区尝试获得锁，
    // 如果拿到锁其他线程在该线程释放锁之前则不会再次拿到锁了，
    // 如果没拿到锁，说明已经存在线程在执行了
    if (!tryAcquire(arg) &&
        // 尝试去获得锁，如果获得锁失败，进行park ,并中断线程，
        
        // addWaiter: 将当钱线程封装为node节点，并设置为tail，
        // 如果header 为 null，则设置一个空node节点作为header

        // acquireQueued :通过判断tail的节点的上一个节点是不是header节点，
        // 如果是header 节点的话，则让tail节点通过CAS 去获得锁，成功修改状态
        // 如果获得锁失败，判断是否需要挂起
        acquireQueued(addWaiter(Node.EXCLUSIVE), arg))

        // 中断当前线程，
        // 当获得锁失败且tail节点的上一个节点waitStatus=SIGNAL（-1）
        selfInterrupt();
}
```

尝试获得锁，如果 ThreadA 还未释放锁，竞争失败则会将该线程添加到等待队列中

1. **`tryAcquire(1)`** 

   ```java
   // 非公平尝试获得锁
   final boolean nonfairTryAcquire(int acquires) {
       // 拿到当前线程
       final Thread current = Thread.currentThread();
       // 获得当前锁的状态
       // 如果此时 ThreadA 释放锁后,线程D 正好进来，那么线程D有可能会直接获得锁，
       // 这里举例 ThreadA 正好释放，ThreadD 正好获得锁，此时ThreadB、ThreadC 则继续排队
       int c = getState();
       if (c == 0) {
           // 代表当前没有锁，去尝试竞争锁，如果竞争成功了，则修改当前线程为独占锁
           // 如果此时存在其他线程在排队获得锁，而此时有线程释放锁，
           // 则 当前线程便会获得锁，那么 对其他排队等待锁的线程来说是不公平的
           if (compareAndSetState(0, acquires)) {
               setExclusiveOwnerThread(current);
               return true;
           }
       }
       // 如果已经存在锁了，并且 当前线程和独占锁的线程是同一个，代表是重入了，
       else if (current == getExclusiveOwnerThread()) {
           // 增加重入次数
           int nextc = c + acquires;
           if (nextc < 0) // overflow
               throw new Error("Maximum lock count exceeded");
           // 设置锁的状态
           setState(nextc);
           return true;
       }
       return false;
   }
   ```

   在此时ThreadA还未释放锁，所以线程B 和线程C 、线程D 会继续尝试获得锁，当获得锁失败后，返回 `false`，继续执行 `acquireQueued(addWaiter(Node.EXCLUSIVE), 1))`

2. **`acquireQueued(addWaiter(Node.EXCLUSIVE), 1))`**

   当ThreadC 获得锁失败 进来添加到等待队列中,

   1. **`addWaiter(Node.EXCLUSIVE)`**

      将当前线程封装为node节点，并设置为tail，如果header 为 null，则设置一个空node节点作为header，在设置tail，最终返回该组装的node节点（ *tail 节点*）

      ```java
      // 添加当前线程到同步队列中：为当前线程和给定模式创建节点和排队节点
      // 返回tail 
      private Node addWaiter(Node mode) {
          // 将一个 Node.EXCLUSIVE 为null 的 node 节点和当前线程封装成一个node
          Node node = new Node(Thread.currentThread(), mode);
          // Try the fast path of enq; backup to full enq on failure
          // 将tail 节点赋值给pred节点
          Node pred = tail;
          // 如果最后一个节点不为空，则将新组装的节点的上一个节点设置为pred ,
          // 并将新组装的节点变成tail，如果成功，则将pred的next节点指向新组装的节点
          if (pred != null) {
              // 将node节点的上一个节点指向pred
              node.prev = pred;
              // 比较并替换node节点为tail节点
              if (compareAndSetTail(pred, node)) {
                  // 将pred的next节点指向node节点
                  pred.next = node;
                  return node;
              }
          }
          // 如果tail（pred） == null ,说明 head 和 tail  目前都是null，即不存在要等待的节点
          // 将新加入的线程组成的node节点 添加到等待锁的队列中,并将node 转换成tail
          // 此处 和上面的功能差不多，都是将node节点转成tail ，不同之处是此处采用自旋，
          // 如果不存在head节点则创建一个head节点，如果存在则将tail 安全替换成node 节点
          enq(node);
          return node;
      }
      
      // 一句话来说就是 将新加入的线程组成的node节点 添加到等待锁的队列中，并将node 转换成tail
      // 并返回旧tail
      // 此处通过自旋操作，判断head是否为空（根据此处的逻辑可以看出，如果tail为null，则head一定也null）
      // 如果head 为空，则创建一个head节点 并赋值给tail
      // 如果 tail 不为空，则在tail 的后面添加一个node 的节点关系，并将node节点转换成新的tail，并重新维护他们之间的关系
      // 将node节点的上一个节点指向旧tail，并将 tail 替换成 node ，旧tail 的下一个节点指向 node（新tail）
      private Node enq(final Node node) {
          for (;;) {
              Node t = tail;
              if (t == null) { // Must initialize
                  // 比较并替换 head 节点，并赋值给tail
                  if (compareAndSetHead(new Node()))
                      tail = head;
              } else {
                  // 将 node 节点的上一个节点指向 旧tail 节点（t)
                  node.prev = t;
                  // 将 node 设置为 tail节点
                  if (compareAndSetTail(t, node)) {
                      // 将旧 tail节点（t) 的下一个节点指向node（新的tail）
                      t.next = node;
                      return t;
                  }
              }
          }
      }
      ```

      ![1559194732983](assets/1559194732983.png)

   2. **`acquireQueued(tail,1)`**

      用上面的图来解释：

      该方法通过自旋进行操作，如果node 是 线程B，则可以去尝试获得锁（因为线程B的上一个节点时header），如果获得锁失败或者node节点是线程C（线程C所在节点的prev 不是header节点） 则会判断是否应该将该线程挂起 `shouldParkAfterFailedAcquire(prev,node)`。

      **`shouldParkAfterFailedAcquire(prev,node)`** 方法主要做了以下几个事情

      1. 判断 prev 的 `waitStatus` 状态
      2. 如果是 **`SIGNAL   = -1`** 表示可以进行挂起
      3. 如果是 **`CANCELLED = 1`** 表示该线程已经取消了,则从等待队列中删除
      4. 如果是  **`PROPAGATE = -3` 或者 ** **`0 默认状态`** ，则修改为 **`SIGNAL`** 状态

      ```java
      // 返回true：表示中断线程，进行等待
      // 返回false：表示 获得锁
      
      // 通过 node 节点 去判断当前节点的上一个节点是不是header 节点，
      // 如果是header节点，则去尝试获得锁
      // 如果不是header 节点，则判断该node节点的上一个节点的waitStatus
      // waitStatus:
      // 如果是取消状态则从队列中删除，
      // 如果是0或-3 ，则自旋设置为-1（SIGNAL），
      // 如果是-1（SIGNAL） 则挂起当前线程，等待上一个节点的锁释放后才会被唤醒
      final boolean acquireQueued(final Node node, int arg) {
          boolean failed = true;
          try {
              // 中断标志
              boolean interrupted = false;
              for (;;) {
                  // 获得 node 的上一个节点
                  final Node p = node.predecessor();
                  // 如果head == node的上一个节点，
                  // 说明该node 上面没有其他要等待的节点，则竞争获得得锁
                  if (p == head && tryAcquire(arg)) {
                      // 获得锁成功，将node设置为head
                      setHead(node);
                      p.next = null; // help GC
                      failed = false;
                      return interrupted;
                  }
                  // 由于是在自旋，如果获得锁失败或者该节点的上一个节点不是header 节点，
                  // 获得锁失败后是否需要park，如果 p.waitStatus = -1,返回true
                  if (shouldParkAfterFailedAcquire(p, node) &&
                      // park当前线程
                      parkAndCheckInterrupt())
                      interrupted = true;
              }
          } finally {
              if (failed)
                  cancelAcquire(node);
          }
      }
      
      // 获得锁失败后是否应该挂起
      // CANCELLED =  1 : 取消状态
      // SIGNAL    = -1 ：只要前置节点释放锁，就会通知标识为 SIGNAL 状态的后续节点的线程
      // CONDITION = -2 ：在Condition中（await,signal）中 会使用到
      // PROPAGATE = -3 ：下一个acquireShared应该无条件传播
      // 1. 当 线程的pred线程被取消（1）时，会将该线程从竞争锁队列中删除
      // 2. 当 线程的pred线程状态不是被取消（1)或者不是 -1 时，会将该线程的pred线程设置为 -1 :SIGNAL
      // 3. 当 线程的pred线程状态是-1是，则返回true，表示可以进行挂起
      private static boolean shouldParkAfterFailedAcquire(Node pred, Node node) {
          int ws = pred.waitStatus;
          if (ws == Node.SIGNAL) // -1
              /*
               * This node has already set status asking a release
               * to signal it, so it can safely park.
               */
              return true;
          if (ws > 0) { // CANCELLED = 1
              // ws >0 即 final int CANCELLED =  1; 为取消状态，
              // 此时做的是将 取消状态的节点删除，该节点的前置节点设置为该几点的后置节点前置节点
              // 举个例子： 如果存在 A ，B ，C 三个节点，
              // A <----prev----B-----next----->C
              // A -----next--->B<----prev------C
              // 此时将B 节点（线程）中断，即设置为CANCELLED ，
              // 那么该方法做了以下操作：
              // pred = B.prev // A
              // c.prev = pred // A  (原来是B)
              // A.next = C;
              // 最终的结果为：
              // A <----prev----C
              // A -----next--->C
              /*
               * Predecessor was cancelled. Skip over predecessors and
               * indicate retry.
               */
              do {
                  // pred = B.prev // A
                  // c.prev = pred // A  (原来是B)
                  node.prev = pred = pred.prev;
              } while (pred.waitStatus > 0);
              // A.next = C;
              pred.next = node;
          } else {// 0,-3
              /*
               * waitStatus must be 0 or PROPAGATE.  Indicate that we
               * need a signal, but don't park yet.  Caller will need to
               * retry to make sure it cannot acquire before parking.
               */
              // 将 pred 的 (waitStatus)状态 设置为 Node.SIGNAL = -1
              compareAndSetWaitStatus(pred, ws, Node.SIGNAL);
          }
          return false;
      }
      ```

      ![1559195938086](assets/1559195938086.png)


###### 锁的释放

由上面可以知道线程A在未释放锁的时候，线程B 和线程C会 添加到等待队列中，并设置上一个节点的状态为 `SIGNAL` 并挂起当前线程。

那么在什么时候唤醒线程B 和线程C的？答案是肯定是释放锁的时候。那么是怎么操作的呢？接下来再接着看源码

```java
// 释放锁
public void unlock() {
    sync.release(1);
}
// 释放锁
public final boolean release(int arg) {
    // 尝试去释放锁，释放锁成功（冲入的锁全部释放完）的话
    if (tryRelease(arg)) {
        Node h = head;
        // 此时 head 的 waitStatus = -1
        if (h != null && h.waitStatus != 0) // h.waitStatus = -1（SIGNAL）
            // unpark head 节点
            unparkSuccessor(h);
        return true;
    }
    return false;
}
// 尝试去释放锁
protected final boolean tryRelease(int releases) {
    // 获得重入的次数，每次释放减去1，直到减为0
    int c = getState() - releases;
    if (Thread.currentThread() != getExclusiveOwnerThread())
        throw new IllegalMonitorStateException();
    boolean free = false;
    if (c == 0) {
        free = true;
        // 设置独占锁为null
        setExclusiveOwnerThread(null);
    }
    // 设置锁的状态标志
    setState(c);
    return free;
}
// 唤醒 node 的下一个 接班人（node.next)
private void unparkSuccessor(Node node) {
    /*
         * If status is negative (i.e., possibly needing signal) try
         * to clear in anticipation of signalling.  It is OK if this
         * fails or if status is changed by waiting thread.
         */
    int ws = node.waitStatus;
    if (ws < 0)
        // 将 node（head) 的 waitStatus 设置为0
        // 在这里之所以设置为 0 ，是因为在 park 线程的时候， 是将head 节点的状态设置为-1 ，
        // 而在下面进行线程唤醒的时候，是通过查找 waitStatus = -1 的，
        // 所以通过这种方式就可以避免再执行到该节点
        compareAndSetWaitStatus(node, ws, 0);

    /*
     * Thread to unpark is held in successor, which is normally
     * just the next node.  But if cancelled or apparently null,
     * traverse backwards from tail to find the actual
     * non-cancelled successor.
     */
    Node s = node.next;
    // 如果 head 的下一个节点为空 或者 该节点已经被取消了
    if (s == null || s.waitStatus > 0) {
        s = null;
        // 在这里是通过从tail 开始遍历， 通过 prev 来循环
        // 为什么要从 tail 通过prev  倒着开始遍历呢 ？
        // 可以从enq 方法中可以看出

        /*
            node.prev = t;
            if (compareAndSetTail(t, node)) {
                // 将旧 tail节点（t) 的下一个节点指向node（新的tail）
                t.next = node;
                return t;
            }
         */
        // 设置tail 是 通过CAS 替换的 ，也就是说 next 不一定存在，但是 prev 是一定会存在的

        for (Node t = tail; t != null && t != node; t = t.prev)
            // 0，-1,-2,-3 在park的时候，已经设置为 -1 了，
            if (t.waitStatus <= 0)
                s = t;
    }
    if (s != null)
        // 唤醒 head 的下一个节点，
        // unpark
        LockSupport.unpark(s.thread);
}

```



#### `await signal`

##### 线程的等待与唤醒的体现

我们知道在进行线程的等待和唤醒的时候，需要拿到锁，通过锁中维护的 `Condition` 来实现锁的等待与唤醒

```java
// 获得一个 Condition 
public Condition newCondition() {
    return sync.newCondition();
}
final ConditionObject newCondition() {
    return new ConditionObject();
}
// ConditionObject 中维护了一个等待队列 
public class ConditionObject implements Condition, java.io.Serializable {
    private static final long serialVersionUID = 1173984872572414699L;
    /** First node of condition queue. */
    private transient Node firstWaiter;
    /** Last node of condition queue. */
    private transient Node lastWaiter;
    public ConditionObject() { }
}
```

###### LockAwaitDemo.await

该方法是将获得锁的线程添加到等待队列中 `new Node('LockAwaitDemo Thread', Node.CONDITION)` ，释放锁并使当前线程处于阻塞状态。

```java
// 等待
public final void await() throws InterruptedException {
    // 判断线程是否被中断，
    // 因为该方法会清除标志位，
    // 如果该线程未执行中断，则会返回false
    // 如果中断过第一次执行该方法则会返回true，再次执行也是会返回false
    if (Thread.interrupted())
        throw new InterruptedException();
    // 将当前页面添加到等待队列中，如果队列不存在则创建一个，
    // 并返回该节点（包含当前线程和waitStatus=CONDITION)
    Node node = addConditionWaiter();

    // 释放当前节点的重入锁，并返回重入次数
    int savedState = fullyRelease(node);
    int interruptMode = 0;
    // 判断该节点是否存在同步队列中，
    // 如果不存在同步队列中，则 park 当前线程
    while (!isOnSyncQueue(node)) {
        LockSupport.park(this);
        // 当代码执行了 await()方法时，当前线程将阻塞在这里
        // 到此为止主要做了几步：
        // 1. 线程拿到锁 ，从同步队列中取出了
        // 2. 线程释放锁，并将该线程封装到等待队列中
        // 3. 线程阻塞在这里，等待从等待队列中再次取出该线程进行后续操作

		// todo  下面的等执行完 signal 后在分析
        if ((interruptMode = checkInterruptWhileWaiting(node)) != 0)
            break;
    }
    if (acquireQueued(node, savedState) && interruptMode != THROW_IE)
        interruptMode = REINTERRUPT;

    if (node.nextWaiter != null) // clean up if cancelled
        unlinkCancelledWaiters();
    if (interruptMode != 0)
        reportInterruptAfterWait(interruptMode);
}

// 添加当前线程到等待队列中
// 将当前线程添加到等待队列最后一个节点中
private Node addConditionWaiter() {
    Node t = lastWaiter;
    // If lastWaiter is cancelled, clean out.
    // 如果 最后一个等待节点不为空且不为CONDITION 说明为取消状态，则删除该节点
    if (t != null && t.waitStatus != Node.CONDITION) {
        // 删除处于取消状态的节点，
        // 并再次获得最后一个等待节点（该节点不再是取消状态了,因为已经删除了所有出于取消状态的节点）
        unlinkCancelledWaiters();
        t = lastWaiter;
    }
    // 等待队列的节点：当前线程，CONDITION（-2）
    Node node = new Node(Thread.currentThread(), Node.CONDITION);
    if (t == null)
        firstWaiter = node;
    else
        t.nextWaiter = node;
    lastWaiter = node;
    return node;
}
// 删除处于取消状态的节点
// 从等待队列中的第一个节点开始进行遍历判断是否是取消状态，如果是取消状态则删除该节点，
// 将该节点的下一个节点的引用取消，并将该节点的下一个节点作为该等待队列的第一个节点
private void unlinkCancelledWaiters() {
    Node t = firstWaiter;
    // 用来做中间值 ，来删除已经取消的节点
    Node trail = null;
    while (t != null) {
        Node next = t.nextWaiter;
        // 如果 firstWaiter 节点的 waitStatus ！= CONDITION ，说明是取消状态的节点
        if (t.waitStatus != Node.CONDITION) {
            // 设置 firstWaiter 下一个节点的引用为空，
            t.nextWaiter = null;
            if (trail == null)
                // 在等待队列中，如果第一个节点为取消状态则将第原本处于第二的节点设置为第一个节点
                firstWaiter = next;
            else
                trail.nextWaiter = next;
            if (next == null)
                // 如果没有下一个节点了，则将该节点设置为最后一个节点
                lastWaiter = trail;
        }
        else
            trail = t;
        t = next;
    }
}

// 释放当前等待节点的所有锁，返回当前锁重入的次数
final int fullyRelease(Node node) {
    boolean failed = true;
    try {
        // 锁标记，大于0 表示有锁，大于1表示冲入的次数
        int savedState = getState();
        // 释放重入锁
        if (release(savedState)) {
            failed = false;
            return savedState;
        } else {
            throw new IllegalMonitorStateException();
        }
    } finally {
        if (failed)
            // 失败则设置为取消状态
            node.waitStatus = Node.CANCELLED;
    }
}

// 判断当前节点是否在同步队列中
final boolean isOnSyncQueue(Node node) {
    // 如果该节点的状态是 -2 ，则不在同步队列中
    // 如果该节点的prev为空，有两种可能，
    // 一种是 在等待队列中，因为该队列中的节点没有prev属性，所以为空
    // 另一个是head 节点， 即获得锁的节点，也不存在同步队列中
    if (node.waitStatus == Node.CONDITION || node.prev == null)
        return false;
    // 因为同步队列中存在next ，而等待队列中存在 nextWaiter ,
    // 所以 node.next 不为空说明就是同步队列
    if (node.next != null) // If has successor, it must be on queue
        return true;
    /*
         * node.prev can be non-null, but not yet on queue because
         * the CAS to place it on queue can fail. So we have to
         * traverse from tail to make sure it actually made it.  It
         * will always be near the tail in calls to this method, and
         * unless the CAS failed (which is unlikely), it will be
         * there, so we hardly ever traverse much.
         */
    // 从tail 节点倒着查看该节点是否存在同步队列中
    // node.prev可以是非null，但尚未在队列中，
    // 因为将其置于队列中的CAS可能会失败。
    // 所以我们必须从尾部穿过以确保它实际上成功。
    return findNodeFromTail(node);
}

// 从tail倒着遍历，如果存在 节点等于 node ，
// 则说明在同步队列中存在该节点
// 为什么这样做呢？
// 因为在 enq 中是先设置prev 的关系，在通过CAS 替换tail ，
// 如果此时prev 不为空，但是 next 却没有，便可以通过这种方式找到
private boolean findNodeFromTail(Node node) {
    Node t = tail;
    for (;;) {
        if (t == node)
            return true;
        if (t == null)
            return false;
        t = t.prev;
    }
}
```

![1559210629611](assets/1559210629611.png)

###### LockSignalDemo.signal

从同步队列中取出 `firstWaiter` , 并将该节点状态由 `CONDITION(-2)` CAS 替换成 `SIGNAL(-1)` ,并添加当前节点到同步队列中，进入到同步队列中的线程会再次去争抢锁。

```java
// 唤醒线程
public final void signal() {
    // 获得锁的线程不是当前线程，报错
    if (!isHeldExclusively())
        throw new IllegalMonitorStateException();

    Node first = firstWaiter;
    if (first != null)
        doSignal(first);
}
private void doSignal(Node first) {
    do {
        // 从等待队列中获得第一个节点下一个节点，赋值给第一个节点，
        // 即 从等待队列中取出第一个节点，
        // 如果原本处于第二的节点为空，说明该等待对列中就只有一个等待节点，
        // 节点取出直接取完了，则设置lastWaiter 为空
        if ( (firstWaiter = first.nextWaiter) == null)
            lastWaiter = null;
        // 取消 first 节点的 nextWaiter 的指向，帮助GC
        first.nextWaiter = null;

        // 设置该first 节点的waitStatus 由-1 变为0 ，
        // 如果替换失败，且 等待队列中还有其他节点的话，继续进行替换下面的节点
    } while (!transferForSignal(first) &&
             (first = firstWaiter) != null);
}
final boolean transferForSignal(Node node) {
    /*
     * If cannot change waitStatus, the node has been cancelled.
     */
    // CAS 替换 node 节点 的 waitStatus 为 0
    // 如果替换失败返回false ，只有一种可能就是节点被 CANCELLED 了
    // 为什么要替换？ 这是要将原本处于等待队列中的节点放到同步队列中
    // 比如 线程A 获得锁之后，进行await ，是将该线程添加到等待队列中并阻塞
    // signal 方法是将该等待队列再次放到 同步队列中，再次能够竞争锁
    if (!compareAndSetWaitStatus(node, Node.CONDITION, 0))
        return false;

    /*
     * Splice onto queue and try to set waitStatus of predecessor to
     * indicate that thread is (probably) waiting. If cancelled or
     * attempt to set waitStatus fails, wake up to resync (in which
     * case the waitStatus can be transiently and harmlessly wrong).
     */
    // CAS 修改 node 节点的状态成功，则将该节点添加到同步队列中
    // 并返回原本的tail 节点：即 node 添加到同步队列的上一个节点
    Node p = enq(node);
    int ws = p.waitStatus;
    // 如果该节点的上一个同步节点 是取消状态
    // 或者 修改上一个节点状态为 SIGNAL 失败则进行 unpark（唤醒该节点的线程）
    if (ws > 0 || !compareAndSetWaitStatus(p, ws, Node.SIGNAL))
        // 唤醒节点上的线程.
        LockSupport.unpark(node.thread);
    return true;
}
```

![1559265878069](assets/1559265878069.bmp)

###### await 被唤醒后的逻辑

上面主要做了两件事，

1. `LockAwaitDemo` 将拿到锁的线程封装成node(CONDITION) 节点添加到等待队列中， 释放锁并阻塞（park)当前线程
2. `LockSignalDemo` 将拿到锁的线程从等待队列中取出，将取出的等待节点添加到同步队列中并唤醒该节点的线程

到此为止我们知道了，`LockAwaitDemo` 线程已经被阻塞了， 而 `LockSignalDemo`  执行完 `doSignal()` 方法后会在执行了  `unLock()` 后才会释放锁，`LockAwaitDemo` 线程才会再次去争抢锁。那么抢到锁之后还会做什么呢？ 再接着 `await` 未分析完的代码分析。

```java
// 等待
public final void await() throws InterruptedException {
    // .....
    while (!isOnSyncQueue(node)) {
        LockSupport.park(this);
        // 当代码执行了 await()方法时，当前线程将阻塞在这里
        // 到此为止主要做了几步：
        // 1. 线程拿到锁 ，从同步队列中取出了
        // 2. 线程释放锁，并将该线程封装到等待队列中
        // 3. 线程阻塞在这里，等待从等待队列中再次取出该线程进行后续操作


        // 当 signal 执行完毕释放锁后会再次执行到这里。
        // 再等待时检查是否中断
        // 如果未中断过返回0 ， 中断过返回1
        if ((interruptMode = checkInterruptWhileWaiting(node)) != 0)
            break;
    }
    // 让该节点再次去竞争锁，重入次数为原来的释放的次数
    // 如果线程未被中断，interruptMode = 0
    // 后面的逻辑主要是判断在等待过程中线程是否被中断了，如果中断了会判断是抛异常还是继续中断
    if (acquireQueued(node, savedState) && interruptMode != THROW_IE)
        interruptMode = REINTERRUPT;

    // 删除处于取消状态的节点
    if (node.nextWaiter != null) // clean up if cancelled
        unlinkCancelledWaiters();
    if (interruptMode != 0)
        reportInterruptAfterWait(interruptMode);
}
private int checkInterruptWhileWaiting(Node node) {
    // 未中断过则返回0 ，
    return Thread.interrupted() ?
        // 如果在等待过程中，被中断了，则需要转移这些中断的节点
        (transferAfterCancelledWait(node) ? THROW_IE : REINTERRUPT) :
    0;
}
// 如果添加到同步队列中 则 返回true
// 添加不到同步队列中，返回false
final boolean transferAfterCancelledWait(Node node) {
    // 将等待节点添加到同步队列中
    if (compareAndSetWaitStatus(node, Node.CONDITION, 0)) {
        enq(node);
        return true;
    }
   
    while (!isOnSyncQueue(node))
        Thread.yield();
    return false;
}
```



#### 疑问

1. **公平锁和非公平锁的体现：** `ReentrantLock` 是一个重入锁，包含公平和非公平锁，在代码那种是怎么体现公平和非公平的？

   > 答：代码中主要是通过模板方法设计模式来设计的，可以通过构造参数指定是否公平锁，默认是非公平锁。
   >
   > 公平锁是无论你有多少个线程过来拿同一把锁，只能乖乖的排队，所以先来的线程会先拿到锁，是相对公平的。
   >
   > 非公平锁是第一次进来的时候就会去竞争锁，如果此时正好有线程释放锁，它就有可能获得锁，这种情况的话对其他已经在排队去或得锁的线程来说是不公平的。在排队取获得锁的时候，不会去判断该线程前面有没有还在排队的线程便去直接去竞争锁，这又是一处不公平的体现。

2. **竞争锁的体现：** 当  `LockAwaitDemo` 线程拿到锁之后 ，`LockSignalDemo` 是怎么竞争锁的？如果竞争失败是怎么阻塞的？阻塞后有线程释放锁又是怎么去解除阻塞竞争锁的？

   > 答：
   >
   > 1. 在  `LockAwaitDemo` 线程拿到锁后，会将当前线程设置到独占锁并设置锁的状态，在其未释放锁的时候  `LockSignalDemo` 线程只能添加到同步队列中`node(signalThread,0);` 作为 同步队列的`tail`节点。
   >
   > 2.  当 `LockSignalDemo` 线程添加到同步队列后会再次去竞争锁，如果失败会通过自旋将该节点的上一个节点的状态修改为 `waitStatus=-1`，并进行park 该节点（ `LockSignalDemo` 线程所在的节点）。
   > 3. 当 `LockAwaitDemo` 线程去释放锁的时候，会去唤醒head节点的next 节点的线程（unpark  `LockSignalDemo` 线程）。这样next  节点的线程（ `LockSignalDemo`）会继续通过自旋去竞争锁。

3. **线程的等待与唤醒的体现：** 当 `LockAwaitDemo` 线程执行 `await()` 时， 是怎么阻塞当前线程的？ 当 `LockAwaitDemo` 线程阻塞后， `LockSignalDemo` 是怎么通过阻塞进行获得锁的？ 当 `LockSignalDemo` 线程拿到锁之后，是怎么唤醒 `LockAwaitDemo` 阻塞线程的？

   > 答：
   >
   > 1. 当我们去进行线程的等待与唤醒的时候，需要拿到锁。拿到锁的线程便是head 节点（thread=null，waitStatus=-1，exclusiveOwnerThread=当前线程，state=重入次数），当进行` LockAwaitDemo.await()` 操作时，会释放重入锁，并将当前线程封装成node（thread= `LockAwaitDemo`，waitStatus=-2|CONDITION），添加到等待队列中并阻塞当前线程。
   > 2.  `LockAwaitDemo` 线程 释放锁（重入锁）后，此时  `LockSignalDemo` 线程会存在同步队列中，等待线程释放后进行unpark head 节点的next 节点（ `LockAwaitDemo` 线程），在通过自旋再次竞争锁
   > 3.  `LockSignalDemo`  线程拿到锁后，将从等待队列中取出firstWaiter 节点的waitStatus修改为0， 并进行唤醒该节点的线程（unpark  `LockAwaitDemo` 线程）和添加到同步队列中。当  `LockSignalDemo` 线程执行完所有的代码 并 `lock.unlock()`  释放锁后，此时  `LockAwaitDemo`  线程便可以继续去竞争锁并拿到锁



## CountDownLatch

### 应用程序实例

```java
public class CountDownLatchDemo extends Thread{

    static CountDownLatch countDownLatch = new CountDownLatch(2);
    public static void main(String[] args) {

        // 开启多个线程，并在run方法中进行线程等待，
        // 只有 countDown  归 0时才会进行执行，
        // 这种方式就是模拟并发执行
        for (int i = 0; i < 3; i++) {
            new CountDownLatchDemo().start();
        }
        // 当然 因为countDownLatch(2) ,所以 执行一次 countDown 还是不行的，
        // 真是情况下可以设置为1嘛  ，这里是例子合并成了一个
        countDownLatch.countDown();

        // 该线程是先执行业务逻辑， 最后在 countDown ， 计数归0 ，
        // 场景有点类似，在该线程中预热缓存 ，在上面多个线程中读取缓存
        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"\t"+"先执行我。。。。");
            countDownLatch.countDown();
        },"ThreadD").start();
    }

    @Override
    public void run() {
        try {
            // 在 countDown 计数器为归0 之前一直在这里阻塞
            countDownLatch.await();
            System.out.println(Thread.currentThread().getName()+
                               "\t"+System.currentTimeMillis());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
```

输出结果

> ThreadD	先执行我。。。。
>
> ThreadA	1559284347551
>
> ThreadB	1559284347551
>
> ThreadC	1559284347551
>

### 原理分析

#### 类图关系

![1559284765386](assets/1559284765386.png)

CountDownLatch  是一个同步工具类，它允许一个或多个线程一直等待，直到其他线程的操作执行完毕再执行。从命名可以解读到 countDown 是倒数的意思，类似于我们倒计时的概念。CountDownLatch 提供了两个方法，一个是 countDown，一个是 await，CountDownLatch  初始化的时候需要传入一个整数，在这个整数倒数到 0 之前，调用了 await 方法的程序都必须要等待，然后通过 countDown 来倒数。

疑问：

1. **`await 等待的原理：`**  当  `CountDownLatchDemo` 多个线程同时启动时，执行到 `run()` 后为什么会处于等待状态？
2. **`countDown 唤醒等待的原理：`** 而 当 `countDown` 执行多次后 计数器归 0 后，是怎么通知   `CountDownLatchDemo`  线程的？

#### `await`

我们知道在初始化 CountDownLatch 的时候， 会传入一个整数，每次执行一次 countDown() ,计数器减一，当计算器为 0 时，处于等待状态的线程才会继续运行。计数器未归 0  的时候这块代码到底是做了什么呢？

```java
// 根据所给的参数构造一个实例
public CountDownLatch(int count) {
    if (count < 0) throw new IllegalArgumentException("count < 0");
    // volatile int state = count;
    this.sync = new Sync(count);
}
```

从上面的构造可以知道， 初始化 CountDownLatch 的时候需要传入一个整数，该值最终被保存到 state 中。是volatile 修饰的，可以保证有序性和可见性。

```java
// 导致当前线程等待，直到锁存器倒计数到零，
// 除非线程是被中断
public void await() throws InterruptedException {
    // 获得共享锁
    sync.acquireSharedInterruptibly(1);
}
public final void acquireSharedInterruptibly(int arg)
    throws InterruptedException {
    // 如果线程被中断了，则抛出异常
    if (Thread.interrupted())
        throw new InterruptedException();
    // 尝试获得共享锁，
    // 如果 计数器为0 则返回1，否则返回-1
    if (tryAcquireShared(arg) < 0)
        // 表示当前计数器还未归0 ，需要去获得共享锁，
        // 将当前线程封装成node 节点添加到同步队列中，并再次获取锁
        // 如果计数器还未归0 则获取锁失败，
        // 并修改当前线程的上一个节点的waitStatus=SIGNAL(-1),
        // 并挂起当前线程
        doAcquireSharedInterruptibly(arg);
}
// 如果当前的 计数器 为0 返回1 否则返回-1
protected int tryAcquireShared(int acquires) {
    return (getState() == 0) ? 1 : -1;
}
private void doAcquireSharedInterruptibly(int arg)
    throws InterruptedException {
    // 创建一个共享锁，封装当前线程的节点到同步队列中
    final Node node = addWaiter(Node.SHARED);
    boolean failed = true;
    try {
        for (;;) {
            // 拿到当前节点的上一个节点
            final Node p = node.predecessor();
            // 如果该当前线程的上一个节点 是 head 节点，则尝试去获得锁
            if (p == head) {
                // r=1 表示计数器已归0
                // r=-1 表示计数器还未归0
                int r = tryAcquireShared(arg);
                // 如果此时 正好CountDown 次数 使计数器归 0
                // 因为是自旋，当线程挂起后再次被唤醒后还是会执行到这块代码
                
                // todo 这块等分析完 countDown 之后再来分析
                if (r >= 0) {
                    setHeadAndPropagate(node, r);
                    p.next = null; // help GC
                    failed = false;
                    return;
                }
            }
            // 获得锁失败后，
            // 将该节点的上一个节点 waitStatus设置为-1，
            // 删除取消的节点
            // 如果当前节点的上一个节点 的waitStatus=-1时，返回true
            
            // 这个方法在分析 lock 方法时已经分析过了，
            // 主要做了一下功能
            // 获得锁失败后是否应该挂起
    		// CANCELLED =  1 : 取消状态
    		// SIGNAL    = -1 ：只要前置节点释放锁，就会通知标识为 SIGNAL 状态的后续节点的线程
    		// CONDITION = -2 ：在Condition中（await,signal）中 会使用到
   			// PROPAGATE = -3 ：下一个acquireShared应该无条件传播
            // 1. 当 线程的pred线程被取消（1）时，会将该线程从竞争锁队列中删除
    		// 2. 当 线程的pred线程状态不是被取消（1)或者不是 -1 时，会将该线程的pred线程设置为 -1 :SIGNAL
    		// 3. 当 线程的pred线程状态是-1是，则返回true，表示可以进行挂起
            if (shouldParkAfterFailedAcquire(p, node) &&
                // park 当前线程
                // 如果返回true 表示已经中断
                parkAndCheckInterrupt())
                throw new InterruptedException();
        }
    } finally {
        if (failed)
            cancelAcquire(node);
    }
}
```

![1559297289600](assets/1559297289600.png)

#### `countDown`

该方法主要是将计数器归0 ，说是释放锁，其实在 await 中并没有获得锁，所以这个 "释放锁" 只是在思想上与`unlock` 相似。

```java
// 计数器递减，如果计数达到零释放所有等待的线程,
public void countDown() {
    sync.releaseShared(1);
}
public final boolean releaseShared(int arg) {
    // 尝试释放共享锁
    // 实际上是将 计数器递减，如果递减后为0 ，则返回true
    if (tryReleaseShared(arg)) {
        // 如果在这里 计数器已经归0 了，便可以进行锁释放
        // 该方法主要是将 挂起的线程进行唤醒，修改waitStatus =-3 表示共享锁
        doReleaseShared();
        return true;
    }
    return false;
}
private void doReleaseShared() {
    /*
         * Ensure that a release propagates, even if there are other
         * in-progress acquires/releases.  This proceeds in the usual
         * way of trying to unparkSuccessor of head if it needs
         * signal. But if it does not, status is set to PROPAGATE to
         * ensure that upon release, propagation continues.
         * Additionally, we must loop in case a new node is added
         * while we are doing this. Also, unlike other uses of
         * unparkSuccessor, we need to know if CAS to reset status
         * fails, if so rechecking.
         */
    for (;;) {
        Node h = head;
        // 因为head 节点中并没有存储线程，
        // 如果head 和 tail 相同的话，说明同步队列中还没有等待获得锁的节点
        if (h != null && h != tail) {
            int ws = h.waitStatus;
            // head 的 waitStatus 如果 SIGNAL 表示可以去获取锁
            if (ws == Node.SIGNAL) {
                // 修改 head 的waitStatus =0 ，
                // 如果成功 唤醒head 的next 节点，
                // 如果失败 跳出当前循环：说明已经设置修改过了
                if (!compareAndSetWaitStatus(h, Node.SIGNAL, 0))
                    continue;            // loop to recheck cases
                // unpark h.next
                // 唤醒 head节点的next节点
                // 这个方法在上面也已经分析过了，主要是唤醒当前节点的下一个节点
                unparkSuccessor(h);
            }
            else if (ws == 0 &&
                     // 在上面if 的方法中，修改了head 的waitStatus =0 了，
                     // 这里在修改为 -3  ，表示是一个共享锁
                     !compareAndSetWaitStatus(h, 0, Node.PROPAGATE))
                continue;                // loop on failed CAS
        }
        if (h == head)                   // loop if head changed
            break;
    }
}
```

![1559297321402](assets/1559297321402.png)

#### `await` 被唤醒后的逻辑

从上面的 `await` 和 `countDown` 我们可以知道，根据实例化 `CountDownLatch` 时的构造参数的整数作为计数器。当执行 `countDown` 的时候计数器会递减，当计数器还未递减到0时，在`await` 方法中线程都会通过封装成一个node（thread=当前线程,waitStatus=0,,nextWaiter=SHARED) 添加到同步队列中，当自旋获得不到锁时会将当前线程的上一个节点的waitStatus设置为-1，并挂起该线程。当 计数器递减为0 后， 会修改head节点的状态值为 `waitStatus=PROPAGATE（-3）`，并唤醒head 节点的下一个节点。

此时我们接着分析，当挂起的线程被唤醒后又做了哪些事情。继续在 `await` 方法中的 `doAcquireSharedInterruptibly（1）` 来分析。

```java
private void doAcquireSharedInterruptibly(int arg)
    throws InterruptedException {
    // 创建一个共享锁，封装当前线程的节点到同步队列中
    final Node node = addWaiter(Node.SHARED);
    boolean failed = true;
    try {
        for (;;) {
            // 拿到当前节点的上一个节点
            final Node p = node.predecessor();
            // 如果该当前线程的上一个节点 是 head 节点，则尝试去获得锁
            if (p == head) {
                // r=1 表示计数器已归0
                // r=-1 表示计数器还未归0
                int r = tryAcquireShared(arg);
                // 如果此时 正好CountDown 次数 使计数器归 0
                // 因为是自旋，当线程挂起后再次被唤醒后还是会执行到这块代码
                if (r >= 0) {
                    // todo xxxx
                    // 设置当前节点为 head
                    setHeadAndPropagate(node, r);
                    // 并取消原head 的next 引用
                    p.next = null; // help GC
                    failed = false;
                    return;
                }
            }
            // 获得锁失败后，
            // ..........
        }
    } finally {
        if (failed)
            cancelAcquire(node);
    }
}
// 设置新的 head 节点，
// 此时是从 CountDownLatch 过来的，propagate 的值只能为 1 （即计数器归0）才会走到这里
private void setHeadAndPropagate(Node node, int propagate) {
    Node h = head; // Record old head for check below
    // 设置新的 head 节点，
    setHead(node);
    /*
         * Try to signal next queued node if:
         *   Propagation was indicated by caller,
         *     or was recorded (as h.waitStatus either before
         *     or after setHead) by a previous operation
         *     (note: this uses sign-check of waitStatus because
         *      PROPAGATE status may transition to SIGNAL.)
         * and
         *   The next node is waiting in shared mode,
         *     or we don't know, because it appears null
         *
         * The conservatism in both of these checks may cause
         * unnecessary wake-ups, but only when there are multiple
         * racing acquires/releases, so most need signals now or soon
         * anyway.
         */
    // propagate > 0 表示当前计数器已归0
    if (propagate > 0 || h == null || h.waitStatus < 0 ||
        (h = head) == null || h.waitStatus < 0) {
        // 获得当前节点的下一个节点，如果是共享锁继续进行唤醒
        Node s = node.next;
        // 此时 CountDownLatch 过来的节点 是 nextWaiter = SHARED;
        // 所以是满足的，即共享锁，继续唤醒下一个节点
        if (s == null || s.isShared())
            // 此时head 节点已经更新了，
            // 所以继续执行该代码，会继续唤醒该节点的下个节点的线程
            doReleaseShared();
    }
}
```

![1559299044604](assets/1559299044604.png)



#### 疑问

1. **`await 等待的原理：`**  当  `CountDownLatchDemo` 多个线程同时启动时，执行到 `run()` 后为什么会处于等待状态？

   > 答：当执行了 `await` 方法后，由于计数器不归0 ，尝试获得锁会失败。并将该线程添加到同步队列中（node=（thread=null,waitStatus=0,nextWaiter=SHARED）) 是一个标志共享锁的节点。并再次节点中获取锁，失败后修改该节点的上一个节点的状态 `waitStatus=-1` ，并挂起当前线程。

2. **`countDown 唤醒等待的原理：`** 而 当 `countDown` 执行多次后 计数器归 0 后，是怎么通知   `CountDownLatchDemo`  线程的？

   > 答：当执行完 `await` 后，由于计数器还未归0 ，所有线程当添加到同步队列中，当执行了 `countDown` 方法使计数器归0后会进行锁的释放。锁的释放主要是将head节点的 `waitStatus=-3（共享）` 并唤醒处于 head 节点的next 节点的线程(ThreadA)让其去竞争锁。被唤醒的线程再次回在 `await` 方法中获取锁，此时计数器归0 ，获得锁成功，该节点成为新的 `head` 。因为节点是共享锁，所以会再次调用锁的释放，再次去唤醒新 head 的next 节点，直至所有挂起的线程都获得锁。

