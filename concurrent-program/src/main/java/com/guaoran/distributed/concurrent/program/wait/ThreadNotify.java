package com.guaoran.distributed.concurrent.program.wait;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2019/2/26 23:26
 */
public class ThreadNotify extends Thread {
    private Object lock;

    public ThreadNotify(Object lock) {
        this.lock = lock;
    }

    @Override
    public void run() {
        synchronized (lock) {
            System.out.println("开始执行 thread notify");
            lock.notify();
            System.out.println("执行结束 thread notify");
        }
    }

    public static void main(String[] args) {
        Object lock = new Object();
        ThreadWait wait = new ThreadWait(lock);
        wait.start();
        ThreadNotify notify = new ThreadNotify(lock);
        notify.start();
    }
}
