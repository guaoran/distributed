package com.guaoran.distributed.concurrent.program.JUC;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Author gucheng
 * @Description CyclicBarrier:阻塞一组线程直到某个事件的发生
 * 2019-05-21 13:54
 */
public class CyclicBarrierDemo extends Thread{
    private CyclicBarrier cyclicBarrier;

    public CyclicBarrierDemo(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(3,new Thread(()->{
            System.out.println("三个线程已经执行完毕，现在可以读取三个线程操作过的数据。。");
        }));
        for (int i = 0; i < 3; i++) {
            new CyclicBarrierDemo(cyclicBarrier).start();
        }
    }

    /**
     * 大概意思是 进行构造的时候,传入一个计时器，即一个函数
     * 当我们每次调用await的时，会进行lock ，计时器-1，并await
     * 当我们调用最后一次的await的时候，会进行lock ，计时器归0，执行传入的函数， 并唤醒所有等待的线程执行后续逻辑
     *
     */
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"进来了,等待其他线程操作完毕");
        System.out.println(Thread.currentThread().getName()+"加载数据缓存操作");
        try {
            cyclicBarrier.await();

            System.out.println(Thread.currentThread().getName()+"读取线程中的数据");

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
