package com.guaoran.distributed.concurrent.program.demo;

import java.util.concurrent.TimeUnit;

/**
 * @Author gucheng
 * @Description 线程中断
 * 2019-05-30 15:50
 */
public class InterruptedDemo {
    public static void main(String[] args) throws InterruptedException {
        //通过 interrupt 中断线程
        Thread thread = new Thread(()->{
            // Thread.currentThread().isInterrupted() 不会清除 标志位，
            // 默认一开始是 false ，如果线程被中断过会返回true，多次调用不会影响该状态值
            while(!Thread.currentThread().isInterrupted()){
            }
            // Thread.interrupted() 静态方法，会清除标记位
            // 当线程执行了线程中断（thread.interrupt();），
            // 第一次调用 Thread.interrupted() 返回true，再次调用时会返回false（表示多次调用会清除标志）
            System.out.println("a:"+Thread.interrupted()+"->"+System.currentTimeMillis());//true
            System.out.println("b:"+Thread.interrupted()+"->"+System.currentTimeMillis());//false
        },"通过 interrupt 中断线程");
        thread.start();
        TimeUnit.SECONDS.sleep(1);
        // 中断线程
        thread.interrupt();



        // 不中断线程，查看默认的锁标记 ，清除标记其实还是还原为false
        new Thread(()->{

            // Thread.interrupted() 静态方法，会清除标记位
            // 当线程执行了线程中断（thread.interrupt();），
            // 第一次调用 Thread.interrupted() 返回true，再次调用时会返回false（表示多次调用会清除标志）
            System.out.println("aa:"+Thread.interrupted()+"->"+System.currentTimeMillis());//true
            System.out.println("bb:"+Thread.interrupted()+"->"+System.currentTimeMillis());//false
        }).start();
    }
}
