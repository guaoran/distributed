package com.guaoran.distributed.concurrent.program;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2018/12/13 17:46
 */
public class ExecutorsDemo implements Runnable{
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService service = null;
        //创建一个固定的线程池,负载比较大的,
        //TODO 该线程池的核心线程数和最大线程数相同，且非核心线程数不使用时立刻进行回收
        //TODO 并且线程的队列是LinkedBlockingQueue,可存储Integer.MAX_VALUE,可以理解为无上限
        service = Executors.newFixedThreadPool(1);
        //创建只有一个线程的线程池，可保证顺序执行
        //TODO 该线程池的核心线程数和最大线程数都是1，
        //TODO 并且线程的队列是LinkedBlockingQueue ，可存储Integer.MAX_VALUE,可以理解为无上限
        service = Executors.newSingleThreadExecutor();
        //创建一个不限制最大线程数
        //TODO 该线程池没有核心线程数，最大线程数是Integer.MAX_VALUE，即非核心线程数可以一直增加
        //TODO 并且非核心线程数不使用时 60s 后就会被回收，
        service = Executors.newCachedThreadPool();
        //定时器，延时执行的线程池
        //TODO 指定核心线程数，最大线程数是 Integer.MAX_VALUE
        service = Executors.newScheduledThreadPool(1);
        service = null;

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 100; i++) {
            //有返回值，且可以提交Runnable和 Callable 的线程
            Future future = executorService.submit(new ExecutorsDemo());
            System.out.println(future.get());
            //只能提交Runnable的线程
            //executorService.execute();
        }
        executorService.shutdown();

    }


    @Override
    public void run() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName());
    }

}
