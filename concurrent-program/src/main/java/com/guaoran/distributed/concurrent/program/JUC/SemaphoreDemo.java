package com.guaoran.distributed.concurrent.program.JUC;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @Author gucheng
 * @Description Semaphore 限流
 * 2019-05-21 13:44
 */
public class SemaphoreDemo extends Thread{
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(5);
        for (int i = 0; i < 100; i++) {
            new SemaphoreDemo(i,semaphore).start();
        }
    }

    private int i;
    private Semaphore semaphore;

    public SemaphoreDemo(int i, Semaphore semaphore) {
        this.i = i;
        this.semaphore = semaphore;
    }

    /**
     * 大概意思是
     * 构造的时候，创建一个可以重入5次锁的一个计数器
     * 每次acquire的时候计数器会减少一次， 当计数器减少为0后后续的任务将等待
     * 当释放锁的时候，计数器会加1 ，并唤醒后的线程去争抢锁，继而这样执行
     */
    @Override
    public void run() {
        try {
            semaphore.acquire();
            System.out.println("第"+i+"个程序进来了。。。。");
            TimeUnit.SECONDS.sleep(1);
            semaphore.release();
            System.out.println(i+"出去了");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
