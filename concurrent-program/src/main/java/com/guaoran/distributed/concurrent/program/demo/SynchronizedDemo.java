package com.guaoran.distributed.concurrent.program.demo;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2019/2/26 21:01
 */
public class SynchronizedDemo {
    public static Object objectLock  = new Object();
    public Object lock = new Object();
    public void locks(){
        //对象级别全局锁
        synchronized (objectLock){}
        //实例级别
        synchronized (lock){}
    }

    public static void test(){
        //全局锁，针对 SynchronizedDemo 对象而言是无论多少个对象，调用该方法都会进行竞争锁
        synchronized (SynchronizedDemo.class){}
    }
    //全局锁，针对 SynchronizedDemo 对象而言是无论多少个对象，调用该方法都会进行竞争锁
    public synchronized static void test2(){}

    public void demo(){
        synchronized (this){
            //针对对象的锁，如果是不同的实例对象，是不存在锁的竞争的
        }
    }
    public static void main(String[] args) {
        SynchronizedDemo.test();
        SynchronizedDemo.test2();
        SynchronizedDemo demo = new SynchronizedDemo();
        SynchronizedDemo demo2 = new SynchronizedDemo();
        demo.demo();
        demo2.demo();

    }
}
