package com.guaoran.distributed.concurrent.program;

import java.util.concurrent.CountDownLatch;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2018/12/13 16:52
 */
public class CountDownLatchDemo {
    public static void main(String[] args) throws InterruptedException {
        Long time = System.currentTimeMillis();
        CountDownLatch countDownLatch = new CountDownLatch(3);
        new Thread(()->{
            countDownLatch.countDown();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(()->{
            countDownLatch.countDown();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(()->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            countDownLatch.countDown();
        }).start();

        countDownLatch.await();
        System.out.println("用时"+(System.currentTimeMillis()-time)/1000+"s");

    }
}
