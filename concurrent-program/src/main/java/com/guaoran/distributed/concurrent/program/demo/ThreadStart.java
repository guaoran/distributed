package com.guaoran.distributed.concurrent.program.demo;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author : guaoran
 * @Description : <br/>
 *  启动线程，常用三种，Thread 、Runnable、Callable+future
 * @date :2019/2/26 11:19
 */
public class ThreadStart {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new Thread(()->{
            System.out.println(Thread.currentThread().getName());
        },"thread").start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        },"runnable").start();

        //继承Thread 类
        ThreadDemo threadDemo = new ThreadDemo();
        threadDemo.start();
        //实现Runnable 接口
        RunableDemo runableDemo = new RunableDemo();
        Thread thread = new Thread(runableDemo);
        thread.start();
        //实现Callable 接口
        ExecutorService executorService = Executors.newCachedThreadPool();
        CallableDemo callableDemo = new CallableDemo();
        Future<String> f = executorService.submit(callableDemo);
        System.out.println(f.get());
        executorService.shutdown();
    }


    static class ThreadDemo extends Thread{
        @Override
        public void run() {
            System.out.println("ThreadDemo");
        }
    }
    static class RunableDemo implements Runnable{
        @Override
        public void run() {
            System.out.println("Runnable");
        }
    }
}
