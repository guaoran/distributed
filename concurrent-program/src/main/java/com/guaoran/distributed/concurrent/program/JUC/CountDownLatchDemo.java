package com.guaoran.distributed.concurrent.program.JUC;

import java.util.concurrent.CountDownLatch;

/**
 * @Author gucheng
 * @Description CountDownLatch 并行执行
 * 2019-05-21 13:21
 */
public class CountDownLatchDemo extends Thread{

    static CountDownLatch countDownLatch = new CountDownLatch(2);
    public static void main(String[] args) {

        // 开启多个线程，并在run方法中进行线程等待，只有 countDown  归 0时才会进行执行，
        // 这种方式就是模拟并发执行
        for (int i = 0; i < 3; i++) {
            String name = "";
            switch (i){
                case 0:
                    name = "ThreadA";
                    break;
                case 1:
                    name = "ThreadB";
                    break;
                case 2:
                    name = "ThreadC";
                    break;
            }
            new CountDownLatchDemo(name).start();
        }
        // 当然 因为countDownLatch(2) ,所以 执行一次 countDown 还是不行的，
        // 真是情况下可以设置为1嘛  ，这里是例子合并成了一个
        countDownLatch.countDown();


        // 该线程是先执行业务逻辑， 最后在 countDown ， 计数归0 ，
        // 场景有点类似，在该线程中预热缓存 ，在上面多个线程中读取缓存
        new Thread(()->{
            System.out.println(Thread.currentThread().getName()+"\t"+"先执行我。。。。");
            countDownLatch.countDown();
        },"ThreadD").start();
    }

    public CountDownLatchDemo(String name) {
        super(name);
    }

    @Override
    public void run() {
        try {
            // 在 countDown 计数器为归0 之前一直在这里阻塞
            countDownLatch.await();
            System.out.println(Thread.currentThread().getName()+"\t"+System.currentTimeMillis());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
