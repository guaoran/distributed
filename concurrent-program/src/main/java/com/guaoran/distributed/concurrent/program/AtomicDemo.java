package com.guaoran.distributed.concurrent.program;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author : guaoran
 * @Description : <br/>
 * 原子类
 * @date :2019/2/27 14:47
 */
public class AtomicDemo {
    private static AtomicInteger count = new AtomicInteger(0);

    public static synchronized void inc() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        count.getAndIncrement();
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                AtomicDemo.inc();
            }).start();
        }
        Thread.sleep(4000);
        System.out.println(count.get());
    }
}
