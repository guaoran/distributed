package com.guaoran.distributed.concurrent.program.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2019/2/26 23:51
 */
public class LockDemo {
    static Lock lock = new ReentrantLock();//重入锁，包含公平锁和非公平锁
    private static int count = 0;
    /*
    synchronized和lock的区别
    1. lock是类上的实现，synchronized是jvm的关键字
    2. lock比synchronized更灵活 ，lock可以选择在什么时候进行加锁和释放锁
       1. synchronized释放锁：异常或执行完同步代码块
    3. lock可以判断锁的状态，而synchronized是一个关键字，无法灵活判断锁的状态
    4. lock 的实现可以是 ReentrantLock是一个重入锁，可以是公平或非公平锁，synchronized是一个非公平锁
     */
    public static void incr(){
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lock.lock();
        count ++;
        lock.unlock();
    }

}
