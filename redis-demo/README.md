# 目录

-   [Redis](#redis)
    -   [单机安装](#单机安装)
    -   [概览](#概览)
    -   [数据结构分析](#数据结构分析)
        -   [string ：字符串](#string-字符串)
        -   [list ：列表有序集合(双向链表)](#list-列表有序集合双向链表)
        -   [hash:哈希表](#hash哈希表)
        -   [set ：无序set](#set-无序set)
        -   [sortedSet ：zset](#sortedset-zset)
    -   [内部原理分析](#内部原理分析)
        -   [过期键的清除](#过期键的清除)
            -   [定时删除](#定时删除)
            -   [惰性删除](#惰性删除)
            -   [定期删除](#定期删除)
        -   [Redis 过期删除策略](#redis-过期删除策略)
            -   [过期键对AOF 、RDB
                和复制的影响](#过期键对aof-rdb-和复制的影响)
        -   [发布订阅模式](#发布订阅模式)
        -   [持久化原理分析](#持久化原理分析)
            -   [RDB 快照方式](#rdb-快照方式)
            -   [AOF 方式](#aof-方式)
                -   [开启 AOF](#开启-aof)
                -   [AOF 的重写原理](#aof-的重写原理)
        -   [内存回收策略](#内存回收策略)
        -   [单线程为什么性能很高？](#单线程为什么性能很高)
        -   [Lua 脚本在Redis的使用](#lua-脚本在redis的使用)
    -   [分布式Redis](#分布式redis)
        -   [Redis的集群](#redis的集群)
        -   [哨兵机制](#哨兵机制)
        -   [Redis-Cluster](#redis-cluster)
    -   [Redis 实践](#redis-实践)
        -   [分布式锁](#分布式锁)
        -   [缓存雪崩](#缓存雪崩)
        -   [缓存穿透](#缓存穿透)
    -   [常用命令](#常用命令)

# Redis

> redis 是单线程的，没有线程安全问题

## 单机安装

```shell
cd /guaoran/redis
wget http://download.redis.io/releases/redis-5.0.3.tar.gz
tar -zxvf redis-5.0.3.tar.gz
cd redis-5.0.3/
make
cd src
make install PREFIX=/guaoran/redis/
cp -r /guaoran/redis/redis-5.0.3/redis.conf  /guaoran/redis/bin/
vi /guaoran/redis/bin/redis.conf
```

```conf
# daemonize no 
daemonize yes
```

启动

```shell
/guaoran/redis/bin/redis-server /guaoran/redis/bin/redis.conf 
```



## 概览

> 现在要做一个新闻的点赞与评论的功能，还有一个发送验证码的功能
>
> 可以用set 集合存储每个人点了那些新闻
>
> 用zset 集合存储每个新闻下有多少个评论数和点赞数，这样在获得点赞或评论数最多新闻的时候，可以根据排序后的score值进行获得
>
> 用hash 存储每个新闻下有谁评论了该新闻，评论内容是什么
>
> 用string 存储验证码某个IP的发送次数

## 数据结构分析

五种数据结构：

### string ：字符串

* key：value
* 字符串、整数、浮点数（incr：原子递增）
* 在redis内部，数据结构是int、SDS

使用场景：

> 用户session缓存，单点登录
>
> 短信验证，ip限制incr
>



### list ：列表有序集合(双向链表)

* key value1 value2 ...valuen
* 内部的数据结构
  * 3.2之前：linkedlist 和 ziplist（压缩列表
  * 3.2之后：quicklist 两个（linkedlist 和 ziplist）集合的特性
    * 由ziplilst组成的双向链表

使用场景

> 生产者 lpush 插入数据，消费者brpop消费数据（分布式队列）
>
> 栈：后进先出 lpush lpop
>
> 队列：先进先出 lpush rpop

### hash:哈希表

* key , {filed:value,filed2:value.......}
* 数据结构
  * hash
  * ziplist :数据量少的时候
  * dict dictEntry

应用场景：

> 存放用户信息

### set ：无序set

* key value vlaue2 value3....valuen
* 数据结构
  * intset /hashtable(key,value(null))

应用场景：

> 交集、并将、差集，去重

### sortedSet ：zset

* key score member
* 数据结构
  *  ziplist 或skiplist + hashtable
  *  有序链表
  * skiplist：通过level去控制

应用场景

> 网站排名，点赞、关注数排行



## 内部原理分析

### 过期键的清除

#### 定时删除

在设置键的过期时间时，创建一个定时事件，当过期时间到达时，由事件处理器自动执行键的删除操作。

优点：对内存是最友好的：因为它保证过期键会在第一时间被删除，过期键所消耗的内存会立即被释放。

缺点：它对CPU 时间是最不友好的：因为删除操作可能会占用大量的CPU 时间

#### 惰性删除

放任键过期不管，但是在键被访问时要检查键是否过期，如果过期的话就删除它并返回空；如果没过期就返回键值

优点：对CPU 时间来说是最友好的：它只会在键被访问时检查，如果过期就进行删除

缺点：它对内存是最不友好的：如果一个键已经过期但却没有被访问，它占用的内存就不会被释放

#### 定期删除

每隔一段时间，对expires 字典进行检查，删除里面的过期键。

定期删除是上面两种策略的一种折中：

* 它每隔一段时间执行一次删除操作，并通过限制删除操作执行的时长和频率，籍此来减
  少删除操作对CPU 时间的影响。
* 另一方面，通过定期删除过期键，它有效地减少了因惰性删除而带来的内存浪费。

### Redis 过期删除策略

删除策略采用的是**惰性删除** + **定期删除**，这两个策略相互配合，可以很好的合理利用CPU 时间和节约内存空间之间取得平衡

原理

* 消极方法(**惰性删除**)：过期就删除：在主键被访问的时候，如果发现他已经失效，那么就删除它。
* 积极方法(**定期删除**)：周期性的从设置过期时间的key中选择一部分失效的key进行删除，对于那些未被查询的key，即便过期，被动方式也无法清除。因此redis 会周期性的随机测试一些key，已过期的key将会被删除。redis每秒会进行10次操作。
  * 随机测试20个带有timeout信息的key
  * 如果随机测试获得的20个key超过25%的key被删除，则重复执行整个流程

#### 过期键对AOF 、RDB 和复制的影响

更新后的RDB 文件

> 在创建新的RDB 文件时，程序会对键进行检查，过期的键不会被写入到更新后的RDB 文件中。因此，过期键对更新后的RDB 文件没有影响。

AOF 文件

> 在键已经过期，但是还没有被惰性删除或者定期删除之前，这个键不会产生任何影响，AOF 文件也不会因为这个键而被修改。
>
> 当过期键被惰性删除或者定期删除之后，程序会向AOF 文件追加一条DEL 命令，来显式地记录该键已被删除。
>
> > 例如：存在message 键已经过期，get message 时，发现过期了
> >
> > 1. 从数据库中删除message ；
> > 2. 追加一条DEL message 命令到AOF 文件；
> > 3. 向客户端返回NIL 。

AOF 重写

> 和RDB 文件类似，当进行AOF 重写时，程序会对键进行检查，过期的键不会被保存到重写后的AOF 文件。因此，过期键对重写后的AOF 文件没有影响。

复制

> 当服务器带有附属节点时，过期键的删除由主节点统一控制：
>
> * 如果服务器是主节点，那么它在删除一个过期键之后，会显式地向所有附属节点发送一个DEL 命令。
> * 如果服务器是附属节点，在执行客户端发送的读命令时，即使碰到过期键也不会将过期键删除，而是继续像处理未过期的键一样来处理过期键；当接到从主节点发来的DEL 命令之后，附属节点才会真正的将过期键删除掉。

### 发布订阅模式

Redis提供了发布订阅功能，可以用于消息的传输，Redis提供了一组命令可以让开发者实现“发布/订阅”模式(publish/subscribe) . 该模式同样可以实现进程间的消息传递
* 发布者发布消息的命令 
    * publish channel message
* 订阅者订阅消息的命令
    * sub channel channel2 ....



### 持久化原理分析

Redis支持两种方式的持久化，一种是RDB方式、另一种是AOF（append-only-file）方式。前者会根据指定的规则“定时”将内存中的数据存储在硬盘上，而后者在每次执行命令后将命令本身记录下来。两种持久化方式可以单独使用其中一种，也可以将这两种方式结合使用

#### RDB 快照方式

当符合一定条件时，Redis会单独创建（fork）一个子进程来进行持久化，会先将数据写入到一个临时文件中，等到持久化过程都结束了，再用这个临时文件替换上次持久化好的文件（`dump.rdb`)。整个过程中，主进程是不进行任何IO操作的，这就确保了极高的性能。如果需要进行大规模数据的恢复，且对于数据恢复的完整性不是非常敏感，那RDB方式要比AOF方式更加的高效。RDB的缺点是最后一次持久化后的数据可能丢失

> --fork的作用是复制一个与当前进程一样的进程。新进程的所有数据（变量、环境变量、程序计数器等）数值都和原进程一致，但是是一个全新的进程，并作为原进程的子进程

Redis会在以下几种情况下对数据进行快照

* 根据配置规则进行自动快照
  * 如： save 900 1 :save seconds changes (900秒内执行1次改变会触发) 
* save命令或者bgsave，
  * save命令会阻塞所有客户端的请求，可以用bgsave

* 执行 `flushall` 命令
* 执行复制操作 
  * 该操作主要是在主从模式下，redis会在复制初始化时进行自动快照

#### AOF 方式

当使用Redis存储非临时数据时，一般需要打开AOF持久化来降低进程终止导致的数据丢失。AOF可以将Redis执行的每一条写命令追加到硬盘文件中，这一过程会降低Redis的性能，但大部分情况下这个影响是能够接受的，另外使用较快的硬盘可以提高AOF的性能

##### 开启 AOF

默认情况下Redis没有开启AOF（append only file）方式的持久化，可以通过appendonly参数启用，在redis.conf 中找到 `appendonly yes` 

开启AOF持久化后每执行一条会更改Redis中的数据的命令后，Redis就会将该命令写入硬盘中的AOF文件。AOF文件的保存位置和RDB文件的位置相同，都是通过dir参数设置的，默认的文件名是apendonly.aof. 可以在redis.conf 中的属性 appendfilename appendonlyh.aof修改

>  ***注意：***  如果配置AOF 后发现所有的数据都丢失了，此时不要担心，因为在redis中默认走的是RDB，当配置了AOF后，则不再从 `dump.rdb`  中读取数据，但是RDB 中的数据并没有丢失，因此需要在RDB 持久化方式的时候将数据复制到AOF文件中，在切换为AOF方式的持久化就可以了。
>
> 在RDB持久化方式中，配置启动AOF，修改完配置文件后，先不要重启，需要先将rdb的数据存储到aof文件中，
 >
> ```shell
> redis-cli> CONFIG SET appendonly yes
> redis-cli> CONFIG SET save ""
> ```
>
> 执行完成后会发现在 `dump.rdb` 所在位置会出现 `appendonly.aof` 的文件，此时可以重启服务。
>
>


AOF：实时保存到文件，默认关闭，开启后默认会走aof，不会再走RDB，

- 手动开启 appendonly yes
- aof 文件的写入，只是针对数据变更的操作，
- appendfsync  :记录日志的方式
  - appendfsync always：总是写入aof文件，并完成磁盘同步
  - appendfsync everysec：每一秒写入aof文件，并完成磁盘同步
  - appendfsync no：写入aof文件，不等待磁盘同步。



由于配置了AOF，当我们执行了对某个key 进行多次修改后，会看到AOF中存在了之前修改时的命令， 这样的话会导致aof 文件中的数据越来越大， 此时我们需要的是只保存最后一次的修改即可。

> 而实际上Redis也考虑到了，可以配置一个条件，每当达到一定条件时Redis就会自动重写AOF文件，这个条件的配置是：
>
> ```properties
> ## 表示的是当目前的AOF文件大小超过上一次重写时的AOF文件大小的百分之多少时会再次进行重写，如果之前没有重写过，则以启动时AOF文件大小为依据
> auto-aof-rewrite-percentage 100
> ## 表示限制了允许重写的最小AOF文件大小，通常在AOF文件很小的情况下即使其中有很多冗余的命令我们也并不太关心。
> auto-aof-rewrite-min-size 64mb
> ```
>
> 还可以通过BGREWRITEAOF 命令手动执行AOF，执行完以后冗余的命令已经被删除了

##### AOF 的重写原理

Redis 可以在 AOF 文件体积变得过大时，自动地在后台对 AOF 进行重写： 重写后的新 AOF 文件包含了恢复当前数据集所需的最小命令集合

### 内存回收策略

Redis中提供了多种内存回收策略，当内存容量不足时，为了保证程序的运行，这时就不得不淘汰内存中的一些对象，释放这些对象占用的空间，那么选择淘汰哪些对象呢？
其中，默认的策略为`noeviction`策略，当内存使用达到阈值的时候，所有引起申请内存的命令会报错

LRU

> 内存管理的一种页面置换算法，对于在内存中但又不用的数据块(内存块）叫做LRU，操作系统会根据哪些数据属于LRU而将其移出内存而腾出空间来加载另外的数据。 

`回收策略`:当物理内存已使用完毕，且申请不到内存时

> `maxmemory-policy` noeviction
>
> `noeviction ` ：默认值，报错
>
> `allkeys-lru` : 挑选出最少使用的数据去淘汰 ,适合 `存放相对热点数据`  的场合
>
> `allkeys-random` ：随机去移除某些key，适合 `存放访问概率相等数据` 的场合
>
> > 从已经设置过期时间的keys中去选择
> >
> > `volatile-random` ：已经设置过期时间中去选择任意数据淘汰
> >
> > `volatile-lru` ：已经设置过期时间中使用比较少的key，
> >
> > > 采样的方式
> >
> > `volatile-ttl` : 已经设置过期时间中即将过期的数据进行淘汰
>

### 单线程为什么性能很高？

* 多路复用机制：可以再同一个时刻处理多个io请求（非阻塞io和多路复用）
* 内存和网络的带宽
* 基于内存操作
* 基于单线程，避免多线程上下文的切换



同步阻塞、同步非阻塞、异步阻塞、异步非阻塞

>同步阻塞和异步阻塞：老师提问学生，
>
>> 同步阻塞：老师一个一个的去问，如果有人答不上来，则会导致后面的学生无法进行回答
>>
>> 异步阻塞：老师让学生会的人先提前抢答，不会的人可以有时间进行思考，想好之后再告诉老师答案
>
>多路复用
>
>> 



### Lua 脚本在Redis的使用

lua 可以保证原子性

在lua脚本中执行redis的命令

`redis.call("set","demo","123")` :添加key是demo，value是123的数据

`eval "return redis.call('get','name')" 0` ：命令方式获得key

`eval "return redis.call('set',KEYS[1],ARGV[1])" 1 guaoran val` ：命令方式添加key

文件方式

> vim firstLuaDemo.lua
>
> return redis.call('get','guaoran')
>
> ./redis-cli --eval firstLuaDemo.lua 

限制ip在多少秒内的访问次数

> vim ratelimit.lua 
>
> ```shell
> local key = "ratelimit:"..KEYS[1]
> 
> local limit = tonumber(ARGV[1])
> 
> local expireTime = ARGV[2]
> 
> local times = redis.call('incr',key)
> 
> if times == 1 then
>         redis.call('expire',key,expireTime)
> end
> 
> if times > limit then
>         return 0
> end
> 
> return 1
> 
> ```
>
>  ./redis-cli --eval ratelimit.lua  127.0.0.1 , 10  10



可以客户端执行

> 127.0.0.1:6379> script load "return redis.call('get','guaoran')"
>
> "fa2264d8f582d7caa3e0b9f3ff2315e88518b852"
>
> 127.0.0.1:6379> evalsha "fa2264d8f582d7caa3e0b9f3ff2315e88518b852"0
> "val"



## 分布式Redis

### Redis的集群

> 135:master
>
> 131,134:slave

修改master配置

> in 135
>
> vim redis.conf
>
> ```config
> #bind 127.0.0.1 
> protected-mode no
> # 以守护线程启用
> daemonize yes
> ## 设置密码
> requirepass guaoran
> ## master 密码
> # masterauth <master-password>
> masterauth guaoran
> ```
>



修改slave配置

> in 131,134
>
> vim redis.conf
>
> ```config
> # replicaof <masterip> <masterport>
> replicaof 192.168.45.135 6379
> #bind 127.0.0.1 
> protected-mode no
> # masterauth <master-password>
> masterauth guaoran
> ```



同步机制

```sequence
slave(136) -> master : SYNC 
master -> master :bgsave
master -> slave(136) :快照
slave(136) -> slave(136) :load快照
master -> slave(136) :命令进行增量复制


```

原理：

>  master 不做全量复制(初始化的时候做全量复制，之后是增量复制），即不去生成磁盘快照
>
> ```shell
> repl-diskless-sync no
> ```





### 哨兵机制



> sentinel
>
> 目的：
>
> > 监控master和slave是否正常
> >
> > 当master出现故障时，从slave中选举一个新的master
>
> raft算法（协议）



哨兵的配置`sentinel.conf`

> ```conf
> # time while performing the synchronization with the master.
> sentinel monitor mymaster 192.168.45.134 6379 1
> 
> # sentinel failover-timeout <master-name> <milliseconds>
> # 表示如果5s内mymaster没响应，就认为SDOWN
> sentinel down-after-milliseconds mymaster 5000
> 
> ## 哨兵密码
> sentinel auth-pass mymaster guaoran
> 
> # sentinel down-after-milliseconds <master-name> <milliseconds>
> #表示如果15秒后,mysater仍没活过来，则启动failover，从剩下的 slave中选一个升级为master
> sentinel failover-timeout mymaster 15000 
> ```
>

两种方式启动哨兵

```shell
redis-sentinel sentinel.conf
redis-server /path/to/sentinel.conf --sentinel
```



> 哨兵模式下，redis 客户端应该连接到哪个redis-server
>
> > JedisSentnelPool
> >
> > 根据哨兵的集群地址，通过哨兵获得redis的master地址 ，拿到地址后会进行连接，并进行监控，

### Redis-Cluster

> 三主六从
>
> 三台虚拟机服务器：135,134,131
>
> > `in 192.168.45.135`
>
> > ```sh ell
> > -- 下载 redis 依赖
> > yum install gcc 
> > cd /guaoran/redis/cluster/
> > wget http://download.redis.io/releases/redis-5.0.3.tar.gz
> > tar -zxvf redis-5.0.3.tar.gz 
> > cd redis-5.0.3  
> > make 
> > cd src
> > -- 使编译后的文件进入到指定目录，这样可以保证目录的整洁
> > make install PREFIX=/guaoran/redis/cluster/redis_cluster_7000
> > 复制 redis 和 哨兵 的配置文件
> > cd ../
> > cp -r redis.conf /guaoran/redis/cluster/redis_cluster_7000/
> > cp -r sentinel.conf /guaoran/redis/cluster/redis_cluster_7000/
> > -- 进入的进行集群的目录下，复制集群目录
> > cd /guaoran/redis/cluster/
> > -- 修改端口信息
> > vi redis_cluster_7000/redis.conf 
> > 
> > ```
> >
> >  vim  redis_cluster_7000/redis.conf 
> >
> > ```conf
> > port 7000
> > # bind 127.0.0.1
> > # 修改保护模式关闭
> > protected-mode no
> > # 以守护线程启用
> > daemonize yes
> > # 启用集群
> > cluster-enabled yes 
> > # 配置每个节点的配置文件，同样以端口号为名称
> > cluster-config-file nodes-7000.conf
> > # 配置集群节点的超时时间，可不改，默认也是
> > cluster-node-timeout 15000  
> > # 启动AOF增量持久化策略
> > appendonly yes  
> > # 发生改变就记录日志
> > appendfsync always  
> > ```
> >
> > 复制该服务器的另外两个节点和两位两个服务器的六个节点
> >
> > ```shell
> > pwd
> > /guaoran/redis/cluster
> > ls
> > redis_cluster_7000
> > -- 以7000 为模板进行复制
> > cp -r redis_cluster_7000 redis_cluster_7001
> > cp -r redis_cluster_7000 redis_cluster_7002
> > ls
> > redis_cluster_7000  redis_cluster_7001  redis_cluster_7002
> > 
> > -- 端口信息修改 和 cluster-config-file nodes-7000.conf 依次对应的端口信息
> > vim redis_cluster_7001/redis.conf 
> > vim redis_cluster_7002/redis.conf 
> > 
> > 
> > -- 进行复制到另外两台服务器中，
> > -- 如果/guaoran/redis/cluster/ 该目录不存在，需要进行创建
> > -- 134 
> > scp -r redis_cluster_7000 root@192.168.45.134:/guaoran/redis/cluster/
> > scp -r redis_cluster_7001 root@192.168.45.134:/guaoran/redis/cluster/
> > scp -r redis_cluster_7002 root@192.168.45.134:/guaoran/redis/cluster/
> > -- 131 
> > scp -r redis_cluster_7000 root@192.168.45.131:/guaoran/redis/cluster/
> > scp -r redis_cluster_7001 root@192.168.45.131:/guaoran/redis/cluster/
> > scp -r redis_cluster_7002 root@192.168.45.131:/guaoran/redis/cluster/
> > 
> > -- 依次启动135,134,131 的各个节点
> > ./redis_cluster_7000/bin/redis-server ./redis_cluster_7000/redis.conf 
> > ./redis_cluster_7001/bin/redis-server ./redis_cluster_7001/redis.conf 
> > ./redis_cluster_7002/bin/redis-server ./redis_cluster_7002/redis.conf 
> > 
> > ps -ef|grep redis
> > ```
> >
> > ![1544848436317](assets/1544848436317.png)
> >
> >  使用redis-cluster 进行集群创建
> >
> > ```shell
> > cd redis_cluster_7000/bin/
> > ./redis-cli --cluster create 192.168.45.135:7000 192.168.45.134:7000 192.168.45.131:7000 192.168.45.135:7001 192.168.45.134:7001 192.168.45.131:7001 192.168.45.135:7002 192.168.45.134:7002 192.168.45.131:7002 --cluster-replicas 2 
> > ```
> >
> > 在最后的replicas 1 指的是 主节点与从节点数的比例值.表示 master/slave=2 ？？？？？？？？
> >
> > 待确定是2还是0.5
> >
> > ![1544849131502](assets/1544849131502.png)
> >
> > 执行成功，可以看到有三台master
> >
> > ![1544849255552](assets/1544849255552.png)
> >
> >  
> >
> > 测试查看信息
> >
> > ```shell
> >  -- 连接任意一台redis
> >  ./redis-cli -c -h 192.168.45.135 -p 7000
> >  -- 查看集群信息 cluster info,查看节点列表 cluster nodes 
> > ```
> >
> > ![1544849536428](assets/1544849536428.png)
> >
> > 测试数据，在135连上一台131的slave，添加一个key为guaoran ，会进行重定向，说明是事务操作会交给master进行操作
> >
> > ![1544849707960](assets/1544849707960.png) 
> >
> > 但是如果这样执行，就会有问题，明明添加了一个，却找不到，再添加一个又是`(error) MOVED` ，使用上面的就正常，（-c表示集群模式 ）
> >
> > ![1544850241190](assets/1544850241190.png) 
> >
> > ![1544851159474](assets/1544851159474.png) 
> >
> > 





## Redis 实践

### 分布式锁

jedis+lua

Redisson

### 缓存雪崩

缓存没有数据了

### 缓存穿透

查询的key都不对应缓存

* 对空值做缓存
* 设置key的规则
* 布隆过滤器
  * 压缩算法
  * 空间效率非常高的概率性算法
  * 在40亿的数据中查询某个数据是否存在
  * 



## 常用命令

[Redis命令](http://redisdoc.com/)

| 命令                  | 注释              |
| --------------------- | ----------------- |
| keys *                | 查询所有key       |
| set key value         | 新增key           |
| expire key expiretime | 设置过期时间      |
| ttl key               | 查看key的过期时间 |
|                       |                   |
|                       |                   |

