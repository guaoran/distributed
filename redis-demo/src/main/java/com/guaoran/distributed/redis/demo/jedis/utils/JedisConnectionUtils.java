package com.guaoran.distributed.redis.demo.jedis.utils;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @author : guaoran
 * @Description : <br/>
 *  Jedis 连接池工具类
 * @date :2018/12/15 15:28
 */
public class JedisConnectionUtils {
    private static JedisPool pool=null;
    static {
        JedisPoolConfig jedisPoolConfig=new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(100);
        pool=new JedisPool(jedisPoolConfig,"192.168.45.135",6379);
    }
    public static Jedis getJedis(){
        return pool.getResource();
    }
}
