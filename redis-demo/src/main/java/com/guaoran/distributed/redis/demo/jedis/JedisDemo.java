package com.guaoran.distributed.redis.demo.jedis;

import com.guaoran.distributed.redis.demo.jedis.utils.DistributedLockUtils;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2018/12/15 15:36
 */
public class JedisDemo extends Thread {
    @Override
    public void run() {
        while (true) {
            DistributedLockUtils distributedLock = new DistributedLockUtils();
            String rs = distributedLock.acquireLock("updateOrder",
                    2000, 5000);
            if (rs != null) {
                System.out.println(Thread.currentThread().getName() + "-> 成功获得锁:" + rs);
                try {
                    Thread.sleep(1000);
//                    distributedLock.releaseLock("updateOrder",rs);
                    distributedLock.releaseLockWithLua("updateOrder", rs);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    public static void main(String[] args) {
        JedisDemo jedisDemo = new JedisDemo();
        for (int i = 0; i < 10; i++) {
            new Thread(jedisDemo, "tName:" + i).start();
        }
    }
}
