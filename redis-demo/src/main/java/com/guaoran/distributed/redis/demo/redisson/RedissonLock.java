package com.guaoran.distributed.redis.demo.redisson;

import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @author : guaoran
 * @Description : <br/>
 * @date :2018/12/15 15:46
 */
public class RedissonLock {
    public static void main2(String[] args) {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.45.135:6379");
        RedissonClient redissonClient = Redisson.create(config);
        RLock rLock = redissonClient.getLock("updateOrder");
        try {
            rLock.tryLock(100, 10, TimeUnit.SECONDS);
            System.out.println("test");
            Thread.sleep(1000);
            rLock.unlock();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            rLock.unlock();
            redissonClient.shutdown();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Semaphore semaphore = new Semaphore(0);
        semaphore.release(); //释放semaphore 才能后面判断为true
        if (semaphore.tryAcquire(100, TimeUnit.SECONDS)) {
            System.out.println("10s过后");
        }

        System.out.println("1");
    }


}
