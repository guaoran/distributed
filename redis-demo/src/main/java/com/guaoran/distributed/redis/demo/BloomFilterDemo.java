package com.guaoran.distributed.redis.demo;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

import java.nio.charset.Charset;

/**
 * @author : guaoran
 * @Description : <br/>
 *  布隆过滤器
 * @date :2018/12/15 17:34
 */
public class BloomFilterDemo {
    public static void main(String[] args) {
        // 初始化一个存储string数据的布隆过滤器，初始化大小为 100W
        BloomFilter bloomFilter = BloomFilter.create
                (Funnels.stringFunnel(Charset.defaultCharset()),
                        1000000,0.001); // 误判率设置为 1%
        bloomFilter.put("hello");
        bloomFilter.put("test");
        System.out.println(bloomFilter.mightContain("hello"));
    }
}
